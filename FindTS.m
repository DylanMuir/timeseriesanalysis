function [varargout] = FindTS(tsSeries, varargin)

% FindTS - FUNCTION Extract times and values from a time series (analogous to 'find')
%
% Usage: [vtTimes <, vfValues>] = FindTS(tsSeries)
%                           ... = FindTS(tsSeries, k)
%                           ... = FindTS(tsSeries, k, 'first')
%                           ... = FindTS(tsSeries, k, 'last')
%
% 'tsSeries' is a unitary time series. FindTS will find event times (in the
% case of an event time series), "true" periods (in the case of a boolean
% time series) or any samples (for other time series types).
%
% 'vtTimes' will be found times in the time series. 'vfValues' will contain
% the corresponding value of the time series at that point in time.
%
% The optional arguments 'k' and 'first'/'last' have the same sense as for
% the matlab find function.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 18th August, 2015

TS_deprecated('find');

[varargout{1:nargout}] = find(tsSeries, varargin{:});

% --- END of FindTS.m ---

