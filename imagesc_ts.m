function varargout = imagesc_ts(varargin)

% imagesc_ts - FUNCTION Produce a false-colour image of a time series
%
% Usage: See usage for imagesc

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 2015

TS_deprecated('imagesc');

[varargout{1:nargout}] = imagesc(varargin{:});