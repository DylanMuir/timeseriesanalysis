function varargout = TS_FindPreviousSample(ts, varargin)

% TS_FindPreviousSample - METHOD Find the entry in a sorted list prior to a desired target entry
%
% Usage: [vnPriorSampleIndex] = ts.TS_FindPreviousSample(vfSortedList, vfSortedTargets)
%
% This method is a convenience utility, to assist with writing an
% interpolation function. 'vfSortedList' and 'vfSortedTargets' MUST be both
% sorted, or else the output of this function is not guaranteed.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 24th September, 2015

% - Use the function handle within the TimeSeries object
[varargout{1:nargout}] = ts.fhFindPreviousSample(varargin{:});

% --- END of TS_FindPreviousSample.m ---
