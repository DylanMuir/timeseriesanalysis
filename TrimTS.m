function tsTrimmed = TrimTS(varargin)

% TrimTS - FUNCTION Trim a time series without resampling
% 
% Usage: tsTrimmed = TrimTS(tsSource, vtTimeLimits)
%        tsTrimmed = TrimTS(tsSource, tsBoolean)
%
% 'tsSource' is a time series. 'vtTimeLimits' is a vector [tStartTime tEndTime]
% which defines a time range to trim 'tsSource' to (including end points).
%
% 'tsTrimmed' will be a new time series, containing data only from the time
% range defined by 'vtTimeLimits'.
%
% Alternatively, a unitary time series 'tsBoolean' can be provided as the
% second argument. In this case, only periods during which 'tsBoolean' is
% 'true' will be kept.
%
% See also ExciseTS, which removes the data between 'vtTimeLimits'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: September 2014

TS_deprecated('trim');

tsTrimmed = trim(varargin{:});

% --- END of TrimTS.m ---