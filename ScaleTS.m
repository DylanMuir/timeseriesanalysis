function tsScaled = ScaleTS(varargin)

% ScaleTS - FUNCTION Scale the samples of a time series
%
% Usage: tsScaled = ScaleTS(tsSource, tsMultiplicand)
%                   ScaleTS(tsSource, fMultiplicand)
%
% 'tsSource' is a continuous time series. 'fScale' is a scalar multiplicand to
% apply to the sample values in 'tsSource'. 
%
% 'tsScaled' will be a copy of 'tsSource', with the sample values
% scaled appropriately.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 22nd October, 2014

TS_deprecated('.* or times');

tsScaled = times(varargin{:});

% --- END of ScaleTS.m ---
