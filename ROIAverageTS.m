function tsROIs = ROIAverageTS(tsSource, sROIs)

% ROIAverageTS - FUNCTION Average the traces within a set of ROIs
%
% Usage: tsROIs = ROIAverageTS(tsSource, sROIs)
%
% 'tsSource' is a time series structure, with 2D sample size. 'vsROIs' is a
% vector of ROI structures, as returned by 'bwconncomp'. The pixels
% identified in 'vsROIs' will be extracted from 'tsSources', and the pixels
% averaged within each ROI.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 28th October, 2015

% -- Check arguments

if (nargin < 2)
   help ROIAverageTS;
   
   error('TimeSeries:Usage', ...
         '*** TimeSeries/ROIAverageTS: Incorrect usage.');
end

% - Extract individual ROIs
vtsROISeries = tsSource.choose(sROIs);

% - Average series within each ROI in turn
for (nROI = 1:numel(vtsROISeries))
   vtsROISeries(nROI) = nanmean(vtsROISeries(nROI));
end

% - Merge resulting series
tsROIs = combine(vtsROISeries);

% --- END of ROIAverageTS.m ---
