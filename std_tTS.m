function tfStd = std_tTS(varargin)

% std_tTS - FUNCTION Compute the standard deviation over a time series, over time
%
% Usage: tfStd = std_tTS(tsSource)
%
% 'tsSource' is a continuous or count time series. The sample infinite-time
% standard deviation for each series in 'tsSource' will be computed and
% returned in 'tfStd'. 'tfStd' will be of size [1 M N ...], corresponding
% to the size of the time series in 'tsSource'.
%
% Samples will be weighted according the time duration they represent, for
% irregularly-sampled time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014


TS_deprecated('std_t');

tfStd = std_t(varargin{:});

% --- END of std_tTS.m ---
