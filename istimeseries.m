function bIsTimeSeries = istimeseries(oObject)

% istimeseries - FUNCTION Test whether a variable is a valid TimeSeries object
%
% Usage: bIsTimeSeries = istimeseries(oObject)
%
% 'oObject' is an arbitrary Matlab variable. 'bIsTimeSeries' is a boolean
% value: 'true' if 'oObject' is a valid TimeSeries object; 'false' otherwise.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 16th November, 2015

ITS_cstrTSFields = {'vtTimeTrace', 'tfSamples', 'strSampleType', 'fhInterpolation'};

% - Check usage
if (nargin < 1)
   help istimeseries;
   error('TimeSeries:Usage', '*** istimeseries: Incorrect usage.');
end

% - Check for a time series object
bIsTimeSeries = isa(oObject, 'TimeSeries');
bIsTimeSeries = bIsTimeSeries || (isstruct(oObject) && all(isfield(oObject, ITS_cstrTSFields)));

% --- END of istimeseries.m ---