function tsSum = nansumTS(varargin)

% nansumTS - FUNCTION Compute the sum (amplitude-wise) of several time series, ignoring NaNs
%
% Usage: tsSum = nansumTS(ts1 <, ts2, ...>)
%
% nansumTS will compute the sum of several time series. Unitary and
% non-unitary time series can be supplied. The time base of the sum will be
% the interleaved time bases of the source time series. Time series will be
% interpolated, when necessary, according to the interpolation function set
% for each time series. This function ignores NaNs, similar in behaviour to
% nansum.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 20th April, 2015

TS_deprecated('nansum');

tsSum = nansum(varargin{:});

% --- END of nansumTS.m ---