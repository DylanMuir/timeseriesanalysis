function [varargout] = GetRangeTS(varargin)

% GetRangeTS - FUNCTION Compute (or extract) the time range of a time series
%
% Usage: [tStartTime, tEndTime] = GetRangeTS(sTS)
% Usage: [vtTimeRange] = GetRangeTS(sTS)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th November, 2014

TS_deprecated('.vtTimeBounds property');

[varargout{1:nargout}] = range(varargin{:});

% --- END of GetRangeTS.m ---
