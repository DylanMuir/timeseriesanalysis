function [tsSum, vnNumNonNaNSeries] = sumTS(varargin)

% sumTS - FUNCTION Compute the addition (amplitude-wise) of several time series
%
% Usage: tsSum = sumTS(ts1 <, ts2, ...>)
%                sumTS(..., 'IgnoreNaNs', bIgnoreNaNs)
%
% sumTS will compute the sum of several time series. Unitary and
% non-unitary time series can be supplied. The time base of the sum will be
% the interleaved time bases of the source time series. Time series will be
% interpolated, when necessary, according to the interpolation function set
% for each time series.
%
% If the parameter 'IgnoreNaNs' is set to 'true', then NaN values will be
% ignored (similar behaviour to nansum). By default, NaN values will
% propagate into the sum (similar behaviour to sum).

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 20th April, 2015

TS_deprecated('sum');

[tsSum, vnNumNonNaNSeries] = sum(varargin{:});

% --- END of sumTS.m ---