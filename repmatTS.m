function tsReplicated = repmatTS(varargin)

% repmatTS - FUNCTION Replicate a time series
%
% Usage: tsReplicated = repmatTS(tsSource, varargin)

TS_deprecated('repmat');

tsReplicated = repmat(varargin{:});

% --- END of repmatTS.m ---
