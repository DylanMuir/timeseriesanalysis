function vtsSplit = SplitTS(varargin)

% SplitTS - FUNCTION Split multiple time series traces into separate objects
%
% Usage: vtsSplit = SplitTS(tsSource <, vnSeriesIndices>)
%
% 'vnSeriesIndices' is a vector of indices into the time series in
% 'tsSource'. If 'tsSeriesIndices' is not provided, then all unitary time
% series within 'tsSource' will be returned.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 4th September, 2014

TS_deprecated('split');

vtsSplit = split(varargin{:});

% --- END of SplitTS.m ---

