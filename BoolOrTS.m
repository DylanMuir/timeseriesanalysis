function [tsOrTS] = BoolOrTS(tsA, tsB)

% BoolOrTS - FUNCTION Perform a boolean OR of two boolean time series
%
% Usage: [tsOrTS] = BoolOrTS(tsA, tsB)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

TS_deprecated('| or ''or''');

tsOrTS = tsA | tsB;

% --- END of BoolOrTS.m ---
