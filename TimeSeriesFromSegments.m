function [tsSeries] = TimeSeriesFromSegments(cSegments, strSampleType, varargin)

% TimeSeriesFromSegments - FUNCTION Construct a time series from a list of segments
%
% Usage: [tsSeries] = TimeSeriesFromSegments(cSegments <, strSampleType, vfSampleRange, fhInterpolation, vtTimeBounds>)
%
% This function constructs a time series from a list of segment times and
% segment values. Each segment is a period of a contiguous value.
%
% 'cSegments' is a cell array {'vSeg1' 'vSeg2' ...}. Each segment 'vSegN'
% is a cell array {[tStart tEnd] fValue}, where 'tStart' and 'tEnd' are the start
% and end times of the segment in a common global time base. 'fValue' is
% the value adopted by the time series during that time segment. 'fValue'
% must be appropriate for the the sample type of the time series
% ('continuous' by default).
%
% The optional argument 'strSampleType' allows you to specify the type of
% time series to be constructed. 'strSampleType' must be one of 'boolean',
% 'categorical', 'count', 'continuous' (i.e. not 'event').
%
% 'vfSampleRange', 'fhInterpolation' and 'vtTimeBounds' have the same sense
% as for 'TimeSeries'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 18th August, 2015

% -- Check arguments

if (nargin < 1)
   help TimeSeriesFromSegments;
   error('TimeSeries:Usage', '*** TimeSeriesFromSegments: Incorrect usage.');
end

% - Catch empty time series
if (isempty(cSegments))
   if (~exist('strSampleType', 'var') || isempty(strSampleType))
      strSampleType = 'continuous';
   end
   
   % - Make an empty time series and return
   tsSeries = TimeSeries([], [], strSampleType);
   return;
end

% - Collect segments
cSegments = reshape([cSegments{:}], 2, [])';
cSegments(:, 1) = cellfun(@(c)(reshape(c, [], 1)), cSegments(:, 1), 'UniformOutput', false);
mtSegmentTimes = cat(2, cSegments{:, 1})';
vfSegmentVals = cat(1, cSegments{:, 2});

[~, nNumSeries] = size(vfSegmentVals);

% - Try to determine sample type, if not provided
if (~exist('strSampleType', 'var') || isempty(strSampleType))
   if (ischar(vfSegmentVals))
      strSampleType = 'categorical';
      
   elseif (islogical(vfSegmentVals))
      strSampleType = 'boolean';
      
   elseif (isa(vfSegmentVals, 'integer'))
      strSampleType = 'count';
      
   elseif (isempty(vfSegmentVals))
      strSampleType = 'event';
    
   else
      strSampleType = 'continuous';
   end
end

switch strSampleType
   case 'event'
      error('TimeSeries:Arguments', '*** TimeSeriesFromSegments: ''strSampleType'' cannot be ''event''.');
      
   case 'boolean'
      % - Only unitary boolean time series are supported
      if (nNumSeries > 1)
         error('TimeSeries:Arguments', '*** TimeSeriesFromSegments: Only unitary boolean time series can be created.');
      end
      
      % - Boolean time series. Take "true" segments, buffer with "false"
      vbUseSegments = logical(vfSegmentVals);
      mtSegmentTimes = mtSegmentTimes(vbUseSegments, :);
      mtSegmentTimes = [mtSegmentTimes(:, 1) - eps(mtSegmentTimes(:, 1)), mtSegmentTimes, mtSegmentTimes(:, 2) + eps(mtSegmentTimes(:, 2))]';
      vfSegmentVals = [false(nnz(vbUseSegments), 1) true(nnz(vbUseSegments), 2) false(nnz(vbUseSegments), 1)]';
      
      % - Create return time series
      tsSeries = TimeSeries(mtSegmentTimes(:), vfSegmentVals(:), strSampleType, varargin{:});
      
   case {'categorical', 'count', 'continuous'}
      % - Create return time series
      mtSegmentTimes = mtSegmentTimes';
      vfSegmentVals = [vfSegmentVals vfSegmentVals]';
      tsSeries = TimeSeries(mtSegmentTimes(:), vfSegmentVals(:), strSampleType, varargin{:});
      
   otherwise
      error('TimeSeries:Arguments', '*** TimeSeriesFromSegments: Unrecognised sample type.');
end

% --- END of TimeSeriesFromSegments.m ---

