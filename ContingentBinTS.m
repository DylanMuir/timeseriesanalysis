function [ctfDependentTSBinned, cmfDependentTriggerValues, vtsIndependentTSBinEvents, vfIndependentSampleBins] = ...
   ContingentBinTS(tsIndependent, tsDependent, vfIndependentSampleBins, vtDependentLagBins, strCrossingType, bSmoothDependent, bDisplayProgress)

% ContingentBinTS - FUNCTION Bin a dependent time series arising from events or threshold crossings in an independent time series
%
% Usage: [ctfDependentTSBinned, cmfDependentTriggerValues, vtsIndependentTSBinEvents, vfIndependentSampleBins] = ...
%           ContingentBinTS(tsIndependent, tsDependent, vfIndependentSampleBins, vtDependentLagBins <, strCrossingType, bSmoothDependent, bDisplayProgress>)
%           ContingentBinTS(tsIndependent, tsDependent, nNumIndependentSampleBins, ...)
%
%
% This function bins values of the independent time series, and finds time
% points where the independent series is equal to that value (rising,
% falling or both crossings). It then extracts segments of the dependent
% time series, according to the bins in 'vtDependentLagBins', and collects
% them. These segments will be returned in 'ctfDependentTSBinned', which is
% a cell array with each cell corresponding to one bin in
% 'vfIndependentSampleBins'. If 'nNumIndependentSampleBins' is suppled
% instead, then that number of equally-spaced bins will be used, spanning
% the minimum and maximum values of the independent time series.
%
% To supply a particular single sample bin in 'vfIndependentSampleBins',
% provide a vector [fBin fBin].
%
% 'cmfDependentTriggerValues' will be a cell array containing the values of
% the dependent time series, at the trigger point. Each cell corresponds to
% one independent sample bin.
%
% 'vtsIndependentTSBinEvents' will be a vector of event time series
% indicating the extracted crossing points, with each element of
% 'vtsIndependentTSBinEvents' corresponding to one independent sample bin.
%
% 'vfIndependentSampleBins' is returned as an output argument.
%
% If an event time series is supplied as the independent time series, then
% 'tfDependentTSBinned' and 'mfDependentTriggerValues' will be returned
% (i.e. not cell arrays).
%
% The optional argument 'strCrossingType' controls whether 'rising',
% 'falling' or 'both' threshold crossing types are examined. This argument
% has the same sense as supplied to 'EventThresholdTS'.
%
% The optional argument 'bSmoothDependent' controls whether the dependent
% time series should be automatically smoothed for time binning. By
% default, smoothing is applied ('true'). This can be skipped if the
% dependent trace has already been smoothed, or to save execution time and
% data storage requirements.
%
% The optional argument 'bDisplayProgress' controls whether progress
% messages are displayed (default: true).

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 9th September, 2014

DEF_strCrossingType = [];
DEF_bSmoothDependent = true;
DEF_bDisplayProgress = true;
cstrAllowedIndSampleTypes = {'event', 'continuous', 'boolean', 'count'};
cstrAllowedDepSampleTypes = {'continuous', 'boolean', 'categorical'};


% -- Check arguments

if (nargin < 4)
   help ContingentBinTS;
   error('TimeSeries:Usage', ...
         '*** ContingentBinTS: Incorrect usage');
end

if (~exist('strCrossingType', 'var') || isempty(strCrossingType))
   strCrossingType = DEF_strCrossingType;
end

if (~exist('bSmoothDependent', 'var') || isempty(bSmoothDependent))
   bSmoothDependent = DEF_bSmoothDependent;
end

if (~exist('bDisplayProgress', 'var') || isempty(bDisplayProgress))
   bDisplayProgress = DEF_bDisplayProgress;
end

if (~tsIndependent.testsampletype(cstrAllowedIndSampleTypes))
   error('TimeSeries:Arguments', ...
         '*** ContingentBinTS: Sample type of independent time series must be one of {%s}.', sprintf('%s ', cstrAllowedIndSampleTypes{:}));
end

if (~tsDependent.testsampletype(cstrAllowedDepSampleTypes))
   error('Timeseries:Arguments', ...
         '*** ContingentBinTS: Sample type of dependent time series must be one of {%s}.', sprintf('%s ', cstrAllowedDepSampleTypes{:}));
end

% - Handle an empty dependent time series
if (isempty(tsDependent))
   ctfDependentTSBinned = [];
   cmfDependentTriggerValues = [];
   vtsIndependentTSBinEvents = TimeSeriesStructure([], [], 'event');
   vfIndependentSampleBins = [];
   return;
end

[~, nNumDependentTS] = size(tsDependent.tfSamples);

% - Were we provided a number of bins, or a list of explicit bins?
if (isscalar(vfIndependentSampleBins))
   nNumIndSampleBins = vfIndependentSampleBins;
   vfIndependentSampleBins = linspace(min(tsIndependent.tfSamples(:)), max(tsIndependent.tfSamples(:)), nNumIndSampleBins);
else
   vfIndependentSampleBins = unique(vfIndependentSampleBins(:));
   nNumIndSampleBins = numel(vfIndependentSampleBins);
end

% - Determine bin widths
vtDependentLagBins = unique(vtDependentLagBins(:));
if (numel(vtDependentLagBins) > 1)
   fLagBinWidth = diff(vtDependentLagBins(1:2));
end

% - Pre-convolve dependent traces with boxcar function (for temporal binning)
if (bSmoothDependent && tsDependent.testsampletype('continuous') && ~isempty(fLagBinWidth))
   try
      tsDependentMA = ConvolveTS(tsDependent, @(vt)(double(abs(vt)<fLagBinWidth/2)./fLagBinWidth));
   catch mErr
      % - Ignore error if the convolution resolution was insufficient
      if ~strcmp(mErr.identifier, 'TimeSeries:ConvolutionResolution')
          rethrow(mErr);
      end
      
      % - If convolution fails, use non-smooothed trace
      tsDependentMA = tsDependent;
   end
else
   tsDependentMA = tsDependent;
end

% - Fix things up for an event time series
if (tsIndependent.testsampletype('event'))
   nNumIndSampleBins = 1;
   vfIndependentSampleBins = 0;
end

% - Fix things up for a boolean time series
if (tsIndependent.testsampletype('boolean'))
   nNumIndSampleBins = 1;
   vfIndependentSampleBins = 0.5;
end

% - Find or create "zero lag" trigger bin
nZeroLagBin = find(vtDependentLagBins == 0, 1, 'first');
bRemoveZeroLagBin = false;

if (isempty(nZeroLagBin))
   vtDependentLagBins(end+1) = 0;
   nZeroLagBin = numel(vtDependentLagBins);
   bRemoveZeroLagBin = true;
end

if (isempty(nNumIndSampleBins))
   nNumIndSampleBins = 1;
end

% -- Locate time points matching independent sample bins

for (nIndSampleBin = nNumIndSampleBins:-1:1)
   % - Handle an event time series
   if (tsIndependent.testsampletype('event'))
      vtsIndependentTSBinEvents(nIndSampleBin) = tsIndependent;
   else
      % - Find time points matching this sample bin
      vtsIndependentTSBinEvents(nIndSampleBin) = EventThresholdTS(tsIndependent, vfIndependentSampleBins(nIndSampleBin), strCrossingType);
   end

   % - Get event times
   vtThisISBinEvents = vtsIndependentTSBinEvents(nIndSampleBin).vtTimeTrace;

   
   if (bDisplayProgress)
       % - Show some progress
       fprintf(1, 'ContingentBinTS: Independent sample bin [%d/%d]: %d matches\n', nIndSampleBin, nNumIndSampleBins, numel(vtThisISBinEvents)); drawnow;
   end
   
   % - Resample dependent traces according to binned lags
   if (~isempty(vtThisISBinEvents))
      ctfDependentTSBinned{nIndSampleBin} = arrayfun(@(tEvent)(tsDependentMA.fhInterpolation(tsDependentMA, vtDependentLagBins + tEvent)), vtThisISBinEvents, 'UniformOutput', false);
      ctfDependentTSBinned{nIndSampleBin} = cat(3, ctfDependentTSBinned{nIndSampleBin}{:});
   else
      ctfDependentTSBinned{nIndSampleBin} = nan(numel(vtDependentLagBins), nNumDependentTS);
   end
end

% - Extract trigger values
cmfDependentTriggerValues = cellfun(@(c)(c(nZeroLagBin, :, :)), ctfDependentTSBinned, 'UniformOutput', false);

if (bRemoveZeroLagBin)
   ctfDependentTSBinned = cellfun(@(c)(c(1:end-1, :, :)), ctfDependentTSBinned, 'UniformOutput', false);
end

% - Reshape output arguments
if (nNumIndSampleBins == 1)
   ctfDependentTSBinned = ctfDependentTSBinned{1};
   cmfDependentTriggerValues = cmfDependentTriggerValues{1};
end

% --- END of ContingentBinTS.m ---