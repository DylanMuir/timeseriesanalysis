function cSegments = SegmentEnumeratorTS(tsSeries)

% SegmentEnumeratorTS - FUNCTION Divide a time series up into contiguous valued segments
%
% Usage: [cSegments] = SegmentEnumeratorTS(tsSeries)
%
% 'tsSeries' is a categorical, count or boolean unitary time series.
% 'cSegments' will be a cell array, with one entry per continugous-valued
% time segment in 'tsSeries'. Each cell will be {[tStart tEnd] fValue},
% where 'tStart' and 'tEnd' identify the start and end times of the
% segment, and 'fValue' contains the category, count or boolean value of
% the time series during that segment.
%
% Boolean time series are handled slightly differently, in that only
% segments of value "true" are returned.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 18th August, 2015

TS_Deprecated('segments');

cSegments = tsSeries.segments();

% --- END of SegmentEnumeratorTS.m ---