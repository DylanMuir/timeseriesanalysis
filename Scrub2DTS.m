function [hFigure] = Scrub2DTS(tsSource, vfDataRange, sRegions)

% Scrub2DTS - FUNCTION Make a window to play or scrub through a 2D time series
%
% Usage: [hFigure] = Scrub2DTS(tsSource, vnChannels, vfDataRange, sRegions)
%        [hFigure] = Scrub2DTS(tsSource, fhExtractionFunction, vfDataRange, sRegions)
%
% 'tsSource' is a TimeSeries object or structure, with at least 2
% dimensions. A window will be created showing the 2D samples as frames,
% which can then be scrubbed through like a video.
%
% Uses 'videofig' from Joo Filipe Henriques.

% Author: Dylan Muir <dylan.muir@unibas.ch>

% -- Check arguments

if (nargin < 1)
   disp('*** Scrub2DTS: Incorrect usage.');
   help Scrub2DTS;
   return;
end

if (~exist('sRegions', 'var') || isempty(sRegions))
   sRegions = [];
end

if (isa(tsSource, 'TimeSeries')) || (isstruct(tsSource) && isfield(tsSource, 'tfSamples'))
   fhExtractionFunc = @(tsSource, vnPixels, vnFrames)tsSource.tfSamples(vnFrames, :, :);
else
   fhExtractionFunc = @(tsSource, vnPixels, vnFrames)tsSource(vnFrames, :, :);
end

if (~exist('vfDataRange', 'var'))
   vfDataRange = [];
end

% -- Get stack parameters
vnStackSize = size(tsSource.tfSamples);
if (numel(vnStackSize) < 3)
   vnStackSize(end+1:3) = 1;
end
nStackLength = vnStackSize(1);
vnFrameSize = vnStackSize(2:3);

% - Make a videofig and show the first frame
fhRedraw = @(n, hFig, hAxes)(PlotFrame(hAxes, tsSource, n, fhExtractionFunc, vnFrameSize, vfDataRange, sRegions));
hFigure = videofig(  nStackLength, ...
   fhRedraw, ...
   30);
% fhRedraw(1);

% - Remove return argument, if not requested
if (nargout == 0)
   clear hFigure;
end

% --- END of Scrub2DTS FUNCTION ---

   function PlotFrame(hAxes, tsSource, nFrame, fhExtractionFunc, vnFrameSize, vfDataRange, sRegions)
      
      % - Extract each requested channel
      imRGB = zeros([vnFrameSize([1 2]) 3]);
      
      % - Extract frame from the stack
      tfFrame = reshape(fhExtractionFunc(tsSource, ':', nFrame), vnFrameSize(1), vnFrameSize(2), []);
      
      nNumChannels = size(tfFrame, 3);
      for (nChannel = 1:nNumChannels)
         mfThisFrame = tfFrame(:, :, nChannel);
                  
         % - Normalise frame within mask
         if (isempty(vfDataRange))
            mfThisFrame = double(mfThisFrame) - nanmin(double(mfThisFrame(:)));
            mfThisFrame = uint8(mfThisFrame ./ nanmax(mfThisFrame(:)) * 255);
            %       mfThisFrame(~mbDataMask) = 0;
         else
            mfThisFrame = double(mfThisFrame) - nanmin(vfDataRange);
            mfThisFrame = mfThisFrame ./ abs(diff(vfDataRange));
            %       mfThisFrame(~mbDataMask) = 0;
            mfThisFrame(mfThisFrame < 0) = 0;
            mfThisFrame(mfThisFrame > 1) = 1;
            mfThisFrame = uint8(mfThisFrame * 255);
         end
         
         % - Assign colour map
         if (nNumChannels == 1)
            imThisRGB = ind2rgb(mfThisFrame, gray(256));
         elseif (nChannel == 1)
            imThisRGB = ind2rgb(mfThisFrame, green);
         elseif (nChannel == 2)
            imThisRGB = ind2rgb(mfThisFrame, red);
         else
            imThisRGB = ind2rgb(mfThisFrame, gray(256));
            imThisRGB = imThisRGB ./ numel(vnChannels);
         end
         
         % - Mix channels
         imRGB = imRGB + imThisRGB;
      end
      
      % - Draw image
      cla(hAxes);
      image(imRGB, 'Parent', hAxes);
      axis equal tight off;
      
      % - Add some information
      strTitle = sprintf('%.2f s of %.2f s', tsSource.vtTimeTrace(nFrame), tsSource.vtTimeTrace(end));
      text(5, 5, strTitle, 'Color', 'white', 'Parent', hAxes);
      
      % - Label the regions
      if (~isempty(sRegions))
         hold on;
         contour(hAxes, (labelmatrix(sRegions) > 0), .5, 'LineWidth', 1);
         hold off;
      end
      
   end

   function [mnRedCMap] = red
      
      mnRedCMap = zeros(256, 3);
      mnRedCMap(:, 1) = linspace(0, 1, 256);
      
   end

   function [mnGreenCMap] = green
      
      mnGreenCMap = zeros(256, 3);
      mnGreenCMap(:, 2) = linspace(0, 1, 256);
      
   end

end
% --- END of Scrub2DTS.m ---
