function varargout = TimeShiftTS(varargin)

% TimeShiftTS - FUNCTION Shift the time base of a time series
%
% Usage: tsTimeShifted = TimeShiftTS(tsSource, tTimeShift)
%
% 'tsSource' is a time series. 'tTimeShift' is a scalar time shift to apply
% to 'tsSource.' 'tsTimeShifted' will be a copy of 'tsSource', but with the
% time base shifted by the offset 'tTimeShift'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

TS_deprecated('timeshift');

[varargout{1:nargout}] = timeshift(varargin{:});

% --- END of TimeShiftTS.m ---
