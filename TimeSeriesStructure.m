function sTS = TimeSeriesStructure(varargin)

% TimeSeriesStructure - FUNCTION Create a time series structure
%
% Usage: sTS = TimeSeriesStructure(vtTimeTrace, tfSamples <, strSampleType, vfSampleRange, fhInterpolation, vtTimeBounds>)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 2nd September, 2014

TS_deprecated('TimeSeries');

sTS = TimeSeries(varargin{:});


%% -- Old interpolation functions, included for backwards compatilbility for loading time series structure objects

% TSI_Linear - Interpolation FUNCTION
   function tfInterpolated = TSI_Linear(sTS, vtInterpolationTimes) %#ok<DEFNU>
      % - Short-cut for empty time series
      if isempty(sTS.vtTimeTrace)
         tfInterpolated = nan(numel(vtInterpolationTimes), 1);
         return;
      end
      
      % - Find time samples preceeding interpolation times
      [vtInterpolationTimes, vnInterpolantOrder] = sort(reshape(vtInterpolationTimes, [], 1));
      vnPreviousSample = reshape(sTS.fhFindPreviousSample(sTS.vtTimeTrace, vtInterpolationTimes), [], 1);
      vbAcceptInterpolation = ~isnan(vnPreviousSample);
      vbExactSample = false(size(vbAcceptInterpolation));
      vbExactSample(vbAcceptInterpolation) = sTS.vtTimeTrace(vnPreviousSample(vbAcceptInterpolation)) == vtInterpolationTimes(vbAcceptInterpolation);
      
      % - Pre-allocate samples
      vnSampleSize = sTS.samplesize;
      vnSampleSize(1) = numel(vtInterpolationTimes);
      tfInterpolated = nan(vnSampleSize);
      
      % - Catch interpolation times which are exactly time points of the source time series
      tfInterpolated(vbExactSample, :) = sTS.tfSamples(vnPreviousSample(vbExactSample), :);
      vbPerformInterpolation = vbAcceptInterpolation & ~vbExactSample;
      
      % - Interpolate samples when necessary
      if any(vbPerformInterpolation)
         vtPreviousTime = sTS.vtTimeTrace(vnPreviousSample(vbPerformInterpolation));
         vtNextTime = sTS.vtTimeTrace(vnPreviousSample(vbPerformInterpolation)+1);
         vfInterpolationFactors = (vtInterpolationTimes(vbPerformInterpolation) - vtPreviousTime) ./ ...
            (vtNextTime - vtPreviousTime);
         
         % - Interpolate samples
         tfNextSample = sTS.tfSamples(vnPreviousSample(vbPerformInterpolation)+1, :);
         tfPreviousSample = sTS.tfSamples(vnPreviousSample(vbPerformInterpolation), :);
         tfInterpolated(vbPerformInterpolation, :) = bsxfun(@times, vfInterpolationFactors, ...
            (tfNextSample - tfPreviousSample)) + tfPreviousSample;
      end
      
      % - Remove non-accepted interpolants
      tfInterpolated(~vbAcceptInterpolation, :) = nan;
      
      % - Reorder samples
      tfInterpolated(vnInterpolantOrder, :) = tfInterpolated(:, :);
   end

% TSI_ZeroOrderHold - Interpolation FUNCTION
   function tfInterpolated = TSI_ZeroOrderHold(sTS, vtInterpolationTimes) %#ok<DEFNU>
      % - Short-cut for empty time series
      if isempty(sTS.vtTimeTrace)
         tfInterpolated = nan(numel(vtInterpolationTimes), 1);
         return;
      end
      
      % - Find time samples preceeding interpolation times
      [vtInterpolationTimes, vnInterpolantOrder] = sort(reshape(vtInterpolationTimes, [], 1));
      vnPreviousSample = reshape(TS_FindPreviousSample(sTS.vtTimeTrace, vtInterpolationTimes), [], 1);
      vbAcceptInterpolation = ~isnan(vnPreviousSample);
      vbExactSample = false(size(vbAcceptInterpolation));
      vbExactSample(vbAcceptInterpolation) = sTS.vtTimeTrace(vnPreviousSample(vbAcceptInterpolation)) == vtInterpolationTimes(vbAcceptInterpolation);
      
      % - Pre-allocate samples
      vnSampleSize = sTS.samplesize;
      vnSampleSize(1) = numel(vtInterpolationTimes);
      tfInterpolated = nan(vnSampleSize);
      
      % - Catch interpolation times which are exactly time points of the source time series
      tfInterpolated(vbExactSample, :) = sTS.tfSamples(vnPreviousSample(vbExactSample), :);
      vbPerformInterpolation = vbAcceptInterpolation & ~vbExactSample;
      
      % - Interpolate samples
      if any(vbPerformInterpolation)
         tfInterpolated(vbPerformInterpolation, :) = sTS.tfSamples(vnPreviousSample(vbPerformInterpolation), :);
      end
      
      % - Reorder samples
      tfInterpolated(vnInterpolantOrder, :) = tfInterpolated;
   end

end

% --- END of TimeSeriesStructre.m ---
