function tsSourceShifted = SampleShiftTS(varargin)

% SampleShiftTS - FUNCTION Shift the samples of a time series, by a fixed amount or according to another time series
%
% Usage: tsSourceShifted = SampleShiftTS(tsSource, fShift)
%        tsSourceShifted = SampleShiftTS(tsSource, tsShift)
%
% 'tsSource' is a continuous time series. 'fShift' is a scalar offset to
% apply to the samples in 'tsSource'. Alternatively, a second time series
% 'tsShift' can be provided, in which case 'tsSource' will be shifted by
% the value of 'tsShift' at the corresponding time point. 'tsShift' must be
% a unitary continuous time series.
%
% 'tsSourceShifted' will be a copy of 'tsSource', with the sample values
% shifted appropriately.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

TS_deprecated('+ or plus');

tsSourceShifted = sampleshift(varargin{:});

% --- END of SampleShiftTS.m ---
