function [bContainsEvent] = ContainsEventTS(tsEvent, varargin)

% ContainsEventTS - FUNCTION Test whether an event time series contains an event
%
% Usage: [bContainsEvent] = ContainsEventTS(tsEvent, vtTestTime)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 18th August, 2015

TS_deprecated('containsevent');

bContainsEvent = tsEvent.containsevent(varargin{:});

% --- END of ContainsEventTS.m ---