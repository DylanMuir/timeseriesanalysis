function [vbRejectH0, vbAcceptH0, vbUndecided, mfCI, vnk] = ...
   MMCTest(fhMCSample, nm, fAlpha, nc, nk_max, fEpsilon, fhCI, fhNeta, fhMCHTest)

% MMCTest - FUNCTION Safely perform multiple Monte Carlo tests, controlling false discovery rate
%
% Usage: [vbRejectH0] = MMCTest(fhMCSample, nm)
%        [vbRejectH0, vbAcceptH0, vbUndecided, mfCI, vnk] = ...
%           MMCTest(fhMCSample, nm <, fAlpha, nc, nk_max, fEpsilon, fhCI, fhNeta, fhMCHTest>)
%
% --- Overview
%
% This function performs simultaneous multiple Monte Carlo tests to
% estimate significance over a set of hypotheses, while controlling either
% the false discovery rate or the familywise error rate. The algorithm is
% an implementation of Gandy & Hahn 2014 [1]; notation closely follows this
% publication.
%
%
% --- Basic usage
%
% 'fhMCSample' is a Monte Carlo sampling function (either with or without
% replacement), with the signature
%    mbExceedTestStat = @fhMCSample(vnHypotheses, nNumSamples)
%
% This function must return an [M x N] matrix, where each element
% is a single sample from one hypothesis 'vnHypothesis(m)' under test. Each
% element is a boolean variable that indicates whether the MC resampled
% test statistic for the corresponding hypothesis was equal to or more
% extreme that the observed test statistic for that hypothesis. I.e.,
% 'mbExceedTestStat' is equivalent to X_ij in [1]. Sampling can be
% performed with or without replacement, as desired, and is controlled
% entirely wihtin 'fhMCSample'. Elements of 'vnHypotheses' are [1..nm].
%
% 'nm' is a scalar integer specifying the number of hypotheses under test.
%
% 'vbRejectH0' will be a [1 x nm] boolean vector, indicating whether H0
% should be rejected for the corresponding hypothesis. By default, the test
% will be performed at a threshold of 'fAlpha' = 0.05.
%
% The optional argument 'fAlpha' can be used to specify the test
% significance threshold.
%
%
% --- Return arguments
%
% 'vbRejectH0', 'vbAcceptH0' and 'vbUndecided' are [1 x nm] boolean
% vectors, specifying whether H0 should be rejected, accepted, or could not
% be decided after a fixed number of samples at the 'fAlpha' significance
% level.
%
% 'mfCI' is a [2 x nm] matrix, containing confidence intervals for P for
% each hypothesis. Each [2 x 1] column vector is ['fUpperPCI'; 'fLowerPCI'].
%
% 'vnk' is a [1 x nm] integer vector, containing the number of samples
% taken from each corresponding hypothesis.
%
%
% --- Example usage
%
% % - Construct a hypothesis testing function, with a true P value of 0.06
% >> fhMCSample = @(vnH, nNS)rand(numel(vnH), nNS) < 0.06;
%
% % - Perform a test with two hypotheses, with default parameters (i.e.
% fAlpha = 0.05).
% >> [vbRejectH0, vbAcceptH0, vbUndecided, mfCI, vnk] = MMCTest(fhMCSample, 2)
%
% vbRejectH0 =
%      0     0
% 
% vbAcceptH0 =
%      1     1
% 
% vbUndecided =
%      0     0
% 
% mfCI =
%     0.0520    0.0488
%     0.0801    0.0769
% 
% vnk =
%     5566      5566
%
% H0 is correctly accepted for both hypotheses at fAlpha = 0.05. 'vnk'
% indicates that 5566 Monte Carlo samples were drawn for each hypothesis.
% Both confidence intervals ('mfCI') contain the true P value of 0.06.
%
% % - Perform a test with fAlpha = 0.061
% >> [vbRejectH0, ~, ~, mfCI, vnk] = MMCTest(fhMCSample, 2, 0.061)
% vbRejectH0 =
%      1     1
% 
% mfCI =
%     0.0594    0.0592
%     0.0610    0.0606
% 
% vnk =
%     2872983   2872983
%
% H0 is correctly rejected for both hypotheses at fAlpha = 0.061. Many more
% Monte Carlo samples were required to reach this decision, since 'fAlpha'
% was close to the true P value. Both confidence intervals contain the true
% P value of 0.06.
%
%
% --- Optional arguments
%
% 'nc' is an integer scalar > 0, defining the maximum number of hypotheses
% to leave undecided. By default 'nc' = 0; i.e. all hypotheses must be
% decided upon.
%
% 'nk_max' is an integer scalar > 0, defining the maximum number of samples
% to take in total over all hypotheses. The algorithm will stop at this
% point, and return the decisions it has made until that time, as well as
% indicating which hypotheses are left undecided. By default, the algorithm
% will continue until the 'nc' criterion is met (i.e. 'nk_max' = inf).
%
% 'fEpsilon' is a real scalar > 0, which determines the coverage
% probability of the P value confidence intervals used to decide on the set
% of hypotheses. This value is a limit on the probability of making an
% incorrect classification (see [1]).
%
% 'bUseParfor' is a boolean flag that indicates whether a 'parfor'
% construct should be used to evaluate a set of hypotheses in parallel. By
% default, a serial 'for' construct will be used. This is faster when
% sampling is not computationally intensive. If calls to 'fhMCSample' are
% slow, then set 'bUseParfor' to true.
%
% 'fhCI' is a function handle that returns a confidence interval for P
% values, for the current iteration. It can be an arbitrary function with
% signature
%    vfCI = @fhCI(nS, nK, nDelta, fhNeta, nm);
%
% or else the string 'ClopperPearsonCI'. If not provided, Clopper-Pearson
% confidence intervals are used by default (see [1]).
%
% 'fhNeta' is a function handle that defines the resampling risk spending
% sequence. It can be an arbitrary function with the signature
%    fNeta_n = fhNeta(nK, fEpsilon)
%
% If not provided, a default spending sequence of fEpsilon .* nK ./ (nK + 1000)
% will be used (see [1]).
%
% 'fhMCHTest' is a function handle that performs a multiple-comparisons
% corrected hypothesis test over a set of hypotheses. It must have the
% signature
%    vbRejectH0 = @fhMCHTest(vfP, fAlpha)
%
% or else one of {'BenjaminiHochberg', 'HolmBonferroni'}. If not provided,
% the default 'BenjaminiHochberg' test will be used.
%
% --- References
%
% [1] Gandry & Hahn 2014, "MMCTest - A Safe Algorithm for Implementing
% Multiple Monte Carlo Tests". Scandinavian J. of Statistics 41, pp1083--1101.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 23rd February, 2015


%% -- Defaults

% - Default spending sequence function
DEF_fhNeta = @(nK, fEpsilon)(fEpsilon .* nK ./ (nK + 1000));

% - Default P value confidence interval function
DEF_fhCI = @ClopperPearsonCI;

% - Default multipl-comparisons corrected hypothesis test
DEF_fhMCHTest = @BenjaminiHochberg;

% - Default maximum number of hypotheses to classify
DEF_nc = 0;

% - Default maxmimum number of iterations
DEF_nk_max = inf;

% - Default starting number of samples to draw per hypothesis
DEF_nDelta_0 = 10;

% - Default geometric growth rate for number of samples
DEF_fa = 1.25;

% - Default Alpha for significance testing
DEF_fAlpha = 0.05;

% - Default Epsilon for false classification risk limit
DEF_fEpsilon = 1/1000;

% - Default parallel options
DEF_bUseParfor = false;


%% - Check arguments

if (nargin < 2)
   help MMCTest;
   error('MMCTest:Usage', ...
      '*** MMCTest: Incorrect usage.');
end

% - Check sampling function handle
if (~isa(fhMCSample, 'function_handle')) || (nargin(fhMCSample) < 2)
   error('MMCTest:Arguments', ...
      '*** MMCTest: ''fhMCSample'' must be a function handle that accepts 2 arguments. See documentation.');
end

% - Check confidence interval function handle
if (~exist('fhCI', 'var') || isempty(fhCI))
   fhCI = DEF_fhCI;
end

if (ischar(fhCI))
   fhCI = str2func(fhCI);
end

if (~isa(fhCI, 'function_handle')) || (nargin(fhCI) < 5)
   error('MMCTest:Arguments', ...
      '*** MMCTest: ''fhCI'' must be a function handle that accepts 5 arguments. See documentation.');
end

% - Check neta function handle
if (~exist('fhNeta', 'var') || isempty(fhNeta))
   fhNeta = DEF_fhNeta;
end

if (~isa(fhNeta, 'function_handle')) || (nargin(fhNeta) < 2)
   error('MMCTest:Arguments', ...
      '*** MMCTest: ''fhNeta'' must be a function handle that accepts 2 arguments. See documentation.');
end

% - Check multiple comparisons test function handle
if (~exist('fhMCHTest', 'var') || isempty(fhMCHTest))
   fhMCHTest = DEF_fhMCHTest;
end

if (ischar(fhMCHTest))
   fhMCHTest = str2func(fhMCHTest);
end

if (~isa(fhMCHTest, 'function_handle')) || (nargin(fhMCHTest) < 2)
   error('MMCTest:Arguments', ...
      '*** MMCTest: ''fhMCHTest'' must be a function handle that accepts 2 arguments. See documentation.');
end

if (~exist('fAlpha', 'var') || isempty(fAlpha))
   fAlpha = DEF_fAlpha;
end

if (~exist('nc', 'var') || isempty(nc))
   nc = DEF_nc;
end

if (~exist('nk_max', 'var') || isempty(nk_max))
   nk_max = DEF_nk_max;
end

if (~exist('fEpsilon', 'var') || isempty(fEpsilon))
   fEpsilon = DEF_fEpsilon;
end

if (~exist('bUseParfor', 'var') || isempty(bUseParfor))
   bUseParfor = DEF_bUseParfor;
end

nDelta_0 = DEF_nDelta_0;
fa = DEF_fa;

% - Pass epsilon parameter to fhNeta
fhNeta = @(nK)fhNeta(nK, fEpsilon);



%% - Main algorithm

% - Initialise algorithm
nDelta = nDelta_0;
vbRejectH0Lower = true(1, nm);
vbRejectH0Upper = false(1, nm);
vbStillClassify = true(1, nm);
mfCI = [zeros(1, nm); ones(1, nm)];
vnS = zeros(1, nm);
vnk = zeros(1, nm);

% - Loop until stopping criteria are met
while (nnz(vbStillClassify) > nc) && (sum(vnk) <= nk_max)
   % - Identify undecided hypotheses
   vnHypToTest = find(vbStillClassify);
   vnk(vnHypToTest) = vnk(vnHypToTest) + nDelta;
   
   % - Get MC samples for remaining hypotheses
   mbX_ij = fhMCSample(vnHypToTest, nDelta);
   
   % - Update partial sum and confidence intervals
   vnS(vnHypToTest) = vnS(vnHypToTest) + sum(mbX_ij, 2)';
   for (nHypIndex = vnHypToTest)
      vfThisCI = fhCI(vnS(nHypIndex), vnk(nHypIndex), nDelta, fhNeta, nm);
      vfLastCI = mfCI(:, nHypIndex);
      vfLastCI(1) = max(vfThisCI(1), vfLastCI(1));
      vfLastCI(2) = min(vfThisCI(2), vfLastCI(2));
      mfCI(:, nHypIndex) = vfLastCI;
   end
   
   % - Increase Monte Carlo sampling block size
   nDelta = floor(fa .* nDelta);
   
   % - Perform multiple-comparison corrected tests to classify hypotheses
   vbRejectH0Lower = fhMCHTest(mfCI(1, :), fAlpha);
   vbRejectH0Upper = fhMCHTest(mfCI(2, :), fAlpha);
   vbStillClassify = xor(vbRejectH0Lower, vbRejectH0Upper);
end

% - Perform final classification of hypotheses
vbRejectH0 = vbRejectH0Upper;
vbAcceptH0 = ~vbRejectH0Lower & ~vbRejectH0Upper;
vbUndecided = vbStillClassify;

% --- END of MMCTest FUNCTION ---


%% --- Utility functions

% ClopperPearsonCI - FUNCTION Compute Clopper-Pearson confidence intervals
% on P values. Notation is as given in [1].

function vfCI = ClopperPearsonCI(nS, nk, nDelta, fhNeta, nm)

% - Pre-calculate rho_k
fRho_k = (fhNeta(nk) - fhNeta(nk - nDelta)) ./ (2*nm);

switch nS
   case 0
      % S == 0
      vfCI = [0 1 - fRho_k.^(1/nk)];
      
   case nk
      % S == k
      vfCI = [fRho_k.^(1/nk) 1];
      
   otherwise
      % 0 < S < k
      vfCI = sort([1-betainv(fRho_k, nk-nS, nS+1)  1-betainv(1-fRho_k, nk+1-nS, nS)]);
end


% BenjaminiHochberg - FUNCTION Compute H0 rejections with FDR
% multiple-comparisons correction

function vbRejectH0 = BenjaminiHochberg(vfP, fAlpha)

% - Order P values
[vfP, vnSortIndices] = sort(vfP);

% - Construct comparison vector
nm = numel(vfP);
vfTest = (1:nm) ./ nm .* fAlpha;

% - Find threshold element
nLastK = find(vfP <= vfTest, 1, 'last');
nLastK(isempty(nLastK)) = 0;              % - Set to zero, if none found (i.e. accept all H0)

% - Construct hypothesis classification vector and re-order
vbRejectH0 = [true(1, nLastK) false(1, nm-nLastK)];
vbRejectH0(vnSortIndices) = vbRejectH0;


% HolmBonferroni - FUNCTION Compute H0 rejections with FWER
% multiple-comparisons correction

function vbRejectH0 = HolmBonferroni(vfP, fAlpha) %#ok<DEFNU>

% - Order P values
[vfP, vnSortIndices] = sort(vfP);

% - Construct comparison vector
nm = numel(vfP);
vfTest = fAlpha ./ (nm + 1 - (1:nm));

% - Find threshold element
nFirstK = find(vfP > vfTest, 1, 'first');
nFirstK(isempty(nFirstK)) = nm+1;         % - Set to 'nm', if none found (i.e. reject all H0)

% - Construct hypothesis classification vector and re-order
vbRejectH0 = [true(1, nFirstK-1) false(1, nm-nFirstK+1)];
vbRejectH0(vnSortIndices) = vbRejectH0;

% --- END of MMCTest.m ---

