function tsMean = nanmeanTS(varargin)

% nanmeanTS - FUNCTION Compute the mean (amplitude-wise) of several time series, ignoring NaNs
%
% Usage: tsMean = nanmeanTS(ts1 <, ts2, ...>)
%
% nanmeanTS will compute the mean of several time series. Unitary and
% non-unitary time series can be supplied. The time base of the mean will be
% the interleaved time bases of the source time series. Time series will be
% interpolated, when necessary, according to the interpolation function set
% for each time series. This function ignores NaNs, similar in behaviour to
% nanmean.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 20th April, 2015

TS_deprecated('nanmean');

tsMean = nanmean(varargin{:});

% --- END of nanmeanTS.m ---