function tsConvolved = ConvolveTS(tsSource, fhFunction, strMethod, vtFunctionWindow)

% ConvolveTS - FUNCTION Convolve a time series with an arbitrary function
%
% Usage: tsConvolved = ConvolveTS(tsSource, fhFunction <, strMethod, vtFunctionWindow>)
%
% 'tsSource' must be a continous or event time series.
% 'fhFunction' must have the signature [mfSamples] = @(mtTime)
%
% where 'mtTime' will be a matrix of time points and 'mfSamples' will be a
% matrix of corresponding samples. 'fhFunction' must return the value of
% the desired function to convolve with for each corresponding time point.
%
% The optional argument 'strMethod' defines what method to use for
% convolution. Presently, only 'fast' is supported.
%
% The argument 'vtFunctionWindow' defines a set of time points over which
% to evaluate 'fhFunction', for each event. This is particularly important
% for convolving 'event' time series, and must be provided for these time
% series. It is also used by the 'exact' convolution method. If not
% provided, ConvolveTS will attempt to work out a reasonable function
% window.
%
% Note that for event time series, each event will be considered to
% represent a unitary impulse.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 5th September, 2014

% -- Defaults and parameters

cstrMethods = {'fast', 'f', 'exact', 'e'};
DEF_strMethod = 'fast';

% -- Check arguments

if (nargin < 2)
   help ConvolveTS;
   error('TimeSeries:Usage', ...
      '*** ConvolveTS: Incorrect usage.');
end

if (~tsSource.testsampletype({'continuous', 'event'}))
   error('TimeSeries:Usage', ...
      '*** ConvolveTS: ''tsSource'' must be a continuous or event time series.');
end

if (~exist('strMethod', 'var') || isempty(strMethod))
   strMethod = DEF_strMethod;
end

if (~any(cellfun(@(c)strcmpi(c, strMethod), cstrMethods)))
   error('TimeSeries:Usage', ...
      ['*** ConvolveTS: ''cstrMethod'' must be one of {' sprintf('%s ', cstrMethods{:}) '}']);
end


if (~exist('vtFunctionWindow', 'var'))
   vtFunctionWindow = [];
end

% - Check for an empty time series
if (isempty(tsSource))
    tsConvolved = TimeSeries([], []);
    return;
end


% -- Perform convolution

switch lower(strMethod)
   case {'exact', 'e'}
      %% --- ONLY THE FAST METHOD IS CURRENTLY IMPLEMENTED ---
      if (tsSource.testsampletype('continuous'))
         tsConvolved = CTS_ConvolveContinuousExact(tsSource, fhFunction, vtFunctionWindow);
         
      elseif (tsSource.testsampletype('event'))
         if (isempty(vtFunctionWindow))
            error('TimeSeries:Arguments', ...
               '*** ConvolveTS: ''vtFunctionWindow'' must be provided for event time series.');
         end
         
         tsConvolved = CTS_ConvolveEvent(tsSource, fhFunction, vtFunctionWindow);
      end
      
      
   case {'fast', 'f'}
      %% --- Use the fast method for estimating convolution
      if (tsSource.testsampletype('continuous'))
         tsConvolved = CTS_ConvolveContinuousFast(tsSource, fhFunction, vtFunctionWindow);
         
      elseif (tsSource.testsampletype('event'))
         if (isempty(vtFunctionWindow))
            error('TimeSeries:Arguments', ...
               '*** ConvolveTS: ''vtFunctionWindow'' must be provided for event time series.');
         end
         
         tsConvolved = CTS_ConvolveEvent(tsSource, fhFunction, vtFunctionWindow);
      end
      
      
   otherwise
      % -- This should not be possible
      error('TimeSeries:Usage', ...
         '*** ConvolveTS: Unknown convolution method. This error should not be possible...');
end

end

% CTS_ConvolveContinuousFast - FUNCTION Perform fast convolution for a continuous time series
function tsConvolved = CTS_ConvolveContinuousFast(tsSource, fhFunction, ~)

w = warning('off', 'MATLAB:structOnObject');
tsSource = struct(tsSource);
warning(w);

% - Defaults
DEF_nNumTimePointsFast = 2^nextpow2(2e5);
DEF_nChunkSizeMb = 1024;

% - Estimate reasonable sampling time
vtOriginalTimeTrace = tsSource.vtTimeTrace;
vtLimTime = tsSource.vtTimeBounds;

tSourceResolution = nanmedian(diff(vtOriginalTimeTrace));
nLengthPeriodicConvTrace = min(2*round(diff(vtLimTime)./tSourceResolution), DEF_nNumTimePointsFast);
tConvResolution = abs(2*diff(vtLimTime)) ./  nLengthPeriodicConvTrace;

if (tConvResolution > tSourceResolution) %#ok<BDSCI>
   warning('TimeSeries:ConvolutionResolution', ...
           '--- ConvolveTS: Possible loss of temporal resolution in convolution [tConvResolution / tSourceResolution = %.2g]. Try using ''exact'' method.', ...
           tConvResolution / tSourceResolution);
end

vtPeriodicConvTimeTrace = shiftdim(linspace(vtLimTime(2)-diff(vtLimTime), vtLimTime(2), nLengthPeriodicConvTrace));
vtPeriodicWindowTrace = shiftdim(linspace(-diff(vtLimTime), diff(vtLimTime), nLengthPeriodicConvTrace));
% [~, nZeroPoint] = min(abs(vtPeriodicConvTimeTrace));

% - Evaluate convolution function
vfConvFunction = reshape(fhFunction(vtPeriodicWindowTrace), [], 1);
vbFuncNans = isnan(vfConvFunction);
vfConvFunction(vbFuncNans) = 0;
vfConvFunction = vfConvFunction .* tConvResolution;
vfConvFunctionFFT = fft(vfConvFunction, 2^nextpow2(nLengthPeriodicConvTrace));

% - Check that the time window was of reasonable length
if (all(abs(vfConvFunction) < 2*eps))
    error('TimeSeries:ConvolutionResolution',...
        '*** ConvolveTS: Convolution function is all-zero at sampling resolution.')
end

% - Retain source samples
vnSampleDims = size(tsSource.tfSamples);
vnSampleDims(1) = 1;
tfSourceSamples = tsSource.tfSamples;
tsSource.tfSamples = [];

% - Duplicate source time series
switch (class(tsSource.tfSamples))
   case {'MappedTensor', 'TensorStack'}
      warning('TimeSeries:DiskStorage', ...
         '--- ConvolveTS: Warning: Creating ''MappedTensor'' for temporary storage of convolution result.');
      
      % - Duplicate time series
      tsConvolved = tsSource;
      
      % - Replace samples with new temporary disk storage
      strSampleClass = class(tsSource.tfSamples(1));
      tsConvolved.tfSamples = MappedTensor(size(tsSource.tfSamples), 'Class', strSampleClass);
      
   otherwise
      tsConvolved = tsSource;
end

% - Work out if we need to split the source time series into chunks
nNumChunks = min(1, ceil(prod(vnSampleDims) * nLengthPeriodicConvTrace / (DEF_nChunkSizeMb * 1024 * 1024 / 8)));

vnChunkBounds = round(linspace(1, prod(vnSampleDims), nNumChunks+1));
vnChunkBounds(1) = 0;
vnChunkBounds(end) = prod(vnSampleDims);
mnChunkLims = [vnChunkBounds(1:end-1)'+1 vnChunkBounds(2:end)'];

% - Loop over time series in chunks, to limit memory usage
for (nChunk = 1:nNumChunks)
   % - Extract and resample these time series
   vnSeriesIndices = mnChunkLims(nChunk, 1):mnChunkLims(nChunk, 2);
   tsSourcePeriodic = tsSource;
   tsSourcePeriodic.tfSamples = tfSourceSamples(:, vnSeriesIndices);
   tsSourcePeriodic = TimeSeries(tsSourcePeriodic).resample(vtPeriodicConvTimeTrace);

   % - Perform convolution using fft
   tfSamples = tsSourcePeriodic.tfSamples;
   % nFirstRealSample = find(~any(isnan(tfSamples(:, :)), 2), 1, 'first');
   tbNanSample = isnan(tfSamples);
   tfSamples(tbNanSample) = 0; % - Ensure 'nan's don't contaminate FFT
   tfSamplesFFT = fft(tfSamples, 2^nextpow2(nLengthPeriodicConvTrace), 1);
   tfConvolvedSamples = ifft(bsxfun(@times, tfSamplesFFT, vfConvFunctionFFT));

   % - Renormalise convolution distortion produced by zero nans
   tfNanCorrection = ones(size(tfSamples), 'like', tfSamples);
   tfNanCorrection(tbNanSample) = 0;
   tfNanCorrectionFFT = fft(tfNanCorrection, 2^nextpow2(nLengthPeriodicConvTrace), 1);
   tfConvolvedNaNSamples = ifft(bsxfun(@times, tfNanCorrectionFFT, vfConvFunctionFFT));
   tfConvolvedSamples = tfConvolvedSamples ./ tfConvolvedNaNSamples;
   
   % - Renormalise convolution distortion produced by function nans
   tfNanCorrection = ones(size(vfConvFunction), 'like', vfConvFunction);
   tfNanCorrection(vbFuncNans) = 0;
   tfNanCorrectionFFT = fft(tfNanCorrection, 2^nextpow2(nLengthPeriodicConvTrace), 1);
   tfConvolvedNaNSamples = ifft(bsxfun(@times, tfSamplesFFT, tfNanCorrectionFFT));
   tfConvolvedSamples = tfConvolvedSamples ./ tfConvolvedNaNSamples;   
   
   % - Trim convolution
   nNumFFTSamples = size(tfConvolvedSamples, 1);
   nNumOrigSamples = size(tfSamples, 1);
   nStartTimePoint = nNumFFTSamples/2+1;
   nExtraDross = nNumFFTSamples - nNumOrigSamples;
   vnConvWindow = [nStartTimePoint-nExtraDross/2:nNumFFTSamples, 1:nStartTimePoint-nExtraDross-nExtraDross/2];
   vnConvWindow = vnConvWindow(1:nNumOrigSamples);
   tfConvolvedSamples = tfConvolvedSamples(vnConvWindow, :);
   
   % - Restore NaN samples
   tfConvolvedSamples(tbNanSample) = nan;
   
   % - Resample back to original time base
   tsConvolvedChunk = TimeSeries(vtPeriodicConvTimeTrace, tfConvolvedSamples, 'continuous', tsSource.vfSampleRange, tsSource.fhInterpolation);
   tsConvolvedChunk = tsConvolvedChunk.resample(tsSource.vtTimeTrace);
   
   % - Place samples into return time series
   tsConvolved.tfSamples(:, vnSeriesIndices) = tsConvolvedChunk.tfSamples;
end

% - Convert back to TimeSeries object
tsConvolved = TimeSeries(tsConvolved);

end

% CTS_ConvolveEvent - FUNCTION Perform convolution for an event time series
function tsConvolved = CTS_ConvolveEvent(tsSource, fhFunction, vtFunctionWindow)

DEF_nChunkSizeMb = 1024;

% - Gather event times
vtEventTimes = tsSource.vtTimeTrace;

% - Split events into chunks
nNumSourceEvents = ceil((DEF_nChunkSizeMb*1024*1024/8) / numel(vtFunctionWindow)/numel(vtEventTimes));
nNumChunks = ceil(numel(vtEventTimes) / nNumSourceEvents);
vnChunkBounds = round(linspace(1, numel(vtEventTimes), nNumChunks+1));
vnChunkBounds(1) = 0;
vnChunkBounds(end) = numel(vtEventTimes);
mnChunkLims = [vnChunkBounds(1:end-1)'+1 vnChunkBounds(2:end)'];

% - Loop over chunks
vtFunctionWindow = reshape(vtFunctionWindow, 1, []);

cmfSamples = {};
for (nChunk = nNumChunks:-1:1)
   % - Compute function window times for this chunk
   vnChunkIndices = mnChunkLims(nChunk, 1):mnChunkLims(nChunk, 2);
   mtChunkWindowTimes = bsxfun(@plus, vtFunctionWindow, vtEventTimes(vnChunkIndices));

   % - Replicate out function evaluation window
   ttWindowTimes = bsxfun(@minus, mtChunkWindowTimes, permute(vtEventTimes, [3 2 1]));

   % - Evaluate convolution function and sum
   cmfSamples{nChunk} = nansum(fhFunction(ttWindowTimes), 3);
end

% - Combine chunks
mfSamples = cat(1, cmfSamples{:});

% - Create return time series
mtWindowTimes = bsxfun(@plus, vtFunctionWindow, vtEventTimes);
[~, vnTimeOrder] = sort(mtWindowTimes(:));
tsConvolved = TimeSeries(mtWindowTimes(vnTimeOrder), mfSamples(vnTimeOrder), 'continuous', [], [], tsSource.vtTimeBounds);

end

% CTS_ConvolveContinuousFast - FUNCTION perform exact convolution for a continuous time series, with no down-sampling
function tsConvolved = CTS_ConvolveContinuousExact(tsSource, fhFunction, vtFunctionWindow)

DEF_nChunkSizeMb = 1024;

% - Estimate reasonable sampling time
vtOriginalTimeTrace = tsSource.vtTimeTrace;
vtLimTime = tsSource.vtTimeBounds;

tSourceResolution = nanmedian(diff(vtOriginalTimeTrace));

if (isempty(vtFunctionWindow))
   warning('TimeSeries:Convolve:FunctionWindow', ...
           '--- ConvolveTS: Warning: Exact convolution is faster if a function window is provided');
        
   % - Use a maximum function window
   vtFunctionWindow = [-1 1] * diff(vtLimTime);
end

% - Generate a period time trace spanning entire source time series
vtPeriodicConvTimeTrace = shiftdim(vtLimTime(1):tSourceResolution:vtLimTime(2));
nLengthPeriodicConvTrace = numel(vtPeriodicConvTimeTrace);

% - Generate a convolution function time trace
vtPeriodicWindowTrace = shiftdim(vtFunctionWindow(1):tSourceResolution:vtFunctionWindow(2));

if (numel(vtPeriodicWindowTrace) == 1)
   warning('TimeSeries:ConvolutionResolution', ...
           '--- ConvolveTS: Convolution function window is too short for sampling resolution.');
   vtPeriodicWindowTrace = vtFunctionWindow(1) + [0 tSourceResolution];
end

% - Evaluate convolution function
vfConvFunction = reshape(fhFunction(vtPeriodicWindowTrace), [], 1);
vfConvFunction = vfConvFunction .* diff(vtPeriodicWindowTrace(1:2));

% - Check that the time window was of reasonable length
if (all(abs(vfConvFunction) < 2*eps))
    error('TimeSeries:ConvolutionResolution',...
        '*** ConvolveTS: Convolution function is all-zero at sampling resolution.')
end

% - Retain source samples
vnSampleDims = tsSource.samplesize;
tfSourceSamples = tsSource.tfSamples;

w = warning('off', 'MATLAB:structOnObject');
tsSource = struct(tsSource);
warning(w);

tsSource.tfSamples = [];

% - Duplicate source time series
switch (class(tsSource.tfSamples))
   case {'MappedTensor', 'TensorStack'}
      warning('TimeSeries:DiskStorage', ...
         '--- ConvolveTS: Warning: Creating ''MappedTensor'' for temporary storage of convolution result.');
      
      % - Duplicate time series
      tsConvolved = tsSource;
      
      % - Replace samples with new temporary disk storage
      strSampleClass = class(tsSource.tfSamples(1));
      tsConvolved.tfSamples = MappedTensor(size(tsSource.tfSamples), 'Class', strSampleClass);
      
   otherwise
      tsConvolved = tsSource;
end

% - Work out if we need to split the source time series into chunks
nNumChunks = min(1, ceil(prod(vnSampleDims) * nLengthPeriodicConvTrace / (DEF_nChunkSizeMb * 1024 * 1024 / 8)));

vnChunkBounds = round(linspace(1, prod(vnSampleDims), nNumChunks+1));
vnChunkBounds(1) = 0;
vnChunkBounds(end) = prod(vnSampleDims);
mnChunkLims = [vnChunkBounds(1:end-1)'+1 vnChunkBounds(2:end)'];

% - Loop over time series in chunks, to limit memory usage
for (nChunk = 1:nNumChunks)
   % - Extract and resample these time series
   vnSeriesIndices = mnChunkLims(nChunk, 1):mnChunkLims(nChunk, 2);
   tsSourcePeriodic = tsSource;
   tsSourcePeriodic.tfSamples = tfSourceSamples(:, vnSeriesIndices);
   tsSourcePeriodic = TimeSeries(tsSourcePeriodic).resample(vtPeriodicConvTimeTrace);

   % - Perform convolution using conv
   tfSamples = tsSourcePeriodic.tfSamples;
   % nFirstRealSample = find(~any(isnan(tfSamples(:, :)), 2), 1, 'first');
   tbNanSample = isnan(tfSamples);
   tfSamples(tbNanSample) = 0; % - Ensure 'nan's don't contaminate convolution
   tfConvolvedSamples = zeros(size(tfSamples), 'like', tfSamples);
   tfConvolvedSamples(:, :) = conv2(vfConvFunction, 1, tfSamples(:, :), 'same');
   
   % - Renormalise convolution distortion produced by zero nans
   tfNanCorrection = ones(size(tfSamples), 'like', tfSamples);
   tfNanCorrection(tbNanSample) = 0;
   tfConvolvedNaNSamples = zeros(size(tfSamples), 'like', tfSamples);
   tfConvolvedNaNSamples(:, :) = conv2(vfConvFunction, 1, tfNanCorrection(:, :), 'same');
   tfConvolvedSamples = tfConvolvedSamples ./ tfConvolvedNaNSamples;
   
   % - Restore NaN samples
   tfConvolvedSamples(tbNanSample) = nan;
   
   % - Resample back to original time base
   tsConvolvedChunk = TimeSeries(vtPeriodicConvTimeTrace, tfConvolvedSamples, 'continuous', tsSource.vfSampleRange, tsSource.fhInterpolation);
   tsConvolvedChunk = tsConvolvedChunk.resample(tsSource.vtTimeTrace);
   
   % - Place samples into return time series
   tsConvolved.tfSamples(:, vnSeriesIndices) = tsConvolvedChunk.tfSamples;
end
% convert back in timeseries
tsConvolved = TimeSeries(tsConvolved);

end

% --- END of ConvolveTS.m ---
