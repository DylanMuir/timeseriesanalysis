function tsBinned = TimeBinTS(tsSource, vtBinEdges)

% TimeBinTS - FUNCTION Perform a temporal binning of a time series (bin average)
%
% Usage: tsBinned = TimeBinTS(tsSource, tBinDuration)
%        tsBinned = TimeBinTS(tsSource, vtBinEdges)
%
% 'tsSource' is a time series to perform binning on. Only 'continuous',
% 'count' and 'event' time series are supported. 'tBinDuration' is the
% temporal width of the bin to use.
%
% 'tsBinned' will be a time series containing the binned data. If
% 'tsSource' is an 'event' time series, then 'tsBinned' will be a 'count'
% time series containing the number of events in each time bin. If
% 'tsSource' is a 'count' or 'continuous' time series, then 'tsBinned' will
% be a 'continuous' time series containing the temporal average of each
% time bin.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 8th Septmeber, 2014

TS_deprecated('timebin');

tsBinned = tsSource.timebin(vtBinEdges);

% --- END of TimeBinTS.m ---
