function vnSampleSize = GetSampleSizeTS(varargin)

% GetSampleSizeTS - FUNCTION Return the size and shape of the data associated with each time sample
%
% Usage: vnSampleSize = GetSampleSizeTS(ts)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 21st April, 2015

TS_deprecated('samplesize');

vnSampleSize = samplesize(varargin{:});

% --- END of GetSampleSizeTS.m ---