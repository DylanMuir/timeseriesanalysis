function vhOutput = phaseplot(varargin)

% phaseplot - METHOD Plot one time series versus another
%
% Usage: vhOutput = phaseplot(tsX, tsY <, tsZ >, ...)
%                   phaseplot(..., <plot options>)
%
% Plot several time series against each other. phaseplot performs either a
% two or three dimensional plot of several time series values. Provided
% time series will be resampled to the time base of the first time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 11th October, 2016

%% -- Check arguments

if (nargin == 0)
   help phaseplot;
   error('TimeSeries:Usage', ...
         '*** TimeSeries/phaseplot: Incorrect usage.');
end

% - Examine TimeSeries arguments
vbIsTimeSeries = cellfun(@istimeseries, varargin);

if (nnz(vbIsTimeSeries) > 3)
   error('TimeSeries:Arguments', ...
         '*** TimeSeries/phaseplot: Only three TimeSeries arguments may be supplied.');
end

if (nnz(vbIsTimeSeries) < 2)
   error('TimeSeries:Arguments', ...
         '*** TimeSeries/phaseplot: At least two TimeSeries arguments must be supplied.');
end

% - Extract time series arguments
ctsTimeSeries = varargin(vbIsTimeSeries);
cPlotParams = varargin(~vbIsTimeSeries);

% - Check for all unitary time series
if any(cellfun(@(ts)sum(samplesize(ts)) > 2, ctsTimeSeries))
   error('TimeSeries:Arguments', ...
         '*** TimeSeries/phaseplot: Only unitary time series must be supplied.');
end


%% - Resample time series to first time series

vtTimeTrace = ctsTimeSeries{1}.vtTimeTrace;
ctsTimeSeries(2:end) = cellfun(@(ts)resample(ts, vtTimeTrace), ctsTimeSeries(2:end), 'UniformOutput', false);


%% - Perform the plot

newplot;

switch nnz(vbIsTimeSeries)
   case 3
      vhOutput = plot3(ctsTimeSeries{1}.tfSamples, ctsTimeSeries{2}.tfSamples, ctsTimeSeries{3}.tfSamples, cPlotParams{:});
      
   case 2
      vhOutput = plot(ctsTimeSeries{1}.tfSamples, ctsTimeSeries{2}.tfSamples, cPlotParams{:});
      
   otherwise
      error('TimeSeries:Bizarre', ...
            '*** TimeSeries/phaseplot: This error should not arise.');
end

% - Clear output arguments if not requested
if (nargout == 0)
   clear vhOutput;
end

% --- END of phaseplot.m ---
