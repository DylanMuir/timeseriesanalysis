function tsDiscretized = DiscretizeTS(varargin)

% DiscretizeTS - FUNCTION Discretize a time series in sample space, according to fixed value thresholds
%
% Usage: tsDiscretized = DiscretizeTS(tsSource, vfThresholds <, tTemporalSmoothing, tMinSegmentDuration, bTakeEarlierTransitions>)
%
% 'tsSource' must be a unitary time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 16th November, 2015

tsDiscretized = DiscretiseTS(varargin{:});

% --- END of DiscretizeTS.m ---
