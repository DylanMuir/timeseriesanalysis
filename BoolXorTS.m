function [tsXorTS] = BoolXorTS(tsA, tsB)

% BoolXorTS - FUNCTION Perform a boolean XOR of two boolean time series
%
% Usage: [tsXorTS] = BoolXorTS(tsA, tsB)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

TS_deprecated('xor');

tsXorTS = xor(tsA, tsB);

% --- END of BoolXorTS.m ---
