function tsMerged = MergeTS(varargin)

% MergeTS - FUNCTION Merge (interleave) several time series
%
% Usage: tsMerged = MergeTS(tsSeries1, tsSeries2, ...)
%
% 'tsSeriesX' must all be time series with the same sampling type, the same
% number of time series and the same sample ranges.
%
% 'tsMerged' will be a time series containing all the time samples
% from 'tsSeriesX' interleaved, such that the original time bases are
% retained.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

TS_deprecated('merge');

tsMerged = merge(varargin{:});

% --- END of MergeTS.m ---
