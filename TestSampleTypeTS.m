function nSampleType = TestSampleTypeTS(varargin)

% TestSampleTypeTS - FUNCTION Test whether a time series is one of a list of allowed sample types
%
% Usage: nSampleType = TestSampleTypeTS(ts, cstrTestSampleTypes)
%
% 'nSampleType' will be 'false', if no match is found, or an index into
% 'cstrTestSampleTypes', if it matches one of the provided sample types.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th November, 2014

TS_deprecated('testsampletype');

nSampleType = testsampletype(varargin{:});

% --- END of TestSampleTypeTS.m ---
