function [fCorrCoeff, fPValuePearson] = CorrTS(varargin)

% CorrTS - FUNCTION Compute the correlation coefficient between two unitary time series
%
% Usage: [fCorrCoeff, fPValuePearson] = CorrTS(tsA, tsB)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 16th April, 2015

TS_deprecated('corr');

[fCorrCoeff, fPValuePearson] = corr(varargin{:});

% --- END of CorrTS.m ---
