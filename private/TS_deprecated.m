function TS_deprecated(strReplacement)

sStack = dbstack;
strCaller = sStack(2).name;

if (exist('strReplacement', 'var'))
   warning('TimeSeries:DeprecatedFunction', ...
      '--- Warning: The function [%s] is deprecated. Use the new class-based syntax: [%s].', ...
      strCaller, strReplacement);
else
   warning('TimeSeries:DeprecatedFunction', ...
      '--- Warning: The function [%s] is deprecated. Use the new class-based syntax.', ...
      strCaller);
end
