function tsExcised = ExciseTS(varargin)

% ExciseTS - FUNCTION Excise samples from a time series without resampling
%
% Usage: tsExcised = ExciseTS(tsSource, vtTimeLimits)
%        tsExcised = ExciseTS(tsSource, tsBoolean)
%        tsExcised = ExciseTS(..., bLeaveNaNs)
%
% 'tsSource' is a time series. 'vtTimeLimits' is a vector [tStartTime tEndTime]
% which defines a time range to excise from 'tsSource' (excluding end points).
%
% Optionally, a unitary boolean time series 'tsBoolean' can be provided as
% the second argument. In this case, all periods of time when 'tsBoolean'
% is 'true' will be excised from 'tsSource'.
%
% The optional argument 'bLeaveNaNs' determines whether samples are
% replaced with NaN, or completely excised. By default 'bLeaveNaNs' is
% 'false', indicating that samples are completely excised.
%
% 'tsExcised' will be a new time series, containing all data from the
% source time series, excluding excised portions.
%
% See also TrimTS, which keeps the data between 'vtTimeLimits'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: September 2014

TS_deprecated('excise');

tsExcised = excise(varargin{:});

% --- END of ExciseTS.m ---
