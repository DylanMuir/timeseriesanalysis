function [varargout] = find(tsSeries, varargin)

% find - FUNCTION Extract times and values from a time series (analogous to matlab 'find')
%
% Usage: [vtTimes <, vfValues>] = find(tsSeries)
%                           ... = find(tsSeries, k)
%                           ... = find(tsSeries, k, 'first')
%                           ... = find(tsSeries, k, 'last')
%                           ... = find(tsSeries, k, 'before', tTargetTime)
%                           ... = find(tsSeries, k, 'after', tTargetTime)
%
% 'tsSeries' is a unitary time series. find will find event times (in the
% case of an event time series), "true" periods (in the case of a boolean
% time series) or any samples (for other time series types).
%
% 'vtTimes' will be found times in the time series. 'vfValues' will contain
% the corresponding value of the time series at that point in time.
%
% The optional arguments 'k' and 'first'/'last' have the same sense as for
% the matlab find function.
%
% The optional arguments 'before'/'after' and 'tTargetTime' permit
% searching for events and samples relative to a given target time
% 'tTargetTime'. Note that 'tTargetTime' will be included in the results,
% if appropriate.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 18th August, 2015

% -- Check arguments

if (nargin < 1) || (nargin > 4)
   help find;
   error('TimeSeries:Usage', '*** find: Incorrect usage.');
end

% - Check for empty timeseries
if (isempty(tsSeries))
   vtTimes = [];
   vfValues = [];

else   
   if (prod(samplesize(tsSeries)) > 1)
      error('TimeSeries:Arguments', '*** find: ''tsSeries'' must be a unitary time series.');
   end
   
   if (nargin > 2)
      switch lower(varargin{2})
         case {'following', 'after'}
            if (nargin < 4) || ~isscalar(varargin{3}) || isempty(varargin{3})
               error('TimeSeries:Usage', '*** find: If ''%s'' is supplied, then ''tTargetTime'' must also be supplied.', varargin{2});
            end
            
            % - Return results after target time
            vtRange = tsSeries.vtTimeBounds;
            [varargout{1:nargout}] = find(trim(tsSeries, [varargin{3} vtRange(2)]), varargin{1}, 'first');
            return;
            
         case {'prior', 'preceding', 'preceeding', 'before'}
            if (nargin < 4) || ~isscalar(varargin{3}) || isempty(varargin{3})
               error('TimeSeries:Usage', '*** FindTS: If ''%s'' is supplied, then ''tTargetTime'' must also be supplied.', varargin{2});
            end
            
            % - Return results before target time
            vtRange = tsSeries.vtTimeBounds;
            [varargout{1:nargout}] = find(trim(tsSeries, [vtRange(1) varargin{3}]), varargin{1}, 'last');
            return;
      end
   end
   
   
   % -- Handle time series classes
   
   switch tsSeries.strSampleType
      case 'boolean'
         % - Boolean time series - find only "true" samples
         vnIndices = find(tsSeries.tfSamples == true, varargin{:});
         vtTimes = tsSeries.vtTimeTrace(vnIndices);
         vfValues = true(size(vtTimes));
         
      case 'event'
         % - Event time series - find event times
         vtTimeTrace = tsSeries.vtTimeTrace(~isnan(tsSeries.vtTimeTrace));
         [~, ~, vtTimes] = find(vtTimeTrace, varargin{:});
         vfValues = true(size(vtTimes));
         
      otherwise
         % - Continuous, categorical, count time series
         vtTimeTrace = tsSeries.vtTimeTrace;
         tfSamples = tsSeries.tfSamples;
         
         vbAcceptSample = ~isnan(tfSamples) & ~isnan(vtTimeTrace);
         vtTimeTrace = vtTimeTrace(vbAcceptSample);
         tfSamples = tfSamples(vbAcceptSample);
         
         [vnIndices, ~, vtTimes] = find(vtTimeTrace, varargin{:});
         vfValues = tfSamples(vnIndices);
   end
end

% - Assign output arguments
varargout{1} = vtTimes;

if (nargout > 1)
   varargout{2} = vfValues;
end


% --- END of find.m ---

