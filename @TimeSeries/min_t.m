function tfMin = min_t(tsSource)

% max_t - METHOD Compute the minumum sample value of a time series, over time
%
% Usage: tfMin = min_t(tsSource)
%        tfMin = tsSource.min_t()

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 8th August, 2016

tfMin = min(tsSource.tfSamples, [], 1);

% --- END of min_t.m ---
