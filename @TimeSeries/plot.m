function vhOutput = plot(tsSeries, varargin)

% plot - METHOD Plot a time series
%
% Usage: vhOutput = plot(tsSeries, ...)
%                   plot(tsSeries1, tsSeries2, ...)
%                   plot(vtsSeries, ...)
%                   plot(..., <plot options>)
%
% Plots a time series of any sample type, contained in 'tsSeries'. Series
% structures containing multiple time series add multiple traces to the
% plot (one for each series).
%
% Standard Matlab plot arguments are supported (see examples below).
%
% The hold state of the axis is respected.
%
% Examples:
% * Plot the time series contained within 'tsSeries'.
%     plot(tsSeries);
%
% * Plot the time series as a green dashed line of width 2 points.
%     plot(tsSeries, 'g:', 'LineWidth', 2);

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: September 2014

% -- Check arguments

if (nargin < 1)
   help plot;
   error('TimeSeries:Usage', ...
         '*** plot: Incorrect usage.');
end

if (numel(tsSeries) > 1)
   % - Add all series to the plot
   bHold = ishold;
   plot(combine(tsSeries), varargin{:});
   
   if (~bHold)
      hold off;
   end
   
   return;
end


% -- Make the plot

newplot;

if (tsSeries.testsampletype('event'))
   if (nargin == 1)
      varargin = {'x'};
   end
   fStemMax = ylim;
   fStemMax = fStemMax(2);
   vhOutput = stem(tsSeries.vtTimeTrace', fStemMax .* ones(size(tsSeries.vtTimeTrace))', varargin{:});

elseif (tsSeries.testsampletype('boolean'))
   bHold = ishold;
      
   % - Work out series size and which ones to plot
   nNumSeries = prod(samplesize(tsSeries));
   
   % - Get patch colours
   mfColorOrder = get(gca, 'ColorOrder');
   
   % - Get patch extent
   vfLims = ylim;
   vfPatchVals = vfLims([1 2 2 1 1]);
   
   for (nSeries = 1:nNumSeries)
      % - Get this time series to plot
      vbSeries = tsSeries.tfSamples(:, nSeries);
      
      if ((nargin > 2) && is_colourspec(varargin{1}))
         vfThisTraceColour = varargin{1};
         cExtraArgs = varargin(2:end);
      else
         nColorIndex = getappdata(gca, 'PlotColorIndex');
         if (isempty(nColorIndex))
            nColorIndex = 1;
         end
         vfThisTraceColour = mfColorOrder(nColorIndex, :);
         setappdata(gca, 'PlotColorIndex', mod(nColorIndex, size(mfColorOrder, 1))+1);
         cExtraArgs = varargin;
      end
      
      % - Loop over runs of "true"
      bInLegend = true;
      while any(vbSeries)
         % - Find run start and end
         nThisRunStart = find(vbSeries, 1, 'first');
         vbSeries(1:nThisRunStart) = true;
         nThisRunEnd = find(vbSeries ~= true, 1, 'first');
         
         % - Catch runs that finish at the end of the series
         if isempty(nThisRunEnd)
            nThisRunEnd = numel(vbSeries);
         end
         vbSeries(1:nThisRunEnd) = false;

         % - Catch segments with no end
         if (isempty(nThisRunEnd))
            nThisRunEnd = numel(vbSeries);
         end
                  
         % - Make a patch of the appropriate colour
         vtPatchTimes = tsSeries.vtTimeTrace([nThisRunStart nThisRunStart nThisRunEnd nThisRunEnd nThisRunStart]);
         hSeries = patch(vtPatchTimes, vfPatchVals, nan, 'FaceColor', vfThisTraceColour, 'EdgeColor', 'none', 'FaceAlpha', 0.4, cExtraArgs{:});
         hold all;

         % - Add to legend, if first patch in series
         if (bInLegend)
            bInLegend = false;
         else
            hAnn = get(hSeries, 'Annotation');
            hLeg = get(hAnn, 'LegendInformation');
            set(hLeg, 'IconDisplayStyle', 'off');
         end
      end
   end
   
   if ~bHold
      hold off;
   end
   
elseif (tsSeries.testsampletype({'categorical', 'count'}))
   % - Handle numerical "zero-order hold" time series
   if (nargin == 1)
      varargin = {'-'};
   end
   
   % - Work out series size and which ones to plot
   nNumSeries = prod(samplesize(tsSeries));

   if (nNumSeries > 1)
      % - Collect traces
      vfSampleSize = tsSeries.samplesize;
      nNumFrameDims = numel(vfSampleSize)-1;
      
      for (nTraceInd = nNumSeries:-1:1)
         [cTraceInds{1:nNumFrameDims}] = ind2sub(vfSampleSize(2:end), nTraceInd);
         mfTraces(:, nTraceInd) = tsSeries.tfSamples(:, cTraceInds{:});
      end   
      
      vhOutput = stairs(tsSeries.vtTimeTrace, mfTraces, varargin{:});
   else
      vhOutput = stairs(tsSeries.vtTimeTrace, tsSeries.tfSamples(:, :), varargin{:});
   end
   
else
   % - Handle other time series types
   if (nargin == 1)
      varargin = {'-'};
   end
   
   vhOutput = plot(tsSeries.vtTimeTrace, tsSeries.tfSamples(:, :), varargin{:});
end

xlabel('Time (s)');
ylabel('Samples');
axis tight;

% - Clear output handles, if not requested
if (nargout == 0)
   clear vhOutput;
end

function bIsColourSpec = is_colourspec(oData)

bIsColourSpec = (isscalar(oData) && ischar(oData)) || (isdouble(oData) && (numel(oData) == 3) && all(oData >= 0) && all(oData <= 1));


% --- END of plot.m ---
