function tsSub = minus(tsA, tsB)

% minus (-) - METHOD Subtract two time series by value
%
% Usage: tsSub = tsA - tsB
%        tsSub = minus(tsA, tsB)
%
% Subtraction can only be performed on countable time series (i.e.
% 'continuous' or 'count' time series). The summed time series will have
% the same time base as 'tsA'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 23rd April, 2015

% -- Check arguments

if (nargin < 2)
   help minus;
   error('TimeSeries:Usage', ...
         '*** minus: Incorrect usage.');
end

% - Use sampleshift method to perform addition
tsSub = sampleshift(tsA, -tsB);

% --- END of minus.m ---