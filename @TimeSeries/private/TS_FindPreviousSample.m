% TS_FindPreviousSample - FUNCTION Locate the previous sample in a time trace

function vnPreviousSample = TS_FindPreviousSample(vtTimeTrace, vtInterpolationTimes)

% - Filter samples to remove time points requiring extrapolation
vbExtrapolation = ~((vtInterpolationTimes >= vtTimeTrace(1)) & (vtInterpolationTimes <= vtTimeTrace(end)));

% - Find nearest preceeding samples
vnPreviousSample = nan(1, numel(vtInterpolationTimes));
if (any(~vbExtrapolation))
   vnPreviousSample(~vbExtrapolation) = BinarySearchSortedList_mex(vtTimeTrace, vtInterpolationTimes(~vbExtrapolation));
   vnPreviousSample(vnPreviousSample == numel(vtTimeTrace)) = numel(vtTimeTrace) - 1;
end

end