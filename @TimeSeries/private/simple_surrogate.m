function [cSeq, f_rng, ec0, rng0] = simple_surrogate(f,w,nSeqLength, nNumSurrogates, f_rng, ec0, rng0)
% simple_surrogate
%
% seq = simple_surrogate(f,w,nSeqLength);
%
% Given a transition matrix produces a random sequence
% with exactly the same transition count.
%
% INPUTS (from trans_count.m)
% f:            a transition count matrix
% w:          a list of words
%
% OUTPUTS
% seq:      a random sequence of words
%               that have transition count f
%
% EXAMPLE:
% >> d = [0 1 1 0 1 0 1 1 1 0 0 1 0];
% >> [f w u v] = trans_count(d,1);
% >> seq = whittle_surrogate_uncond(f,w)
% seq = 0   1   0   1   1   1   0   1   0   1   1   0   0
% >> seq = whittle_surrogate_uncond(f,w)
% seq = 1   1   0   1   0   1   0   1   0   0   1   1   1
%
% Sucessive calls produce sequences sampled
% randomly and uniformly from the set of all sequences
% with the same transition count.
%

% Fixed zeroth order case 10/8/2013

if (~exist('nSeqLength', 'var'))
   nSeqLength = sum(f(:))+1;
end

if (~exist('nNumSurrogates', 'var'))
   nNumSurrogates = 1;
end

N = size(w,1);
sym = 1:N;
if size(f,2)==1 %if f is not a matrix then use random permutation
   
   parfor (nSurrogate = 1:nNumSurrogates)
      
      dum = [];
      for i=1:N
         dum = [dum i*ones(1,f(i))];
      end
      indx = randperm(nSeqLength);
      cSeq{nSurrogate} = dum(indx);
   end
   
   return;
end

% - Pre-compute transition probabilities
if (~exist('f_rng', 'var') || isempty(f_rng))
   f_tp = bsxfun(@rdivide, f, sum(f, 2));
   f_rng = [zeros(size(f, 1), 1) cumsum(f_tp, 2)];
   ec0 = full(sum(f, 1) ./ sum(f(:)));
   rng0 = [0 cumsum(ec0)];
end

cSeq = {};
for (nSurrogate = 1:nNumSurrogates)
   % - Choose a starting word
   sr = rand(1);
   indx = (sr >= rng0(1:end-1)) & (sr <= rng0(2:end)); %#ok<PFBNS>
   
   u = sym(indx); %#ok<PFBNS>
   
   ytrial = zeros(1,nSeqLength);
   for mm=1:nSeqLength
      ytrial(mm) = u;
      %    ec = f(u, :) ./ sum(f(u, :));
      %    rng = [0 cumsum(ec)];
      rng = f_rng(u, :); %#ok<PFBNS>
      
      sr = rand(1);
      indx = (sr >= rng(1:end-1)) & (sr <= rng(2:end));
      
      % - Handle the case where there is no outward transition
      if (nnz(indx) == 0)
         sr = rand(1);
         indx = (sr >= rng0(1:end-1)) & (sr <= rng0(2:end));
      end
      
      u = sym(indx);
   end
   
   % Convert number sequence back to symbol sequence
   ls = size(w,2);
   seq = zeros(1,nSeqLength+ls-1);
   seq(1:ls) = w(ytrial(1),:);
   for i=2:nSeqLength
      seq(ls+i-1) = w(ytrial(i),ls);
   end
   
   cSeq{nSurrogate} = seq(1:nSeqLength);
end


if (nNumSurrogates == 1)
   cSeq = cSeq{1};
end

