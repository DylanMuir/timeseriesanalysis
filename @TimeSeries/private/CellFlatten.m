function [cellArray] = CellFlatten(varargin)

% CellFlatten - FUNCTION Convert a list of items to a single level cell array
%
% Usage: [cellArray] = CellFlatten(arg1, arg2, ...)
%
% CellFlatten will convert a list of arguments into a single-level cell array.
% If any argument is already a cell array, each cell will be concatenated to
% 'cellArray' in a list.  The result of this function is a single-dimensioned
% cell array containing a cell for each individual item passed to CellFlatten.
% The order of cell elements in the argument list is guaranteed to be
% preserved.
%
% This function is useful when dealing with variable-length argument lists,
% each item of which can also be a cell array of items.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 14th May, 2004

% -- Check arguments

if (nargin == 0)
   disp('*** CellFlatten: Incorrect usage');
   help CellFlatten;
   return;
end

% -- Convert arguments

% - Which elements contain cell subarrays?
vbIsCellSubarray = cellfun(@iscell, varargin, 'UniformOutput', true);

% - Skip if no cell subarrays
if ~any(vbIsCellSubarray)
   cellArray = varargin;
   return;
end

% - Recursively flatten subarrays
varargin(vbIsCellSubarray) = cellfun(@(c)CellFlatten(c{:}), varargin(vbIsCellSubarray), 'UniformOutput', false);

% - Count the total number of arguments
vnArgSizes(nargin) = nan;
vnArgSizes(~vbIsCellSubarray) = 1;
vnArgSizes(vbIsCellSubarray) = cellfun(@numel, varargin(vbIsCellSubarray), 'UniformOutput', true);
vnArgEnds = cumsum(vnArgSizes);
vnArgStarts = [1 vnArgEnds(1:end-1)+1];
nNumArgs = vnArgEnds(end);

% - Preallocate return array
cellArray = cell(1, nNumArgs);

% - Deal out non-cell subarray arguments
cellArray(vnArgEnds(~vbIsCellSubarray)) = varargin(~vbIsCellSubarray);

% - Deal out arguments into return array
for nIndexArg = find(vbIsCellSubarray)
   cellArray(vnArgStarts(nIndexArg):vnArgEnds(nIndexArg)) = varargin{nIndexArg};
end

% --- END of CellFlatten.m ---
