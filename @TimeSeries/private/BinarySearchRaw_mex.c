#include <math.h>
#include <mex.h>

/* BinarySearchRaw_mex - MEX FUNCTION Binary search within a sorted array
 *
 * Usage: [nTargetIndex] = BinarySearchRaw_mex(vfList, fSearchItem, vnBounds)
 *
 * Search within 'vfList', over the range 'vnBounds(1)' to 'vnBounds(2)', for the index
 * of the list entry preceding 'fSearchItem'. If 'fSearchItem' is found at location 'vnBounds(2)',
 * then the index of the second-to-last item will be returned. If 'fSearchItem' matches
 * multiple items in 'vfList', then the index of the final matching item will be returned.
 * The desired index will be returned in 'nTargetIndex'.
 *
 * Note that 'vfList' MUST be already sorted. This condition is not checked by BinarySearchRaw_mex.
 */

/* Author: Dylan Muir <dylan.muir@unibas.ch>
 * Created: 3rd September, 2014
 */

/* BinarySearchRaw - FUNCTION Raw binary search within a sorted list (MATLAB)
	function nTargetIndex = BinarySearchRaw(vfList, fSearchItem, vnBounds)
		nListIndex = round((vnBounds(1)+vnBounds(2))/2);
		
		while true
			fListItem = vfList(nListIndex);
			
			if (fSearchItem < fListItem)
				% - Subdivide lower
				vnBounds(2) = nListIndex;
				nListIndex = floor((vnBounds(1)+vnBounds(2))/2);
				
			else %(fSearchItem >= fListItem)
				if (nListIndex == vnBounds(2))
					% - Accept condition
					nTargetIndex = nListIndex;
					return;
					
				elseif (nListIndex < vnBounds(2)) && (fSearchItem < vfList(nListIndex+1))
					% - Accept condition
					nTargetIndex = nListIndex;
					return;
				
				else % (fSearchItem >= vfList(nListIndex+1))				  
					% - Subdivide upper
					vnBounds(1) = nListIndex;
					nListIndex = ceil((vnBounds(1)+vnBounds(2))/2);
				end
			end
		end
	end */

/* - Debug flag */
/* #define DEBUG */

/* - Include a definition for "round", which is not included on windows */
#ifdef _WIN32
#define round(val)  (floor(val + 0.5))
#endif

void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[] )
{
	double	*vfList, *fSearchItem, *nTargetIndex, *vnBoundsArg, fListItem;
	int		nListIndex, vnBounds[2];
	
	/* - Create matrix for the return argument. */
	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
 
	/* - Assign pointers to each input and output. */
	vfList = mxGetPr(prhs[0]);
	fSearchItem = mxGetPr(prhs[1]);
	vnBoundsArg = mxGetPr(prhs[2]);
	vnBounds[0] = (int) (vnBoundsArg[0]) - 1;
	vnBounds[1] = (int) (vnBoundsArg[1]) - 1;
	nTargetIndex = mxGetPr(plhs[0]);
	
	/* - Perform binary search */
	nListIndex = round((double)(vnBounds[0] + vnBounds[1])/2);
	
	while (true) {
		fListItem = vfList[nListIndex];
		
        #ifdef DEBUG
            mexPrintf("List index [%d]; bounds [%d %d]; value [%.2f], search [%.2f]\n", nListIndex, vnBounds[0], vnBounds[1], fListItem, *fSearchItem);
        #endif
        
		if (*fSearchItem < fListItem) {
			/* Subdivide lower */
			vnBounds[1] = nListIndex;
			nListIndex = floor((double)(vnBounds[0] + vnBounds[1])/2);
		
		} else {	/* (*fSearchItem >= fListItem) */
			if (nListIndex == vnBounds[1]) {
				/* Accept condition */
				*nTargetIndex = nListIndex;
				break;

			} else if ((nListIndex < vnBounds[1]) & (*fSearchItem < vfList[nListIndex+1])) {
				/* Accept condition */
				*nTargetIndex = nListIndex;
				break;
			
			} else {	/* (*fSearchItem >= vfList[nListIndex+1]) */
				/* Subdivide upper */
				vnBounds[0] = nListIndex;
				nListIndex = ceil((double)(vnBounds[0] + vnBounds[1])/2);
			}
		}
	}
}