% TSI_ZeroOrderHold - Interpolation FUNCTION
function tfInterpolated = TSI_ZeroOrderHold(sTS, vtInterpolationTimes)
% - Short-cut for empty time series
if isempty(sTS.vtTimeTrace)
   tfInterpolated = nan(numel(vtInterpolationTimes), 1);
   return;
end

% - Find time samples preceeding interpolation times (plus eps, to ensure we take the later of two samples
[vtInterpolationTimes, vnInterpolantOrder] = sort(reshape(vtInterpolationTimes, [], 1));
vnPreviousSample = reshape(TS_FindPreviousSample(sTS.vtTimeTrace, vtInterpolationTimes + eps(vtInterpolationTimes)), [], 1);

% - Catch samples after end of data
vnPreviousSample(vtInterpolationTimes + eps(vtInterpolationTimes) > sTS.vtTimeTrace(end)) = numel(sTS.vtTimeTrace);
vbPerformInterpolation = ~isnan(vnPreviousSample);

% - Pre-allocate samples
vnSampleSize = sTS.samplesize;
vnSampleSize(1) = numel(vtInterpolationTimes);
tfInterpolated = nan(vnSampleSize);

% - Interpolate samples
if any(vbPerformInterpolation)
   tfInterpolated(vbPerformInterpolation, :) = sTS.tfSamples(vnPreviousSample(vbPerformInterpolation), :);
end

% - Reorder samples
tfInterpolated(vnInterpolantOrder, :) = tfInterpolated(:, :);

end
