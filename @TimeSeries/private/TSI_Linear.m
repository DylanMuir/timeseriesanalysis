
% TSI_Linear - Interpolation FUNCTION
function tfInterpolated = TSI_Linear(sTS, vtInterpolationTimes)
% - Short-cut for empty time series
if isempty(sTS.vtTimeTrace)
   tfInterpolated = nan(numel(vtInterpolationTimes), 1);
   return;
end

% - Find time samples preceeding interpolation times
[vtInterpolationTimes, vnInterpolantOrder] = sort(reshape(vtInterpolationTimes, [], 1));
vnPreviousSample = reshape(TS_FindPreviousSample(sTS.vtTimeTrace, vtInterpolationTimes), [], 1);
vbAcceptInterpolation = ~isnan(vnPreviousSample);

% - Pre-allocate samples
vnSampleSize = sTS.samplesize;
vnSampleSize(1) = numel(vtInterpolationTimes);
tfInterpolated = nan(vnSampleSize);

% - Catch interpolation times which are exactly time points of the
% source time series
[vbExactSample, vnTimePoint] = sTS.fhIsMember(vtInterpolationTimes, sTS.vtTimeTrace);
tfInterpolated(vbExactSample, :) = sTS.tfSamples(vnTimePoint(vbExactSample), :);
vbPerformInterpolation = vbAcceptInterpolation & ~vbExactSample;

% - Interpolate samples when necessary
if any(vbPerformInterpolation)
   vtPreviousTime = sTS.vtTimeTrace(vnPreviousSample(vbPerformInterpolation));
   vtNextTime = sTS.vtTimeTrace(vnPreviousSample(vbPerformInterpolation)+1);
   vfInterpolationFactors = (vtInterpolationTimes(vbPerformInterpolation) - vtPreviousTime) ./ ...
      (vtNextTime - vtPreviousTime);
   
   % - Correct interpolation factors for inf sample times (take following sample)
   vbInfTimes = isinf(vtPreviousTime);
   vfInterpolationFactors(vbInfTimes) = 1;
   
   % - Interpolate samples
   tfNextSample = double(sTS.tfSamples(vnPreviousSample(vbPerformInterpolation)+1, :));
   tfPreviousSample = double(sTS.tfSamples(vnPreviousSample(vbPerformInterpolation), :));
   tfInterpolated(vbPerformInterpolation, :) = bsxfun(@times, vfInterpolationFactors, ...
      (tfNextSample - tfPreviousSample)) + tfPreviousSample;
end

% - Remove non-accepted interpolants
tfInterpolated(~vbAcceptInterpolation, :) = nan;

% - Reorder samples
tfInterpolated(vnInterpolantOrder, :) = tfInterpolated(:, :);

% - Cast return data to original format
%   Note: take first sample to de-reference MappedTensors or TensorStacks
tfInterpolated = cast(tfInterpolated, class(sTS.tfSamples(1)));

end
