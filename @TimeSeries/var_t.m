function tfVar = var_t(tsSource)

% var_t - METHOD Compute variance of time series, over time
%
% Usage: tfVar = var_t(tsSource)
%
% 'tsSource' is a continuous or count time series. The sample infinite-time
% variance for each series in 'tsSource' will be computed and returned in
% 'tfVar'. 'tfVar' will be of size [1 M N ...], corresponding to the size
% of the time series in 'tsSource'.
%
% Samples will be weighted according the time duration they represent, for
% irregularly-sampled time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

cstrPerimittedSampleTypes = {'continuous', 'count'};

% - Check arguments
if (nargin < 1)
   help var_t;
   error('TimeSeries:Usage', ...
         '*** var_t: Incorrect usage.');
end

switch lower(tsSource.strSampleType)
   case cstrPerimittedSampleTypes
      % - Permitted
      
   otherwise
      error('TimeSeries:Argument', ...
            ['*** var_t: The sample type of ''tsSource'' must be one of {' sprintf('%s ', cstrPerimittedSampleTypes{:}) '}.']);
end

% - Determine time point weightings
vfSampleWeight = diff(tsSource.vtTimeTrace)/2;
dt1 = [vfSampleWeight(1);vfSampleWeight];
dt2 = [vfSampleWeight;vfSampleWeight(end)];
vfSampleWeight = dt1+dt2;

% - Weight samples
tfSamples = ones(size(tsSource.tfSamples));
tfSamples(:, :) = bsxfun(@times, double(tsSource.tfSamples(:, :)), vfSampleWeight) ./ nanmean(vfSampleWeight);

% - Subtract sample mean
tfSamples = bsxfun(@minus, tfSamples, nanmean(tfSamples, 1));

% - Compute variance
tfVar = nansum(abs(tfSamples.^2) ./ size(tfSamples, 1), 1);

% --- END of var_t.m ---
