function tfMean = nanmean_t(tsSource)

% nanmean_t - METHOD Compute variance of time series, over time, ignoring NaNs
%
% Usage: tfMean = tsSource.nanmean_t()
%
% 'tsSource' is a continuous or count time series. The sample infinite-time
% variance for each series in 'tsSource' will be computed and returned in
% 'tfMean'. 'tfMean' will be of size [1 M N ...], corresponding to the size
% of the time series in 'tsSource'.
%
% Samples will be weighted according the time duration they represent, for
% irregularly-sampled time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 14th September, 2015

tfMean = tsSource.mean_t(true);

% --- END of nanmean_t.m ---
