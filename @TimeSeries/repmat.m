function tsReplicated = repmat(tsSource, varargin)

% repmat - FUNCTION Replicate a time series
%
% Usage: tsReplicated = repmat(tsSource, varargin)

varargin = cellfun(@(c)reshape(c, 1, []), varargin, 'UniformOutput', false);
vnReplication = [1 varargin{:}];

tsReplicated = tsSource;
tsReplicated.tfSamples = repmat(tsReplicated.tfSamples, vnReplication);

% --- END of repmat.m ---
