function ts = insert(ts, vtNewTimeTrace, tfNewSamples)

% insert - Method Insert samples into a time series
%
% Usage: ts = insert(ts, vtNewTimeTrace, tfNewSamples)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 21st April, 2015

% -- Check arguments

if (nargin < 3)
   help insert;
   error('TimeSeries:Usage', ...
         '*** insert: Incorrect usage.')
end

% - Just create a new time series and merge
ts = merge(ts, TimeSeries(vtNewTimeTrace, tfNewSamples, ts.strSampleType, ts.vfSampleRange));

% --- END of insert.m ---