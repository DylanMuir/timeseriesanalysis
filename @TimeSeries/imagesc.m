% imagesc - METHOD Overloaded imagesc function
%
% Usage: h = imagesc(ts, <...>)
% 
% See matlab imagesc for usage.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 2015

function h = imagesc(ts, varargin)

% - Test for a regularly-sampled time series (rough guess)
vtISI = diff(ts.vtTimeTrace);
if (any(vtISI > 2 .* nanmedian(vtISI)))
   warning('TimeSeries:NonRegularSampling', ...
           '--- TimeSeries/imagesc: Warning: ''imagesc'' does not work well for non-regularly sampled time series.');
end

% - Remove 'inf' times
vbInfTimes = isinf(ts.vtTimeTrace);

% - Make the image
newplot;
h = imagesc(ts.vtTimeTrace(~vbInfTimes), 1:size(ts.tfSamples, 2), ts.tfSamples(~vbInfTimes, :)', varargin{:});
xlabel('Time (s)');
ylabel('Series index');

if (nargout == 0)
   clear h;
end

% --- END of imagesc.m ---
