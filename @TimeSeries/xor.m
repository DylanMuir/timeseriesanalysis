function [tsXorTS] = xor(tsA, tsB)

% and - METHOD Perform a boolean XOR of two boolean time series
%
% Usage: [tsXorTS] = xor(tsA, tsB)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

% -- Check arguments

if (nargin < 2)
   help TimeSeries/and;
   error('TimeSeries:Usage', '*** TimeSeries/xor: Incorrect usagge.');
end

if (~TestSampleTypeTS(tsA, 'boolean') || ~TestSampleTypeTS(tsB, 'boolean'))
   error('TimeSeries:Arguments', '*** TimeSeries/xor: Both ''tsA'' and ''tsB'' must be boolean time series.');
end


% - Resample 'tsB' to 'tsA'
tsB = tsB.resample(tsA.vtTimeTrace);

% - Perform boolean 'and'
tsXorTS = tsA;
tsXorTS.tfSamples = xor(tsXorTS.tfSamples == true, tsB.tfSamples == true); % nan == false

% --- END of xor.m ---
