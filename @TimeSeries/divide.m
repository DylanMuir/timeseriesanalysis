function tsDivided = divide(tsNumerator, tsDenominator)

% divide - FUNCTION Divide two time series by value, or divide one time series by a scalar
%
% Usage: tsDivided = divide(tsNumerator, fDenominator)
%        tsDivided = divide(tsNumerator, tsDenominator)
%        tsDivided = divide(fNumerator, tsDenominator)
%
% 'tsNumerator' is a numeric time series. 'fDenominator' is a scalar
% divisor to apply to the samples in 'tsNumerator'. Alternatively,
% 'tsDenominator' is a time series to divide 'tsNumerator' by.
%
% 'tsDivided' will be a copy of 'tsNumerator', with the sample values
% divided appropriately. If a scalar 'fNumerator' is provided, then
% 'tsDivided' will be a copy of 'tsDenominator', with sample values divided
% appropriately.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 22nd October, 2014

% - Constants

DT_cstrPermittedSampleTypes = {'continuous', 'count'};


% -- Check arguments

if (nargin < 2)
   help divide;
   error('TimeSeries:Usage', ...
         '*** divide: Incorrect usage.');
end

% - Deal with an array of numerator time series
if (~isscalar(tsNumerator))
   tsDivided = arrayfun(@(ts)divide(ts, tsDenominator), tsNumerator);
   return;
end

% - Deal with an array of denominator time series
if (~isscalar(tsDenominator))
   tsDivided = arrayfun(@(ts)divide(tsNumerator, ts), tsDenominator);
   return;
end

% - Deal with a scalar numerator
if (isscalar(tsNumerator) && isnumeric(tsNumerator))
   tsDivided = tsDenominator;
   tsDivided.tfSamples = double(tsNumerator) ./ double(tsDivided.tfSamples);
   return;
end

% - Deal with a scalar denominator
if (isscalar(tsDenominator) && isnumeric(tsDenominator))
   tsDivided = tsNumerator;
   tsDivided.tfSamples = double(tsDivided.tfSamples) ./ double(tsDenominator);
   return;
end

% - Check time series types
if (~all(tsNumerator.testsampletype(DT_cstrPermittedSampleTypes))) || (~all(tsDenominator.testsampletype(DT_cstrPermittedSampleTypes)))
   error('TimeSeries:Arguments', ...
         '*** divide: Both ''tsNumerator'' ''tsDenominator'' time series must have a numeric sample type.');
end

% - Both time series must have same sample sizes, or one must be a scalar
vnSampleSizeN = tsNumerator.samplesize;
vnSampleSizeD = tsDenominator.samplesize;

if (prod(vnSampleSizeN) ~= 1) && (prod(vnSampleSizeD) ~= 1) && ~isequal(vnSampleSizeN, vnSampleSizeD)
   error('TimeSeries:Arguments', ...
         '*** divide: Time series must have the same sample sizes, or one must be unitary.')
end

% - Resample denominator time series to numerator time base
tsDenominator = tsDenominator.resample(tsNumerator.vtTimeTrace);

% - Determine result size
vnResultSize = [size(tsNumerator.tfSamples, 1) max(vnSampleSizeN(2:end), vnSampleSizeD(2:end))];

% - Create a divided time series
tsDivided = tsNumerator;
tsDivided.tfSamples = ones(vnResultSize); % - Upcast everything to double
tsDivided.tfSamples(:, :) = bsxfun(@rdivide, double(tsNumerator.tfSamples(:, :)), tsDenominator.tfSamples(:, :));

% --- END of divide.m ---
