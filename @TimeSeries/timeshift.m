function tsTimeShifted = timeshift(tsSource, tTimeShift)

% TimeShiftTS - FUNCTION Shift the time base of a time series
%
% Usage: tsTimeShifted = timeshift(tsSource, tTimeShift)
%
% 'tsSource' is a time series. 'tTimeShift' is a scalar time shift to apply
% to 'tsSource.' 'tsTimeShifted' will be a copy of 'tsSource', but with the
% time base shifted by the offset 'tTimeShift'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

if (nargin < 2)
   help timeshift;
   error('TimeSeries:Usage', ...
         '*** timeshift: Incorrect usage.');
end

if (~isscalar(tTimeShift) || ~isnumeric(tTimeShift))
   error('TimeSeries:Usage', ...
         '*** timeshift: ''tTimeShift'' must be a scalar time to shift by.')
end

% - Simply shift the time base
tsTimeShifted = tsSource;
tsTimeShifted.vtTimeTrace = tsTimeShifted.vtTimeTrace + tTimeShift;

% --- END of timeshift.m ---
