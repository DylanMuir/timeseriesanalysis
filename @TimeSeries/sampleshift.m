function tsSourceShifted = sampleshift(tsSource, tsShift)

% sampleshift - METHOD Shift the samples of a time series, by a fixed amount or according to another time series
%
% Usage: tsSourceShifted = sampleshift(tsSource, fShift)
%        tsSourceShifted = sampleshift(tsSource, tsShift)
%
% 'tsSource' is a continuous time series. 'fShift' is a scalar offset to
% apply to the samples in 'tsSource'. Alternatively, a second time series
% 'tsShift' can be provided, in which case 'tsSource' will be shifted by
% the value of 'tsShift' at the corresponding time point. 'tsShift' must be
% a unitary continuous time series.
%
% 'tsSourceShifted' will be a copy of 'tsSource', with the sample values
% shifted appropriately.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

% -- Constants

SST_cstrPermittedSampleTypes = {'continuous', 'count'};


% -- Check arguments

if (nargin < 2)
   help sampleshift;
   error('TimeSeries:Usage', ...
         '*** sampleshift: Incorrect usage.');
end

% - Deal with a scalar shift
if (isscalar(tsShift) && isnumeric(tsShift)) || isempty(tsShift)
   tsSourceShifted = tsSource;
   tsSourceShifted.tfSamples = tsSourceShifted.tfSamples + tsShift;
   return;
end

% - Check time series types
if (~all(tsSource.testsampletype(SST_cstrPermittedSampleTypes))) || (~all(tsShift.testsampletype(SST_cstrPermittedSampleTypes)))
   error('TimeSeries:Arguments', ...
         '*** sampleshift: Both ''tsSource'' ''tsShift'' time series must have a numeric sample type.');
end

% - Check for unitary shift time series
[~, nNumSeries] = size(tsShift.tfSamples);
if (nNumSeries > 1)
   error('TimeSeries:Arguments', ...
         '*** sampleshift: ''tsShift'' must be a unitary time series.');
end

% - Resample shift time series to source time base
tsShift = tsShift.resample(tsSource.vtTimeTrace);

% - Create a shifted time series
tsSourceShifted = tsSource;
tsSourceShifted.tfSamples = bsxfun(@plus, tsSourceShifted.tfSamples, tsShift.tfSamples);

% --- END of sampleshift.m ---
