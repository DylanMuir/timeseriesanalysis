% TimeSeries - CLASS Encapsulate and manipulate time-series data
%
% Usage: ts = TimeSeries(vtTimeTrace, tfSamples, ...)
%             TimeSeries(vtTimeTrace, [], ...)
%             TimeSeries(... <, strSampleType, vfSampleRange, fhInterpolation, vtTimeBounds)
%
% TimeSeries is a class for representing and manipulating time-series data.
% 
% 'vtTimeTrace' is a double vector [T 1] of sampling times, with an arbitrary
% zero time. 'vtTimeTrace' is assumed to be in seconds, but as long as all
% sample times are used consistently then you can use any unit you like.
%
% 'tfSamples' is a tensor of samples [T N M ...]. Each column vector t=[1..T]
% corresponds to the sampling time vtTimeTrace(t). Each column vector
% defines a time series with common sampling times and common data format.
% If 'tfSamples' is provided as an empty matrix, then an 'event' time
% series is created.
%
% 'strSampleType' defines what time of time series data is represented. It
% can be one of {'continuous', 'event', 'count', 'category', 'boolean'}:
%
%    'continuous': Real-valued samples, with linear interpolation by default
%    'event': Momentary events that occur at the corresponding sample time
%    'count': Integer-valued samples, where the relative magnitude of
%             samples is meaningful. Zero-order hold interpolation is used
%             by default.
%    'category': Integer-valued samples, where the relative magnitude of
%             samples is not meaningful. Zero-order hold interpolation is
%             used by default.
%    'boolean': Boolean-valued samples. Zero-order hold interpolation is
%             used by default.
%
% The optional arguments 'vfSampleRange' and 'vtTimeBounds' allow you to
% specify valid ranges of the time series object, independent of the values
% wihtin 'vtTimeTrace' and 'tfSamples'.
%
% The desired interpolation function can be specified either as a string
% {'Linear', 'ZeroOrderHold'}. If not speficed, a reasonable default
% interpolation function will be assigned.
%
% 'fhInterpolation' can be a user-defined interpolation function with the
% signature tfInterpolatedSamples = @(sTimeSeries, vtInterpolationTimes).
% 'sTimeSeries' is a time series object; 'vtInterpolationTimes' is a
% list of T time points for which interpolation is desired.
% 'tfInterpolatedSamples' must be a tensor [Tx...], where each row 't'
% corresponds to the same time point specified in 'vtInterpolationTimes',
% and the sample size and shape must match that of 'sTimeSeries'.
%
% Utility methods:
%    [vnPriorSampleIndex] = ts.fhFindPreviousSample(vtSortedList, vtSortedTargets)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 16th April, 2015

classdef TimeSeries
   properties (SetAccess = private)
      vtTimeTrace;      % Time base for all time series contained in a single object
      tfSamples;        % Samples corresponding to each time point (if not an 'event' time series object)
      fhInterpolation;  % Function used to interpolate between sampled time points 
      vtTimeBounds;     % Temporal bounds of time series in this object
      vfSampleRange;    % Range of sample values in time series in this object
      strSampleType;    % Time of time series contained in this object
   end
   
   % - Function handles used to accelerate object manipulation
   properties (Transient, Hidden, SetAccess = private, GetAccess = private)
      fhBinarySearchSortedList;  % Function handle for searching in a sorted list
      fhFindPreviousSample;      % Function handle for finding previous samples in a sorted list
      fhIsMember;                % Function handle for `ismember` (accelerated version)
   end
   
   methods
      %% TimeSeries - CONSTRUCTOR
      function sTS = TimeSeries(vtTimeTrace, tfSamples, strSampleType, vfSampleRange, fhInterpolation, vtTimeBounds)
         % -- Default parameters
         DEF_vtTimeBounds = zeros(0, 2);
         DEF_vfSampleRange = zeros(0, 2);
                 
         % -- Global parameters
         cstrSampleTypes = {'categorical', 'count', 'continuous', 'event', 'boolean'};
         
         % - Get utility function handles
         [sTS.fhFindPreviousSample, sTS.fhBinarySearchSortedList, sTS.fhIsMember] = TS_GetMexFunctionHandles;
         
         % -- Check arguments
         
         % -- Handle "zero parameter" initialisation
         if (nargin == 0)
            vtTimeTrace = [];
            tfSamples = [];
         end
         
         % - Convert a time series structure
         if (isstruct(vtTimeTrace) && all(isfield(vtTimeTrace, {'vtTimeTrace', 'tfSamples', 'strSampleType', 'fhInterpolation'})))
            tsOld = vtTimeTrace;
            
            % - Try to convert function handles
            if (~isempty(tsOld.fhInterpolation))
               switch func2str(tsOld.fhInterpolation)
                  case {'TimeSeriesStructure/TSI_Linear', 'TSI_Linear'}
                     tsOld.fhInterpolation = @TSI_Linear;
                     
                  case {'TimeSeriesStructure/TSI_ZeroOrderHold', 'TSI_ZeroOrderHold'}
                     tsOld.fhInterpolation = @TSI_ZeroOrderHold;
                     
                  otherwise
                     warning('TimeSeries:Conversion', ...
                        '--- TimeSeries: Warning: Unknown interpolation function when converting from structure format. This may produce an error.');
               end
            end
            
            % - Create a TimeSeries object
            sTS = TimeSeries(tsOld.vtTimeTrace, tsOld.tfSamples, tsOld.strSampleType, tsOld.vfSampleRange, tsOld.fhInterpolation, tsOld.vtTimeBounds);
            return;
         end
         
         % - For compatibility, at least two arguments are required
         if (nargin == 1)
            help TimeSeries;
            error('TimeSeries:Usage', '*** TimeSeries: Incorrect usage.');
         end
         
         % - Make sure 'vtTimeTrace' is correctly shaped and of class 'double'
         switch class(vtTimeTrace)
            case 'duration'
               vtTimeTrace = seconds(vtTimeTrace);
               
            otherwise
               vtTimeTrace = double(vtTimeTrace);
         end
         
         if ~iscolumn(vtTimeTrace)
            vtTimeTrace = reshape(vtTimeTrace, [], 1);
         end
         
         sTS.vtTimeTrace = vtTimeTrace;
         
         if (~exist('vtTimeBounds', 'var'))
            vtTimeBounds = DEF_vtTimeBounds;
         end
         
         sTS.vtTimeBounds = vtTimeBounds;
         
         % - Make sure time trace is a sorted vector
         if ~issorted(vtTimeTrace)
            error('TimeSeries:Arguments', ...
               '*** TimeSeries: Time traces must be strictly increasing.');
         end
         
         % - Try to determine sample type, if not provided
         if (nargin < 3) || isempty(strSampleType)
            if (ischar(tfSamples))
               strSampleType = 'categorical';
               
            elseif (islogical(tfSamples))
               strSampleType = 'boolean';
               
            elseif (isa(tfSamples, 'integer'))
               strSampleType = 'count';
               
            elseif (isempty(tfSamples) && ~isempty(vtTimeTrace))
               strSampleType = 'event';
               
            else
               strSampleType = 'continuous';
            end
            
         else
            % - Check sample type
            vbMatchSampleType = ismember(strSampleType, cstrSampleTypes);
            
            if (~any(vbMatchSampleType))
               error('TimeSeries:Arguments', ...
                  ['*** TimeSeries: ''strSampleType'' must be one of ' sprintf('%s ', cstrSampleTypes{:})]);
            end            
         end
         sTS.strSampleType = strSampleType;
         
         % - Check size of samples array
         if (size(tfSamples, 1) ~= numel(vtTimeTrace))
            if (numel(tfSamples) == numel(vtTimeTrace))
               % - Be lenient if we have a vector with the incorrect orientation
               tfSamples = reshape(tfSamples, [], 1);
               
            elseif (isempty(tfSamples) && strcmpi(strSampleType, 'event'))
               % - An event time series needs an empty 'tfSamples'
               % continue;
               
            else
               error('TimeSeries:Arguments', ...
                  '*** TimeSeries: The number of rows in ''tfSamples'' must match the number of elements in ''vtTimeTrace''');
            end
         end
         
         % - Assign samples
         sTS.tfSamples = tfSamples;
                  
         % - Check for floating point samples for continuous sample types
         if (strcmpi(sTS.strSampleType, 'continuous') && ~isfloat(tfSamples))
            warning('TimeSeries:IntegerContinuous', ...
               '--- TimeSeries: A continuous time series was created with non-floating point samples. This time series will not support ''nan'' values.');
         end
         
         % - Try to determine data range, if not provided
         if (~exist('vfSampleRange', 'var') || isempty(vfSampleRange))
            switch (lower(strSampleType))
               case {'categorical'}
                  vfSampleRange = unique(tfSamples);
                  vfSampleRange = vfSampleRange(~isnan(vfSampleRange));
                  
               otherwise
                  % - Use default sample range
                  vfSampleRange = DEF_vfSampleRange;
            end
         end
         
         sTS.vfSampleRange = vfSampleRange;
         
         % - Check interpolation function
         if (~exist('fhInterpolation', 'var') || isempty(fhInterpolation))
            switch (lower(strSampleType))
               case {'categorical', 'count', 'boolean'}
                  sTS.fhInterpolation = @TSI_ZeroOrderHold;
               
               case {'event'}
                  sTS.fhInterpolation = [];
                  
               otherwise
                  sTS.fhInterpolation = @TSI_Linear;
            end
            
         else
            if (isa(fhInterpolation, 'function_handle'))
               if (nargin(fhInterpolation) ~= 2)
                  error('TimeSeries:Arguments', ...
                     '*** TimeSeries: Error: ''fhInterpolation'' must accept two arguments.');
               end
               sTS.fhInterpolation = fhInterpolation;
               
            elseif ischar(fhInterpolation)
               switch (lower(fhInterpolation))
                  case {'zeroorderhold', 'tsi_zeroorderhold'}
                     sTS.fhInterpolation = @TSI_ZeroOrderHold;
                     
                  case {'linear', 'tsi_linear'}
                     sTS.fhInterpolation = @TSI_Linear;
                     
                  otherwise
                     sTS.fhInterpolation = str2func(fhInterpolation);
                     
                     if (nargin(fhInterpolation) ~= 2)
                        error('TimeSeries:Arguments', ...
                           '*** TimeSeries: Error: ''fhInterpolation'' must accept two arguments.');
                     end
               end
               
            else
               error('TimeSeries:Arguments', ...
                     '*** TImeSeries: Error: ''fhInterpolation'' must be a function handle or a string function name.');
            end
         end
                  
         % - Trim time series to time bounds, if provided
         if (~isempty(vtTimeBounds))
            sTS = sTS.trim(vtTimeBounds);
         end
      end      
      
      %% Accessor methods
      function vtTimeBounds = get.vtTimeBounds(ts)
         if (~isempty(ts.vtTimeBounds))
            % - Time bounds explicitly set
            vtTimeBounds = ts.vtTimeBounds;
            
         elseif (isempty(ts.vtTimeTrace))
            % - Empty time trace
            vtTimeBounds = zeros(0, 2);
         
         else
            % - Extract the time bounds from the time trace
            vtTimeBounds(1) = ts.vtTimeTrace(1);
            vtTimeBounds(2) = ts.vtTimeTrace(end);
         end
      end
      
      function vfSampleRange = get.vfSampleRange(ts)
         % - Sample bounds explicitly set?
         if (~isempty(ts.vfSampleRange))
            vfSampleRange = ts.vfSampleRange;
            
         elseif (isempty(ts.vtTimeTrace))
            % - Empty time trace
            vfSampleRange = zeros(0, 2);
            
         else
            % - Return default bounds
            vfSampleRange = [-inf inf];
         end
      end
   end
   
   %% Save and load methods
   methods (Static = true)
      function oTS = loadobj(oObj)
         oTS = TimeSeries(oObj.vtTimeTrace, oObj.tfSamples, oObj.strSampleType, oObj.vfSampleRange, oObj.fhInterpolation, oObj.vtTimeBounds);
      end
   end
end

%% Utility functions

% BinarySearchSortedList - FUNCTION Perform binary search between two sorted lists
   function vnPreviousSample = BinarySearchSortedList(vfList, vfItems)
      % - Set up indices
      nNumItems = numel(vfItems);
      nListBound = numel(vfList);
      vnBoundIndices = [1 nListBound];
      
      % - Preallocate return vector
      vnPreviousSample = nan(nNumItems, 1);

      % - Find first item
      vnPreviousSample(1) = BinarySearchRaw_mex(vfList, vfItems(1), vnBoundIndices);
      
      % - Find last item
      if (nNumItems > 1)
         vnBoundIndices(1) = vnPreviousSample(1);
         vnPreviousSample(end) = BinarySearchRaw_mex(vfList, vfItems(end), vnBoundIndices);
         vnBoundIndices(2) = max(vnBoundIndices(1)+1, vnPreviousSample(end));
      else
         return;
      end
      
      % - Find other items
      for (nItemIndex = 2:nNumItems-1)
         % - Find the current item
         nThisLocation = BinarySearchRaw_mex(vfList, vfItems(nItemIndex), vnBoundIndices);
         
         % - Restruct binary search domains
         vnBoundIndices(1) = nThisLocation;
         
         % - Record item location
         vnPreviousSample(nItemIndex) = nThisLocation;
      end
   end

% TS_GetMexFunctionHandles - FUNCTION Compile and return mex function handles
function [fhFindPreviousSample, fhBinarySearchSortedList, fhIsMember] = TS_GetMexFunctionHandles
   % - Try to get cached mex handles
   persistent sUserData
   if ~isempty(sUserData) && isfield(sUserData, 'TS_SavedMexFunctionHandles')
      fhFindPreviousSample = sUserData.TS_SavedMexFunctionHandles.fhFindPreviousSample;
      fhBinarySearchSortedList = sUserData.TS_SavedMexFunctionHandles.fhBinarySearchSortedList;
      fhIsMember = sUserData.TS_SavedMexFunctionHandles.fhIsMember;
      return;
   end

   % - Does the compiled MEX function exist?
   if (exist('BinarySearchSortedList_mex') ~= 3) %#ok<EXIST>
      try %#ok<TRYNC>
         % - Move to the MappedTensor private directory
         strMTDir = fileparts(which('TimeSeries'));
         strCWD = cd(fullfile(strMTDir, 'private'));
         
         % - Try to compile the MEX functions
         disp('--- TimeSeries: Compiling MEX functions.');
         mex('BinarySearchSortedList_mex.c', '-largeArrayDims', '-O');
         
         % - Move back to previous working directory
         cd(strCWD);
      end
   end
   
   % - Did we succeed?
   if (exist('BinarySearchSortedList_mex') == 3) %#ok<EXIST>
      fhBinarySearchSortedList = @BinarySearchSortedList_mex;
      
   else
      % - Just use the slow matlab version
      warning('TimeSeries:MEXCompilation', ...
         '--- TimeSeries: Could not compile MEX functions.  Using slow matlab versions.');
      
      fhBinarySearchSortedList = @BinarySearchSortedList;
   end
   
   % - Return the "find previous sample" function
   fhFindPreviousSample = @TS_FindPreviousSample;
   
   % - Find the buil=in "ismember" function
   if exist('_ismemberhelper', 'builtin')
      fhIsMember = @(a, b)builtin('_ismemberhelper', a, b);
   else
      fhIsMember = @ismember;
   end
   
   % - Cache function handles to accelerate object construction
   sUserData.TS_SavedMexFunctionHandles.fhFindPreviousSample = fhFindPreviousSample;
   sUserData.TS_SavedMexFunctionHandles.fhBinarySearchSortedList = fhBinarySearchSortedList;
   sUserData.TS_SavedMexFunctionHandles.fhIsMember = fhIsMember;
end

% --- END of TimeSeries CLASS ---
% --- END of TimeSeries.m ---
