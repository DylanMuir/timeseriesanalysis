function tfMean = mean_t(tsSource, bIgnoreNaNs)

% mean_t - METHOD Compute mean of time series, over time
%
% Usage: tfMean = tsSource.mean_t(<bIgnoreNaNs>)
%
% 'tsSource' is a continuous or count time series. The sample mean for each
% series in 'tsSource' will be computed and returned in 'tfMean'. 'tfMean'
% will be of size [1 M N ...], corresponding to the size of the time series
% in 'tsSource'.
%
% Samples will be weighted according the time duration they represent, for
% irregularly-sampled time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 14th September, 2015

cstrPerimittedSampleTypes = {'continuous', 'count'};

DEF_bIgnoreNaNs = false;

% - Check arguments
if (nargin < 1)
   help mean_t;
   error('TimeSeries:Usage', ...
         '*** mean_t: Incorrect usage.');
end

if (~exist('bIgnoreNaNs', 'var') || isempty(bIgnoreNaNs))
   bIgnoreNaNs = DEF_bIgnoreNaNs;
end

if (~any(tsSource.testsampletype(cstrPerimittedSampleTypes)))
   error('TimeSeries:Argument', ...
        ['*** mean_t: The sample type of ''tsSource'' must be one of {' sprintf('%s ', cstrPerimittedSampleTypes{:}) '}.']);
end

% - Check for an empty time series
if (isempty(tsSource))
   tfMean = [];
   return;
end

% - Determine time point weightings
vfSampleWeight = diff(tsSource.vtTimeTrace)/2;
dt1 = [vfSampleWeight(1);vfSampleWeight];
dt2 = [vfSampleWeight;vfSampleWeight(end)];
vfSampleWeight = dt1+dt2;

% - Weight samples
tfSamples = ones(size(tsSource.tfSamples));
tfSamples(:, :) = bsxfun(@times, double(tsSource.tfSamples(:, :)), vfSampleWeight) ./ nanmean(vfSampleWeight);

% - Compute sample mean
if (bIgnoreNaNs)
   tfMean = nanmean(tfSamples, 1);
else
   tfMean = mean(tfSamples, 1);
end

% --- END of mean_t.m ---
