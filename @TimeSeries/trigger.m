function ctfTriggeredSamples = trigger(tsDependent, tsTrigger, vtTriggerWindow, bReturnTS, bSkipChecks)

% trigger - METHOD Break apart a time series according to an event trigger
%
% Usage: ctfTriggeredSamples =   trigger(tsDependent, tsTrigger, vtTriggerWindow <, bReturnTS>)
%        vtsTriggeredSamplesTS = trigger(tsDependent, tsTrigger, vtTriggerWindow, bReturnTS = true)
%
% This method finds all samples within a dependent time series
% 'tsDependent', that fall close to an event in a trigger time series
% 'tsTrigger', within a relative temporal window defined by
% 'vtTriggerWindow' = ['tStart' 'tEnd'].
%
% 'ctfTriggeredSamples' will be a cell array, with size either [N 1] or
% [N 2], where 'N' is the number of events in 'tsTrigger'. If 'tsDependent'
% is an event time series, then 'ctfTriggeredSamples' will be an [N 1] cell
% array. Each element ctfTriggeredSamples{n, 1} contains the event times
% within the window 'vtTriggerWindow', relative to the time of the
% corresponding event in 'tsTrigger'.
%
% If 'tsDependent' has associated sample values (i.e. is not an event time
% series), then each element ctfTriggeredSamples{n, 2} contains the sample
% data associated with the corresponding sample times in
% ctfTriggeredSamples{n 1}.
%
% If 'bReturnTS' is 'true', then 'vtsTriggeredSamplesTS' will be an array
% of time series, where each element corresponds to a single event in
% 'tsTrigger' as before.

% Author: Dylan Muir <dylan.muir@unibas.ch>
%         Antonin Blot <antonin.blot@unibas.ch>
% Created: 15th June, 2016

% -- Defaults

DEF_bReturnTS = false;


% -- Check arguments

if ~exist('bReturnTS', 'var');
   bReturnTS = DEF_bReturnTS;
end

if (~exist('bSkipChecks', 'var') || ~bSkipChecks)
   if (nargin < 3)
      help trigger;
      error('TimeSeries:Usage', ...
         '*** TimeSeries/trigger: Incorrect usage.');
   end
   
   if (~testsampletype(tsTrigger, 'event'))
      error('TimeSeries:Arguments', ...
         '*** TimeSeries/trigger: ''tsTrigger'' must be an event time series.');
   end
   
   if (numel(vtTriggerWindow) ~= 2)
      error('TimeSeries:Arguments', ...
         '*** TimeSeries/trigger: ''vtTriggerWindow'' must be a vector [tStart tEnd].');
   end
end

% -- Catch an empty dependent time series
if (isempty(tsDependent))
   if (bReturnTS)
      ctfTriggeredSamples = tsDependent;
   else
      ctfTriggeredSamples = {[]};
   end
   return;
end

% -- Perform triggering

% - Get time traces
vtDependent = tsDependent.vtTimeTrace;
vtTrigger = tsTrigger.vtTimeTrace;
vnSampleSize = tsDependent.samplesize;
bEventDependent = testsampletype(tsDependent, 'event');

% - Allocate variables
if (bReturnTS)
   % - Preallocate with an empty time series
   ctfTriggeredSamples(1:numel(vtTrigger)) = TimeSeries([], [], tsDependent.strSampleType);
else
   % - Preallocate the cell array
   ctfTriggeredSamples = cell(numel(vtTrigger), 1);
end
mnBorders = nan(numel(vtTrigger), 2);

% - Short-circuit an empty dependent time series
if isempty(tsDependent)
   return;
end

% - Compute window boundaries, ensuring that they are within the dependent time series
tFirstDependent = vtDependent(1);
tLastDependent = vtDependent(end);
vtWinStart = vtTrigger + vtTriggerWindow(1);
vtWinEnd = vtTrigger + vtTriggerWindow(2);

% - Ignore triggers that include a NaN, or triggers outside the dependent time series
vbIgnoreTrigger = (vtWinEnd < tFirstDependent) | (vtWinStart > tLastDependent);
vbIgnoreTrigger = vbIgnoreTrigger | isnan(vtWinEnd) | isnan(vtWinStart);

vbFixStart = vtWinStart < tFirstDependent;
vtWinStart(vbFixStart) = tFirstDependent;
vtWinEnd = max(vtWinEnd, tFirstDependent);
vbFixEnd = vtWinEnd > tLastDependent;
vtWinEnd(vbFixEnd) = tLastDependent;

% - If there are no trigger events within dependent time series, don't do anything
vbFindSamples = ~vbIgnoreTrigger & ~vbFixStart;
if any(vbFindSamples)
    % - Find the sample indices that define the window start
    mnBorders(vbFindSamples, 1) = BinarySearchSortedList_mex(vtDependent, vtWinStart(vbFindSamples)) + 1; % - Because BSSL returns the sample *preceding* the target
end
vbFindSamples = ~vbIgnoreTrigger & ~vbFixEnd;
if any(vbFindSamples)
    % - Find the sample indices that define the window end
    mnBorders(vbFindSamples, 2) = BinarySearchSortedList_mex(vtDependent, vtWinEnd(vbFindSamples));
end

% - Fix up window starts and ends if they were clipped to the first or last sample
mnBorders(vbFixStart, 1) = 1;
mnBorders(vbFixEnd, 2) = numel(vtDependent);

% - Extract samples for each trigger event
for (nTrigger = find(~vbIgnoreTrigger)')
   % - Get sample indices for this trigger
   vnSampleIndices = mnBorders(nTrigger,1):mnBorders(nTrigger,2);
   
   % - Extract sample times relative to this trigger
   vtRelativeTimes = vtDependent(vnSampleIndices) - vtTrigger(nTrigger);
   
   % - Check for valid sample times
   vbValidSamples = (vtRelativeTimes >= vtTriggerWindow(1)) & (vtRelativeTimes <= vtTriggerWindow(2));
   vnSampleIndices = vnSampleIndices(vbValidSamples);
   
   % - Extract sample data corresponding to this trigger, if required
   if (bEventDependent)
      tfSamples = [];
   else
      tfSamples = reshape(tsDependent.tfSamples(vnSampleIndices, :), [numel(vnSampleIndices) vnSampleSize(2:end)]);
   end
   
   % - Generate return argument
   if (bReturnTS)
      ctfTriggeredSamples(nTrigger).vtTimeTrace = vtRelativeTimes(vbValidSamples);
      ctfTriggeredSamples(nTrigger).tfSamples = tfSamples;
   else
      ctfTriggeredSamples{nTrigger, 1} = vtRelativeTimes(vbValidSamples);
      ctfTriggeredSamples{nTrigger, 2} = tfSamples;
   end
end

% --- END of trigger.m ---
