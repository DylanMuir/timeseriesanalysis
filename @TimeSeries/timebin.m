function tsBinned = timebin(tsSource, vtBinEdges)

% timebin - FUNCTION Perform a temporal binning of a time series (bin average)
%
% Usage: tsBinned = timebin(tsSource, tBinDuration)
%        tsBinned = timebin(tsSource, vtBinEdges)
%
% 'tsSource' is a time series to perform binning on. Only 'continuous',
% 'count' and 'event' time series are supported. 'tBinDuration' is the
% temporal width of the bin to use.
%
% 'tsBinned' will be a time series containing the binned data. If
% 'tsSource' is an 'event' time series, then 'tsBinned' will be a 'count'
% time series containing the number of events in each time bin. If
% 'tsSource' is a 'count' or 'continuous' time series, then 'tsBinned' will
% be a 'continuous' time series containing the temporal average of each
% time bin.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 8th Septmeber, 2014

cstrPermittedTSType = {'continuous', 'count', 'event'};


% -- Check arguments

if (nargin < 2)
   help timebin;
   error('TimeSeries:Usage', ...
         '*** timebin: Incorrect usage.');
end

if (~tsSource.testsampletype(cstrPermittedTSType))
   error('TimeSeries:Argument', ...
         ['*** timebin: The sample type of ''tsSource'' must be one of {' sprintf('''%s'' ', cstrPermittedTSType{:}) '}' ]);
end


% -- Work out binning

% - Ensure we have a sorted column vector
vtBinEdges = sort(shiftdim(vtBinEdges(:)));

% - Check for reasonable time bin values
if (any(isnan(vtBinEdges)) || any(isinf(vtBinEdges)))
   error('TimeSeries:Argument', ...
         '*** timebin: ''vtBinEdges'' and ''tBinDuration'' must not contain ''nan'' or ''inf''.');
end

% - Was ''tBinDuration'' or ''vtBinEdges'' provided?
if isscalar(vtBinEdges)
   % - Make a uniform binning time vector
   tBinDuration = vtBinEdges;
   vtRange = tsSource.vtTimeBounds;
   vtBinEdges = shiftdim(vtRange(1):tBinDuration:(vtRange(2)+tBinDuration));
   

   % - Check whether time binning is necessary
   if (~tsSource.testsampletype('event')) && (nanmedian(diff(tsSource.vtTimeTrace)) > tBinDuration)
      warning('TimeSeries:NoBinning', ...
              '--- timebin: Time series resolution is coarser than binning resolution. No binning performed.')
      tsBinned = tsSource;
      return;
   end
   
else
   % - Get bin durations
   vtBinDurations = diff(vtBinEdges);
   tBinDuration = mean(vtBinDurations(:));
   
   % - Check for uniform binning
   if (any(abs(vtBinDurations - tBinDuration) > 2*eps(max(vtBinEdges))))
      warning('TimeSeries:Binning', ...
              '--- timebin: Warning: temporal binning must use equal-sized bins.');
   end
end

% - Calculate bin centres
vtBinCentres = mean([vtBinEdges(1:end-1) vtBinEdges(2:end)], 2);


% -- Perform binning over different sampling types

switch lower(tsSource.strSampleType)
   case 'continuous'
      tsBinned = TimeBinContinousTS(tsSource, tBinDuration, vtBinCentres);
      
   case 'count'
      tsSource.strSampleType = 'continuous';
      tsBinned = TimeBinContinousTS(tsSource, tBinDuration, vtBinCentres);
      
   case 'event'
      tsBinned = TimeBinEventTS(tsSource, vtBinEdges, vtBinCentres);
      
   otherwise
      error('TimeSeries:Unsupported', ...
            ['*** timebin: The sample type of ''tsSource'' must be one of {' sprintf('''%s'' ', cstrPermittedTSType{:}) '}' ]);
end

end

% --- END of timebin FUNCTION ---

% TimeBinContinuousTS - FUNCTION Perform temporal binning on a continuous
% time series
function tsBinned = TimeBinContinousTS(tsSource, tBinDuration, vtBinCentres)

% check if I can use convolution
vtOriginalTimeTrace = tsSource.vtTimeTrace;
vtLimTime = tsSource.vtTimeBounds;
tSourceResolution = nanmedian(diff(vtOriginalTimeTrace));
DEF_nNumTimePointsFast = 2^nextpow2(2e5);
nLengthPeriodicConvTrace = min(2*round(diff(vtLimTime)./tSourceResolution), DEF_nNumTimePointsFast);
tConvResolution = abs(2*diff(vtLimTime)) ./  nLengthPeriodicConvTrace;

% even when the resolution is about the same it can crash, add a 10% margin
if (tConvResolution* 1.1 > tSourceResolution )  
    method = 'exact';
    % for the exact method provides a vtFunctionWindow for speed
    vtFunctionWindow =  [-tBinDuration tBinDuration]/2;
else
    method = 'fast';
    vtFunctionWindow = [];
end 
    
% - Convolve time series with boxcar function (force continuous)
tsBinned = ConvolveTS(tsSource, @(vtTime)BoxcarNormFilter(vtTime, tBinDuration), method, vtFunctionWindow);

% - Resample time series
tsBinned = tsBinned.resample(vtBinCentres);

   function vfResponse = BoxcarNormFilter(vtTime, tBinDuration)
      vfResponse = double(abs(vtTime) < tBinDuration/2) ./ tBinDuration;
   end
end


function tsBinned = TimeBinCountTS(tsSource, vtBinCentres) %#ok<DEFNU>

% - Perform temporal binning
tsBinned = ResampleTS(tsSource, vtBinCentres);

end


function tsBinned = TimeBinEventTS(tsSource, vtBinEdges, vtBinCentres)

% - Get event times
vtEventTimes = tsSource.vtTimeTrace;

% - Perform binning
[vnCounts, vnBinIndex] = histc(vtEventTimes, vtBinEdges);

% - Incorporate last time bin
vnCounts(end-1) = sum(vnCounts(end-1:end));
vnCounts = vnCounts(1:end-1);

% - Construct a count time series
tsBinned = TimeSeries(vtBinCentres, vnCounts, 'count');

end




% --- END of timebin.m ---
