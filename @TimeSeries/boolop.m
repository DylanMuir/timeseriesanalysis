function [tsBoolean] = boolop(tsSource, fhOperator)

% BoolOpTS - METHOD Perform a sample-wise operator on a time series, resulting in a boolean time series
%
% Usage: [tsBoolean] = boolop(tsSource, fhOperator)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

% -- Check arguments

if (nargin < 1)
   help boolop;
   error('TimeSeries:Usage', '*** boolop: Incorrect usage.')
end
   

% -- Check for event time series

if (testsampletype(tsSource, 'event'))
   if (nargin > 1)
      error('TimeSeries:Arguments', '*** boolop: ''fhOperator'' must not be supplied for an event time series.')
   end      
   
   % - Just convert to a boolean time series, which is "true" when events occur
   vtTimeTrace = [tsSource.vtTimeTrace-eps(tsSource.vtTimeTrace) tsSource.vtTimeTrace tsSource.vtTimeTrace+eps(tsSource.vtTimeTrace)]';
   tfSamples = [false(size(tsSource.vtTimeTrace)) true(size(tsSource.vtTimeTrace)) false(size(tsSource.vtTimeTrace))]';
   tsBoolean = TimeSeries(vtTimeTrace(:), tfSamples(:));

else
   if (nargin < 2)
      error('TimeSeries:Arguments', '*** boolop: ''fhOperator'' must be supplied for a non-event time series.')
   end
   
   % - Perform sample-wise operation
   tsBoolean = TimeSeries(tsSource.vtTimeTrace, logical(fhOperator(tsSource.tfSamples)), 'boolean');
end

% - Record as a boolean time series
tsBoolean.strSampleType = 'boolean';


% --- END of boolop.m ---
