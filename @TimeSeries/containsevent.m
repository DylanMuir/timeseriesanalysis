function [bContainsEvent] = containsevent(tsEvent, vtTestTime)

% containsevent - METHOD Test whether an event time series contains an event
%
% Usage: [bContainsEvent] = containsevent(tsEvent, vtTestTime)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 18th August, 2015

% -- Check arguments

if (nargin < 1)
   help containsevent;
   error('TimeSeries:Usage', '*** containsevent: Incorrect usage.');
end

if (~TestSampleTypeTS(tsEvent, 'event'))
   error('TimeSeries:Arguments', '*** containsevent: ''tsEvent'' must be an event time series.');
end


% -- Test time series

if (nargin < 2)
   bContainsEvent = ~isempty(tsEvent.vtTimeTrace) && ~all(isnan(tsEvent.vtTimeTrace));
   
else
   if (numel(vtTestTime) ~= 2)
      error('TimeSeries:Arguments', '*** ContainsEventTS: ''vtTestTime'' must be [''tStart'' ''tEnd''].');
   end
   
   vtEventTimes = tsEvent.vtTimeTrace(~isnan(tsEvent.vtTimeTrace));
   bContainsEvent = ~isempty(vtEventTimes) && any((vtEventTimes >= vtTestTime(1)) & (vtEventTimes <= vtTestTime(2)));
end

% --- END of containsevent.m ---