function tfValue = sample(tsSource, tSampleTimes)

% sample - METHOD Return values associated with particular sample times
%
% Usage: tfValue = sample(tsSource, tSampleTimes)
%

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 1st February, 2018

if (prod(tsSource.samplesize) > 1) && (size(tSampleTimes(:, :), 2) > 1)
   error('TimeSeries:Arguments', ...
         'A matrix of sample times can only be provided for unitary time series.');
end

vnOrigSize = size(tSampleTimes);

tfValue = tsSource.fhInterpolation(tsSource, tSampleTimes);
tfValue = reshape(tfValue, vnOrigSize);

% --- END of sample.m ---