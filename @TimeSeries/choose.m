% choose - METHOD Select a subset of time series from a source TS
%
% Usage: ts.choose(<subscript indices>)
%
% Examples:
%    ts.choose(1)    % - Return a time series which contains the first time series in 'ts'
%    ts.choose(1, :) % - Return a time series containing a slice of a 2D time series

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 2015

function tsSubset = choose(tsSource, varargin)
   % - Make a copy of the source time series
   tsSubset = tsSource;

   % - What sort of referencing is being used?
   if isstruct(varargin{1})
      if all(isfield(varargin{1}, {'ImageSize', 'PixelIdxList', 'NumObjects'}))
         % - Check matching image sizes
         vnTSSampleSize = tsSource.samplesize();
         if (~isequal(vnTSSampleSize(2:end), varargin{1}.ImageSize))
            error('TimeSeries:Arguments', ...
                  '*** TimeSeries/choose: The image size defined in ''sROIs'' must match the sample size of the time series.');
         end
         
         % - Replicate out time series
         nNumROIs = varargin{1}.NumObjects;
         tfSamples = tsSubset.tfSamples;
         tsSubset.tfSamples = [];
         tsSubset = repmat(tsSubset, 1, nNumROIs);
         
         % - Pick out ROI traces
         for (nROI = 1:nNumROIs)
            tsSubset(nROI).tfSamples = tfSamples(:, varargin{1}.PixelIdxList{nROI});
         end
         
      else % - Incorrect struct format
         error('TimeSeries:Arguments', ...
               '*** TimeSeries/choose: ''sROIs'' must be a ROI structure as returned by ''bwconncomp''.');
      end
      
   else % - Standard matlab subreferencing
      % - Reference 'tfSamples'
      tsSubset.tfSamples = tsSubset.tfSamples(:, varargin{:});
   end
end

% --- END of choose.m ---
