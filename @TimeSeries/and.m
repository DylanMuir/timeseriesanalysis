function [tsAndTS] = and(tsA, tsB)

% and - METHOD Perform a boolean AND of two boolean time series
%
% Usage: [tsAndTS] = and(tsA, tsB)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

% -- Check arguments

if (nargin < 2)
   help TimeSeries/and;
   error('TimeSeries:Usage', '*** TimeSeries/and: Incorrect usagge.');
end

if (~testsampletype(tsA, 'boolean') || ~testsampletype(tsB, 'boolean'))
   error('TimeSeries:Arguments', '*** TimeSeries/and: Both ''tsA'' and ''tsB'' must be boolean time series.');
end


% - Resample 'tsB' to 'tsA'
tsB = tsB.resample(tsA.vtTimeTrace);

% - Perform boolean 'and'
tsAndTS = tsA;
tsAndTS.tfSamples = (tsAndTS.tfSamples == true) & (tsB.tfSamples == true); % nan == false

% --- END of and.m ---
