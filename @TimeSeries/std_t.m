% std_t - METHOD Compute the standard deviation of a time series, over time
%
% Usage: tfStd = std_t(tsSource)
%
% 'tsSource' is a continuous or count time series. The sample infinite-time
% standard deviation for each series in 'tsSource' will be computed and
% returned in 'tfVar'. 'tfVar' will be of size [1 M N ...], corresponding
% to the size of the time series in 'tsSource'.
%
% Samples will be weighted according the time duration they represent, for
% irregularly-sampled time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

function tfStd = std_t(tsSource)

tfStd = sqrt(tsSource.var_t());

% --- END of std_t.m ---
