function tsSum = plus(tsA, tsB)

% plus (+) - METHOD Add two time series by value
%
% Usage: tsSum = tsA + tsB
%        tsSum = plus(tsA, tsB)
%
% Addition can only be performed on countable time series (i.e.
% 'continuous' or 'count' time series). The summed time series will have
% the same time base as 'tsA'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 23rd April, 2015

% -- Check arguments

if (nargin < 2)
   help plus;
   error('TimeSeries:Usage', ...
         '*** plus: Incorrect usage.');
end

% - Use sampleshift method to perform addition
tsSum = sampleshift(tsA, tsB);

% --- END of plus.m ---