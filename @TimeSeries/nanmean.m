function tsMean = nanmean(varargin)

% nanmean - FUNCTION Compute the mean (amplitude-wise) of several time series, ignoring NaNs
%
% Usage: tsMean = nanmean(ts1 <, ts2, ...>)
%
% nanmean will compute the mean of several time series. Unitary and
% non-unitary time series can be supplied. The time base of the mean will be
% the interleaved time bases of the source time series. Time series will be
% interpolated, when necessary, according to the interpolation function set
% for each time series. This function ignores NaNs, similar in behaviour to
% nanmean.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 20th April, 2015

% - Check arguments

if (nargin < 1)
   help nanmean;
   error('TimeSeries:Usage', ...
         '*** nanmean: Incorrect usage.');
end

% - Use sum to compute sum
[tsMean, vnNumSeriesInSum] = sum(varargin{:}, 'IgnoreNaNs', true);

% - Compute amplitude-wise mean
tsMean.tfSamples = tsMean.tfSamples ./ vnNumSeriesInSum;

% --- END of nanmean.m ---