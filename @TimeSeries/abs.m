function tsAbs = abs(tsSource)

% abs - METHOD Compute the absolute value of a time series
%
% Usage: tsAbs = abs(tsSource)
%
% abs will compute the absolute value of a time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 15th September, 2015

% - Constants

TSA_cstrAllowedSampleTypes = {'continuous', 'count'};


% - Check arguments

if (nargin < 1)
   help abs;
   error('TimeSeries:Usage', ...
         '*** abs: Incorrect usage.');
end

if (~any(testsampletype(tsSource, TSA_cstrAllowedSampleTypes)))
   error('TimeSeries:Arguments', ...
         ['*** abs: ''tsSource'' must be have a sample type of {', sprintf('%s ', TSA_cstrAllowedSampleTypes{:}), '}']);
end

tsAbs = tsSource;
tsAbs.tfSamples = abs(tsAbs.tfSamples);

% --- END of abs.m ---