function cSegments = segments(tsSeries)

% segments - METHOD Divide a time series up into contiguous valued segments
%
% Usage: [cSegments] = segments(tsSeries)
%
% 'tsSeries' is a categorical, count or boolean unitary time series.
% 'cSegments' will be a cell array, with one entry per continugous-valued
% time segment in 'tsSeries'. Each cell will be {[tStart tEnd] fValue},
% where 'tStart' and 'tEnd' identify the start and end times of the
% segment, and 'fValue' contains the category, count or boolean value of
% the time series during that segment.
%
% Boolean time series are handled slightly differently, in that only
% segments of value "true" are returned.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 18th August, 2015

% -- Defaults

DEF_cstrAllowedSampleTypes = {'categorical', 'count', 'boolean'};


% -- Check arguments

if (nargin < 1)
   help segments;
   error('TimeSeries:Usage', '*** segments: Incorrect usage.');
end

if (~any(testsampletype(tsSeries, DEF_cstrAllowedSampleTypes)))
   error('TimeSeries:Arguments', ...
      ['*** segments: ''tsSeries'' must have a sample type one of {' sprintf('''%s'', ', DEF_cstrAllowedSampleTypes{:}) '}.']);
end

if (prod(samplesize(tsSeries)) > 1)
   error('TimeSeries:Arguments', '*** segments: ''tsSeries'' must be a unitary time series.');
end


% -- Enumerate segments

% - Get runs
[vnRunVals, vnRunStarts, vnRunLengths] = TSS_VectorRuns(tsSeries.tfSamples);

% - Filter boolean time series to only "true" segments
if (testsampletype(tsSeries, 'boolean'))
   vbKeepSegment = vnRunVals;
   vnRunVals = vnRunVals(vbKeepSegment);
   vnRunStarts = vnRunStarts(vbKeepSegment);
   vnRunLengths = vnRunLengths(vbKeepSegment);
end


% -- Produce cell array of segments

vnRunEnds = vnRunStarts + vnRunLengths-1;
vnRunEnds(vnRunEnds > numel(tsSeries.vtTimeTrace)) = numel(tsSeries.vtTimeTrace);

for (nSegment = numel(vnRunVals):-1:1)
   cSegments{nSegment} = {tsSeries.vtTimeTrace([vnRunStarts(nSegment) vnRunEnds(nSegment)]) vnRunVals(nSegment)};
end

% --- END of segments FUNCTION ---


% SETS_VectorRuns - FUNCTION Split up a series into runs
function [vnRunVals, vnRunStarts, vnRunLengths] = TSS_VectorRuns(vnVector)

vnVector = reshape(vnVector, 1, []);

vnRunStarts = find([true diff(vnVector)] ~= false);
vnRunEnds = [vnRunStarts(2:end)-1 numel(vnVector)];
vnRunLengths = vnRunEnds - vnRunStarts + 1;
vnRunVals = vnVector(vnRunStarts);


% --- END of segments.m ---