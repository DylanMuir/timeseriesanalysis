function [tsNotTS] = not(tsA)

% not - METHOD Perform a boolean NOT of a boolean time series
%
% Usage: [tsNotTS] = not(tsA)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

% -- Check arguments

if (nargin < 1)
   help TimeSeries/not;
   error('TimeSeries:Usage', '*** TimeSeries/not: Incorrect usagge.');
end

if (~testsampletype(tsA, 'boolean'))
   error('TimeSeries:Arguments', '*** TimeSeries/not: ''tsA'' must be a boolean time series.');
end


% - Perform boolean 'not'
tsNotTS = tsA;
tsNotTS.tfSamples = ~(tsNotTS.tfSamples == true);  % nan == false

% --- END of not.m ---
