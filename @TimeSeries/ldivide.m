function [tsDivided] = ldivide(tsDenominator, tsNumerator)

% ldivide - METHOD left division (.\)
%
% Usage: [tsDivided] = ldivide(tsDenominator, tsNumerator)
%
% See 'divide' for more information

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 23rd September, 2015

% - Check arguments
if (nargin < 2)
   help ldivideTS;
   error('TimeSeries:Usage', ...
         '*** ldivide: Incorrect usage.');
end

% - Just call divide
tsDivided = divide(tsNumerator, tsDenominator);

% --- END of ldivide.m ---
