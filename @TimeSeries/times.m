function tsMul = times(tsA, tsB)

% times (.*) - METHOD Multiply two time series by value
%
% Usage: tsMul = tsA .* tsB
%        tsMul = times(tsA, tsB)
%
%        tsMul = fScalar .* tsB
%        tsMul = tsA .* fScalar
%        tsMul = times(fScalar, tsB)
%        tsMul = times(tsA, fScalar)
%
% Multiplication can only be performed on countable time series (i.e.
% 'continuous' or 'count' time series). The resulting time series will have
% the same time base as 'tsA', or as the single source time series in the
% case of multiplication by a scalar.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 23rd April, 2015

DEF_cstrPermittedSampleTypes = {'continuous', 'count'};


% -- Check arguments

if (nargin < 2)
   help times;
   error('TimeSeries:Usage', ...
         '*** times: Incorrect usage.');
end

% - Deal with an array of source time series
if (~isscalar(tsA))
   tsMul = arrayfun(@(ts)timesTS(ts, tsB), tsA);
   return;
end

% - Deal with a scalar multiplicand or source
if (isscalar(tsB) && isnumeric(tsB))
   tsMul = tsA;
   tsMul.tfSamples = double(tsMul.tfSamples) .* double(tsB);
   return;
end

if (isscalar(tsA) && isnumeric(tsA))
   tsMul = tsB;
   tsMul.tfSamples = double(tsMul.tfSamples) .* double(tsA);
   return;
end

% - Test for countable time series
if (~tsA.testsampletype(DEF_cstrPermittedSampleTypes) || ~tsB.testsampletype(DEF_cstrPermittedSampleTypes))
   error('TimeSeries:Arguments', ...
         '*** times: Both time series must be of class ''continuous'' or ''count''.');
end

% - Both time series must have same sample sizes, or one must be a scalar
vnSampleSizeA = tsA.samplesize;
vnSampleSizeB = tsB.samplesize;

if (prod(vnSampleSizeA) ~= 1) && (prod(vnSampleSizeB) ~= 1) && ~isequal(vnSampleSizeA, vnSampleSizeB)
   error('TimeSeries:Arguments', ...
         '*** times: Time series must have the same sample sizes, or one must be unitary.')
end

% - Resample tsB in order to perform multiplication
tsB = tsB.resample(tsA.vtTimeTrace);

% - Determine result size
vnResultSize = [size(tsA.tfSamples, 1) max(vnSampleSizeA(2:end), vnSampleSizeB(2:end))];

% - Multiply time series
tsMul = tsA;
tsMul.tfSamples = ones(vnResultSize);  % - Upcast everything to double
tsMul.tfSamples(:, :) = bsxfun(@times, tsA.tfSamples(:, :), tsB.tfSamples(:, :));   % - Colon referencing used to handle mapped tensors and stacks

% --- END of times.m ---
