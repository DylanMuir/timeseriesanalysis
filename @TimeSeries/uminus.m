function tsNeg = uminus(ts)

% uminus (-) - METHOD Negate a time series, by value
%
% Usage: tsNeg = -ts;
%        tsNeg = uminus(ts);
%
% Negation can only be performed on countable time series (i.e. of class
% 'continuous' or 'count').

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 23rd April, 2015

% -- Check arguments

if (nargin < 1)
   help uminus;
   error('TimeSeries:Usage', ...
         '*** uminus: Incorrect usage.');
end

% - Check class of time series
if (~testsampletype(ts, {'continuous', 'count'}))
   error('TimeSeries:Arguments', ...
         '*** uminus: Input time series must be of class ''continuous'' or ''count''.');
end

% - Negate samples
tsNeg = ts;
tsNeg.tfSamples = -tsNeg.tfSamples;

% --- END of uminus.m ---