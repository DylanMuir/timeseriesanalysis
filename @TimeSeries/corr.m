function [fCorrCoeff, fPValuePearson] = corr(tsA, tsB)

% corr - METHOD Compute the correlation coefficient between two unitary time series
%
% Usage: [fCorrCoeff, fPValuePearson] = corr(tsA, tsB)
%
% 'tsA' and 'tsB' are two unitary time series. 'tsB' will be resampled
% using the time base from 'tsA', and the linear correlation coefficient
% between sample traces will be computed. Any NaN samples will be ignored.
% If either of 'tsA' or 'tsB' is empty, or all-NaN, then 'nan' will be
% returned.
%
% Note that currently no correction for irrelgular sampling is made.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 16th April, 2015

% -- Check arguments

if (nargin < 2)
   help corr;
   error('TimeSeries:Usage', ...
         '*** corr: Incorrect usage.');
end


% - Check time series sizes

if ((size(tsA.tfSamples, 2) > 1) || (size(tsB.tfSamples, 2) > 1))
   error('TimeSeries:Arguments', ...
         '*** corr: ''tsA'' and ''tsB'' must both be unitary time series.');
end


% - Trim and resample time series B
tsB = ResampleTS(tsB, tsA.vtTimeTrace);

% - Remove NaNs
vbIsNan = isnan(tsA.tfSamples) | isnan(tsB.tfSamples);

if any(~vbIsNan)
   tsA.tfSamples = tsA.tfSamples(~vbIsNan);
   tsB.tfSamples = tsB.tfSamples(~vbIsNan);

   % - Compute correlation coefficient
   [fCorrCoeff, fPValuePearson] = corr(tsA.tfSamples, tsB.tfSamples);
else
   fCorrCoeff = nan;
   fPValuePearson = nan;
end

% --- END of corr.m ---
