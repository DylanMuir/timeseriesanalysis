function tsConcatenated = concatenate(varargin)

% concatenate - METHOD Concatenate several time series back to back
%
% Usage: tsConcatenated = concatenate(tsSeries1, tsSeries2, ...)
%
% 'tsSeriesX' must all be time series with the same sampling type, the same
% number of time series and the same sample ranges.
%
% 'tsConcatenated' will be a time series containing all the time samples
% from 'tsSeriesX' placed back-to-back, such that the time bases of each
% series are shifted to follow one another.
%
% Concatenated time series will always be shifted to start at T=0.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

% -- Check arguments

if (nargin < 1)
   help concatenate;
   error('TimeSeries:Usage', ...
         '*** concatenate: Incorrect usage.');
end

% - Convert from a cell array argument
if iscell(varargin{1})
   varargin = varargin{1};
end

if (~all(cellfun(@istimeseries, varargin)))
   error('TimeSeries:Arguments', ...
         '*** TimeSeries/concatenate: All inputs must be TimeSeries objects.');
end


% -- Collect information

% - Short-cuts
if (numel(varargin) == 1)
   if (numel(varargin{1}) == 1)
      % - Single time series
      tsConcatenated = varargin{1}.timeshift(varargin{1}.vtTimeBounds(1));
      return;
      
   else
      % - Split out array of time series
      varargin = reshape(num2cell(varargin{1}), 1, []);
   end
end

% - Collect time series together
varargin = cellfun(@shiftdim, varargin, 'UniformOutput', false);
vtsTimeSeries = vertcat(varargin{:});

% - Collect all samples
ctfSamples = shiftdim({vtsTimeSeries.tfSamples});

% - Collect all time traces
cvtTimeTraces = shiftdim({vtsTimeSeries.vtTimeTrace});

% - Collect all ranges
mfSampleRanges = vertcat(vtsTimeSeries.vfSampleRange);

% - Collect time series sample types
cstrSampleTypes = shiftdim({vtsTimeSeries.strSampleType});


% -- Check for compatible time series

% - Check sample sizes
vnSampleSize = varargin{1}.samplesize;
if (~all(cellfun(@(c)isequal(c.samplesize, vnSampleSize), varargin(2:end))))
   error('TimeSeries:Incompatible', ...
      '*** concatenate: Incompatible time series for concatenation: Sample sizes differ.')
end

if (~all(cellfun(@(s)strcmpi(s, cstrSampleTypes{1}), cstrSampleTypes)))
   error('TimeSeries:Incompatible', ...
      '*** concatenate: Incompatible time series for concatenation: Sample types differ.')
end

if (~isequal(mfSampleRanges, repmat(mfSampleRanges(1, :), numel(vtsTimeSeries), 1)))
   warning('TimeSeries:Incompatible', ...
      '--- concatenate: Warning: Potentially incompatible time series for concatenation: Sample ranges differ.')
end



% -- Concatenate time vectors

% - Remove empty time series
vbIsEmpty = arrayfun(@isempty, vtsTimeSeries);

% - Were they all empty?
if all(vbIsEmpty)
   tsConcatenated = vtsTimeSeries(1);
   return;
end

% - Determine start time, end time and duration for each time series
vtStartTime = cellfun(@(c)(c.vtTimeBounds(1)), varargin);
vtEndTime = cellfun(@(c)(c.vtTimeBounds(2)), varargin);
vtStartTime = reshape(vtStartTime, [], 1);
vtEndTime = reshape(vtEndTime, [], 1);

% - Shift all time traces to start at zero
cvtTimeTraces = cellfun(@(vt, start)(vt - start), ...
                        cvtTimeTraces, num2cell(vtStartTime), ...
                        'UniformOutput', false);

% - Collect final times for each time trace
vtFinalTimes = [0; cumsum(vtEndTime - vtStartTime)];
cvtFinalTimes = num2cell(vtFinalTimes(1:end-1));

% - Add progressive final times to each time trace
cvtTimeTraces = cellfun(@(vt, o)(vt + o), cvtTimeTraces, cvtFinalTimes, 'UniformOutput', false);


% -- Create the concatenated time series

tsConcatenated = vtsTimeSeries(1);
tsConcatenated.tfSamples = cell2mat(ctfSamples(~vbIsEmpty));
tsConcatenated.vtTimeTrace = cell2mat(cvtTimeTraces(~vbIsEmpty));

% - Fix up time series bounds
if (~isempty(tsConcatenated.vtTimeBounds))
   tsConcatenated.vtTimeBounds = [0 vtFinalTimes(end)];
end

% --- END of concatenate.m ---
