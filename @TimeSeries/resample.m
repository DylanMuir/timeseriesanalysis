function [tsResampled] = resample(tsSource, vtTargetTimeBase)

% resample - METHOD Resample a time series to a target time base
%
% Usage: [tsResampled] = resample(tsSource, vtTargetTimeBase)
%
% resample uses the defined interpolation function to resample a time
% series 'tsSource', to a target time base.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: September 2014

switch tsSource.strSampleType
   case 'event'
      % - We can't really resample, so just return the original series
      tsResampled = tsSource;
      
      % - Set time bounds
      vtTimeBounds = [min(vtTargetTimeBase(:)) max(vtTargetTimeBase(:))];
      tsResampled.vtTimeBounds = vtTimeBounds;
      
      % - Trim events outside bounds
      vbIncludeEvent = (tsResampled.vtTimeTrace >= vtTimeBounds(1)) & (tsResampled.vtTimeTrace <= vtTimeBounds(2));
      tsResampled.vtTimeTrace = tsResampled.vtTimeTrace(vbIncludeEvent);
      
   otherwise
      % - Call the supplied interpolation function
      tsResampled = TimeSeries(vtTargetTimeBase, tsSource.fhInterpolation(tsSource, vtTargetTimeBase), ...
                               tsSource.strSampleType, tsSource.vfSampleRange, tsSource.fhInterpolation);
end

% --- END of resample.m ---
