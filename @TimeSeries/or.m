function [tsAndTS] = or(tsA, tsB)

% and - METHOD Perform a boolean OR of two boolean time series
%
% Usage: [tsAndTS] = or(tsA, tsB)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

% -- Check arguments

if (nargin < 2)
   help TimeSeries/or;
   error('TimeSeries:Usage', '*** TimeSeries/or: Incorrect usagge.');
end

if (~TestSampleTypeTS(tsA, 'boolean') || ~TestSampleTypeTS(tsB, 'boolean'))
   error('TimeSeries:Arguments', '*** TimeSeries/or: Both ''tsA'' and ''tsB'' must be boolean time series.');
end


% - Construct interleaved time base
cvtTimeBases = cellfun(@(c)(c.vtTimeTrace), {tsA, tsB}, 'UniformOutput', false);
vtSharedTimeBase = unique(vertcat(cvtTimeBases{:}));

% - Resample onto shared time base
tsA = ResampleTS(tsA, vtSharedTimeBase);
tsB = ResampleTS(tsB, vtSharedTimeBase);

% - Perform boolean 'or'
tsAndTS = tsA;
tsOrTS.tfSamples = (tsOrTS.tfSamples == true) | (tsB.tfSamples == true);   % nan == false

% --- END of or.m ---
