function vtsSplit = split(tsSource, vnSeriesIndices)

% split - FUNCTION Split multiple time series traces into separate objects
%
% Usage: vtsSplit = split(tsSource <, vnSeriesIndices>)
%
% 'vnSeriesIndices' is a vector of indices into the time series in
% 'tsSource'. If 'tsSeriesIndices' is not provided, then all unitary time
% series within 'tsSource' will be returned.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 4th September, 2014

% -- Check arguments

if (nargin < 1)
   help split;
   error('TimeSeries:Usage', ...
         '*** split: Incorrect usage');
end

% - Get size of souce time series
vnSampleSize = GetSampleSizeTS(tsSource);
[nNumTimePoints, nNumTimeSeries] = size(tsSource.tfSamples);

% - Check selected indices
if (~exist('vnSeriesIndices', 'var') || isempty(vnSeriesIndices))
   vnSeriesIndices = 1:nNumTimeSeries;
end

if (any(rem(vnSeriesIndices, 1) > eps(vnSeriesIndices)) || any(vnSeriesIndices < 1) || any(vnSeriesIndices > nNumTimeSeries))
   error('TimeSeries:Arguments', ...
      '*** SplitsTS: ''vnSeriesIndices'' must be valid subscript indices, within the range [1..%d]\n', nNumTimeSeries);
end

% - Do we even need to split?
if ((nNumTimeSeries == 1) && numel(vnSeriesIndices) == 1)
   vtsSplit = tsSource;
   return;
end

% - Extract samples
tfSamples = tsSource.tfSamples(:, vnSeriesIndices);
tsSource.tfSamples = [];

% - Produce new time series
nNumTraces = numel(vnSeriesIndices);
vtsSplit = repmat(tsSource, 1, nNumTraces);

% - Deal out slices of tfSamples
[vtsSplit.tfSamples] = expand(mat2cell(tfSamples, nNumTimePoints, ones(1, nNumTraces)));

% --- END of split FUNCTION ---


function varargout = expand(cObject)
varargout = cObject;

% --- END of split.m ---

