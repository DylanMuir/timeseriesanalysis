function [tsDivided] = rdivide(tsNumerator, tsDenominator)

% rdivide - METHOD right division (./)
%
% Usage: [tsDivided] = rdivide(tsNumerator, tsDenominator)
%
% See 'divide' for more information

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 23rd September, 2015

% - Check arguments
if (nargin < 2)
   help rdivideTS;
   error('TimeSeries:Usage', ...
         '*** rdivide: Incorrect usage.');
end

% - Just call divide
tsDivided = divide(tsNumerator, tsDenominator);

% --- END of rdivide.m ---
