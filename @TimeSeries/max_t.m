function tfMax = max_t(tsSource)

% max_t - METHOD Compute the maximum sample value of a time series, over
% time. NaN samples are ignored.
%
% Usage: tfMax = max_t(tsSource)
%        tfMax = tsSource.max_t()

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 8th August, 2016

tfMax = nanmax(tsSource.tfSamples, [], 1);

% --- END of max_t.m ---
