function tsExcised = excise(tsSource, vtTimeLimits, bLeaveNaNs)

% excise - FUNCTION Excise samples from a time series without resampling
% 
% Usage: tsExcised = excise(tsSource, vtTimeLimits <, bLeaveNaNs>)
%
% 'tsSource' is a time series. 'vtTimeLimits' is a vector [tStartTime tEndTime]
% which defines a time range to excise from 'tsSource' (excluding end points).
%
% The optional argument 'bLeaveNaNs' determines whether samples are
% replaced with NaN, or completely excised. By default 'bLeaveNaNs' is
% 'false', indicating that samples are completely excised.
%
% 'tsExcised' will be a new time series, containing all data from the
% source time series, excluding portions falling within 'vtTimeLimits'.
%
% See also the trim method, which keeps only the data between 'vtTimeLimits'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: September 2014

DEF_bLeaveNaNs = false;


% -- Check arguments

if (nargin < 2)
   help excise;
   error('TimeSeries:Usage', ...
         '*** excise: Incorrect usage.');
end

if (~exist('bLeaveNaNs', 'var') || isempty(bLeaveNaNs))
   bLeaveNaNs = DEF_bLeaveNaNs;
end

% - Handle case when called with a boolean time series as the second argument
if (istimeseries(vtTimeLimits))
   tsSegments = vtTimeLimits;
   
   % - Test for boolean sample type
   if (~testsampletype(tsSegments, 'boolean'))
      error('TimeSeries:Arguments', ...
         '*** TimeSeries/excise: When called with two time series, the second time series must have ''boolean'' sample type.');
   end
   
   % - Test for unitary samples
   if (prod(samplesize(tsSegments)) > 1)
      error('TimeSeries:Arguments', ...
         '*** TimeSeries/excise: When called with two time series, the second time series must have unitary samples.');
   end
   
   % - Loop over segments to excise
   tsExcised = tsSource;
   for (cExciseSegment = segments(tsSegments))
      tsExcised = excise(tsExcised, cExciseSegment{1}{1}, bLeaveNaNs);
   end
   
   % - Return excised time series
   return;
end

% - Find first and last time point
vtTimeLimits = [nanmin(vtTimeLimits(:)); nanmax(vtTimeLimits(:))];

% - Find time points within the original time series that can be included
vbBeforeExcision = tsSource.vtTimeTrace < vtTimeLimits(1);
vbAfterExcision = tsSource.vtTimeTrace > vtTimeLimits(2);

% - Copy source time series
tsExcised = tsSource;
vnSampleSize = size(tsSource.tfSamples);

% - Catch an empty time series
if ((numel(tsExcised.vtTimeTrace) == 0) || all(isnan(tsExcised.vtTimeTrace)))
   return;
end

% - Trim for different sample types
switch lower(tsSource.strSampleType)
   case {'continuous', 'count', 'categorical'}
      if (bLeaveNaNs)
         % - Replace samples with NaNs
         tsExcised.tfSamples(~(vbBeforeExcision | vbAfterExcision), :) = nan;
         
      else
         % - Completely excise samples
         % - Interpolate to find excision points
         tsExcised.tfSamples = [reshape(tsExcised.tfSamples(vbBeforeExcision, :), [], vnSampleSize(2:end));
                                tsExcised.fhInterpolation(tsSource, vtTimeLimits(1));
                                nan([1, vnSampleSize(2:end)]);
                                tsExcised.fhInterpolation(tsSource, vtTimeLimits(2));
                                reshape(tsExcised.tfSamples(vbAfterExcision, :), [], vnSampleSize(2:end))];
      
         % - Include excision time points in time trace
         tsExcised.vtTimeTrace = [tsExcised.vtTimeTrace(vbBeforeExcision); vtTimeLimits([1 1 2]); tsExcised.vtTimeTrace(vbAfterExcision)];
      end
      
   case 'event'
      % - Excise the time series
      tsExcised.vtTimeTrace = tsExcised.vtTimeTrace(vbBeforeExcision|vbAfterExcision);
      
   otherwise
      % - Not supported
      error('TimeSeries:Unsupported', ...
            '*** excise: Excise is not supported for time series of class ''%s''.', tsSource.strSampleType);
end

% --- END of excise.m ---
