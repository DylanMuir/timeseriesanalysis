function tsSum = nansum(varargin)

% nansum - FUNCTION Compute the sum (amplitude-wise) of several time series, ignoring NaNs
%
% Usage: tsSum = nansum(ts1 <, ts2, ...>)
%
% nansum will compute the sum of several time series. Unitary and
% non-unitary time series can be supplied. The time base of the sum will be
% the interleaved time bases of the source time series. Time series will be
% interpolated, when necessary, according to the interpolation function set
% for each time series. This function ignores NaNs, similar in behaviour to
% nansum.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 20th April, 2015

% - Check arguments

if (nargin < 1)
   help nansum;
   error('TimeSeries:Usage', ...
         '*** nansum: Incorrect usage.');
end

% - Use sum to compute sum
tsSum = sum(varargin{:}, 'IgnoreNaNs', true);

% --- END of nansum.m ---