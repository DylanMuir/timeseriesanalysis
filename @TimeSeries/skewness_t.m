% skewness_t - METHOD Overloaded skewness method for TimeSeries objects
%
% Usage: mfSkewness = skewness_t(ts, <flag>)
%
% This method computes the skewness of the time series data samples, over
% the time dimension. Note that no weighting or adjustment is made for
% irregularly-sampled time series.
%
% 'flag' is a boolean flag controlling bias correction. See the
% documentation for built-in skewness function for details.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 15th January, 2018

function mfSkewness = skewness_t(ts, flag)

% - By default, do not correct for bias
if (~exist('flag', 'var') || isempty(flag))
   flag = 1;
end

% - Compute skewness over samples
mfSkewness = skewness(ts.tfSamples, flag, 1);

% --- END of skewness_t.m ---
