function tsMean = mean(varargin)

% mean - FUNCTION Compute the mean (amplitude-wise) of several time series
%
% Usage: tsMean = mean(ts1 <, ts2, ...>)
%                 mean(..., 'IgnoreNaNs', bIgnoreNaNs)
%
% mean will compute the mean of several time series. Unitary and
% non-unitary time series can be supplied. The time base of the mean will be
% the interleaved time bases of the source time series. Time series will be
% interpolated, when necessary, according to the interpolation function set
% for each time series.
%
% If the parameter 'IgnoreNaNs' is set to 'true', then NaN values will be
% ignored (similar behaviour to nanmean). By default, NaN values will
% propagate into the sum (similar behaviour to mean).

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 20th April, 2015

% - Check arguments

if (nargin < 1)
   help mean;
   error('TimeSeries:Usage', ...
         '*** mean: Incorrect usage.');
end

% - Check for property / value pairs
vnIsProperty = find(cellfun(@ischar, varargin));
if (numel(vnIsProperty) > 0)
   for (nProp = vnIsProperty)
      if ~isequal(lower(varargin{nProp}), 'ignorenans')
         error('TimeSeries:Usage', ...
            '*** mean: Unrecognised property [%s].', varargin{nProp});
      end
   end
   
   vbPropValue = false(size(varargin));
   vbPropValue([vnIsProperty vnIsProperty+1]) = true;
   varargin = varargin(~vbPropValue);
end

% - Use TimeSeries/sum to compute sum
[tsMean, vnNumSeriesInSum] = sum(varargin{:});

% - Compute amplitude-wise mean
tsMean.tfSamples = tsMean.tfSamples ./ vnNumSeriesInSum;

% --- END of mean.m ---
