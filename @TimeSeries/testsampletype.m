function nSampleType = testsampletype(ts, cstrTestSampleTypes)

% testsampletype - FUNCTION Test whether a time series is one of a list of allowed sample types
%
% Usage: nSampleType = testsampletype(ts, cstrTestSampleTypes)
%
% 'nSampleType' will be 'false', if no match is found, or an index into
% 'cstrTestSampleTypes', if it matches one of the provided sample types.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th November, 2014

if (nargin < 2)
   help testsampletype;
   error('TimeSeries:Usage', ...
         '*** testsampletype: Incorrect usage.');
end

% - Is this a valid time series?
if (~isa(ts, 'TimeSeries'))
   nSampleType = false;
   return;
end

if ~iscell(cstrTestSampleTypes)
   cstrTestSampleTypes = {cstrTestSampleTypes};
end

[~, nSampleType] = ismember(ts.strSampleType, cstrTestSampleTypes);

% --- END of testsampletype.m ---
