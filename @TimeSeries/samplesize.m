function vnSampleSize = samplesize(ts)

% samplesize - FUNCTION Return the size and shape of the data associated with each time sample
%
% Usage: vnSampleSize = samplesize(ts)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 21st April, 2015

% - Check arguments

if (nargin < 1)
   help samplesize;
   error('TimeSeries:Usage', ...
         '*** samplesize: Incorrect usage.');
end

vnSampleSize = size(ts.tfSamples);
vnSampleSize(1) = 1;

% --- END of samplesize.m ---