function tsMerged = merge(varargin)

% merge - FUNCTION Merge (interleave) several time series
%
% Usage: tsMerged = merge(tsSeries1, tsSeries2, ...)
%
% 'tsSeriesX' must all be time series with the same sampling type, the same
% number of time series and the same sample ranges.
%
% 'tsMerged' will be a time series containing all the time samples
% from 'tsSeriesX' interleaved, such that the original time bases are
% retained.
%
% Long runs of NaN samples will be removed prior to merging, leaving only
% the NaNs at the beginning and end of the runs.
%
% See also 'combine'

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

% -- Check arguments

if (nargin < 1)
   help merge;
   error('TimeSeries:Usage', ...
         '*** merge: Incorrect usage.');
end


% -- Collect information

% - Collect time series together
varargin = cellfun(@shiftdim, varargin, 'UniformOutput', false);
vtsTimeSeries = vertcat(varargin{:});

% - Collect all samples
ctfSamples = shiftdim({vtsTimeSeries.tfSamples});

% - Collect all time traces
cvtTimeTraces = shiftdim({vtsTimeSeries.vtTimeTrace});

% - Collect all ranges
mfSampleRanges = vertcat(vtsTimeSeries.vfSampleRange);

% - Collect time series sample types
cstrSampleTypes = shiftdim({vtsTimeSeries.strSampleType});

% - Get numbers of time traces
[~, vnNumTimeSeries] = cellfun(@size, ctfSamples);


% -- Check for compatible time series

if (~all(vnNumTimeSeries == vnNumTimeSeries(1)))
   error('TimeSeries:Incompatible', ...
      '*** merge: Incompatible time series for merging: Number of time series differ.')
end

if (~all(cellfun(@(s)strcmpi(s, cstrSampleTypes{1}), cstrSampleTypes)))
   error('TimeSeries:Incompatible', ...
      '*** merge: Incompatible time series for merging: Sample types differ.')
end

if (~testsampletype(vtsTimeSeries(1), 'event')) && (~isequal(mfSampleRanges, repmat(mfSampleRanges(1, :), numel(vtsTimeSeries), 1)))
   warning('TimeSeries:Incompatible', ...
      '--- merge: Incompatible time series for merging: Sample ranges differ.')
end


% -- Merge time vectors

% - Discard long NaN sample runs for time series posessing values
if (~testsampletype(vtsTimeSeries(1), 'event'))
   [ctfSamples, cvtTimeTraces] = cellfun(@TSM_TrimNans, ctfSamples, cvtTimeTraces, 'UniformOutput', false);
end

% - Reshape all samples to shape of initial series
vnSeriesShape = size(ctfSamples{1});
vnSeriesShape = vnSeriesShape(2:end);
ctfSamples(2:end) = cellfun(@(c)reshape(c, [size(c, 1) vnSeriesShape]), ctfSamples(2:end), 'UniformOutput', false);

% - Concatenate all time series samples
tfAllSamples = cell2mat(ctfSamples);

% - Measure number of time points per time series
% vnNumTimePoints = cellfun(@(s)size(s, 1), ctfSamples);

% - Construct a time base to sort
vtAllTimeBase = cell2mat(cvtTimeTraces);

% - Determine sorting order and sort time base
[vtAllTimeBase, vnSampleIndex] = sort(vtAllTimeBase);


% -- Create the merged time series

tsMerged = vtsTimeSeries(1);
tsMerged.vtTimeTrace = vtAllTimeBase;

if (~strcmpi(cstrSampleTypes{1}, 'event'))
   tsMerged.tfSamples = reshape(tfAllSamples(vnSampleIndex, :), size(tfAllSamples));
end

% --- END of merge METHOD ---

% TSM_TrimNans - FUNCTION Trim long series of all-nan samples
function [tfSamples, vtTimeTrace] = TSM_TrimNans(tfSamples, vtTimeTrace)

% - Find time points where all samples are NaN
vbAllNan = all(isnan(tfSamples(:, :)), 2);

% - Identify middle portions of nan runs, by erosion
vbRemoveSamples = ~logical(conv(double(~[0; vbAllNan; 1]), [1 1 1]', 'same'));
vbRemoveSamples = vbRemoveSamples(2:end-1);

% - Remove time base
vtTimeTrace = vtTimeTrace(~vbRemoveSamples);

% - Remove samples and reshape to original shape
vnSampleSize = size(tfSamples);
tfSamples = tfSamples(~vbRemoveSamples, :);
tfSamples = reshape(tfSamples, [], vnSampleSize(2:end));

% --- END of merge.m ---
