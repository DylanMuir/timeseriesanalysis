function [tsSum, vnNumNonNaNSeries] = sum(varargin)

% sum - METHOD Compute the addition (amplitude-wise) of several time series
%
% Usage: tsSum = sum(ts1 <, ts2, ...>)
%                sum(..., 'IgnoreNaNs', bIgnoreNaNs)
%
% This function will perform the amplitude-wise addition of several time
% series. Both unitary and non-unitary time series can be summed. The
% returned time series will have a time base that is the union of the
% source time series' time bases.
%
% If the parameter 'IgnoreNaNs' is set to 'true', then NaN values will be
% ignored (similar behaviour to nansum). By default, NaN values will
% propagate into the sum (similar behaviour to sum).

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 20th April, 2015

% -- Check arguments

if (nargin < 1)
   help sumTS;
   error('TimeSeries:Usage', ...
         '*** sum: Incorrect usage.');
end

% - By default, don't ignore NaNs
bIgnoreNaNs = false;

% - Check for property / value pairs
vnIsProperty = find(cellfun(@ischar, varargin));
if (numel(vnIsProperty) > 0)
   for (nProp = vnIsProperty)
      switch lower(varargin{nProp})
         case {'ignorenans'}
            bIgnoreNaNs = true;
            
         otherwise
            error('TimeSeries:Arguments', ...
               '*** sum: Unrecognised property [%s].', varargin{nProp});
      end
   end
   
   vbPropValue = false(size(varargin));
   vbPropValue([vnIsProperty vnIsProperty+1]) = true;
   varargin = varargin(~vbPropValue);
end

% - Check for TimeSeries objects
if ~all(cellfun(@(a)isa(a, 'TimeSeries'), varargin))
   error('TimeSeries:Arguments', ...
         '*** sum: All inputs must be ''TimeSeries'' objects.');
end

% - Check for array of time series objects
if (numel(varargin{1}) > 1)
   % - Convert to cell array of cell arrays of time series
   varargin = CellFlatten(cellfun(@num2cell, varargin, 'UniformOutput', false));
end

% - Get time series
cvtsSources = varargin;

% - All time series must be of the same sample type
strTSClass = cvtsSources{1}.strSampleType;

if ~all(cellfun(@(c)c.testsampletype(strTSClass), cvtsSources))
   error('TimeSeries:Arguments', ...
         '*** sumTS: All time series must have the same sample type.')
end

% - Check number of time series in total
vnNumTimeSeries = cellfun(@(c)size(c.tfSamples, 2), cvtsSources);

if (sum(vnNumTimeSeries) == 1)
   % - Just return source
   tsSum = cvtsSources{1};
   vnNumNonNaNSeries = ones(numel(tsSum.vtTimeTrace), 1);
   return;
end


% -- Perform sum of time series

% - First perform sum on non-unitary time series
[cvtsSources, cvtsNonNaNSources] = cellfun(@(c)sTS_SumNonUnitary(c, bIgnoreNaNs), cvtsSources, 'UniformOutput', false);

% - Construct interleaved time base
cvtTimeBases = cellfun(@(c)(c.vtTimeTrace), cvtsSources, 'UniformOutput', false);
vtSharedTimeBase = unique(vertcat(cvtTimeBases{:}));

% - Interpolate all time series to the shared time base
cvtsSources = cellfun(@(c)c.resample(vtSharedTimeBase), cvtsSources, 'UniformOutput', false);
cvtsNonNaNSources = cellfun(@(c)c.resample(vtSharedTimeBase), cvtsNonNaNSources, 'UniformOutput', false);

% - Sum samples
ctfSamples = cellfun(@(c)(c.tfSamples), cvtsSources, 'UniformOutput', false);
ctfNonNaNSources = cellfun(@(c)(c.tfSamples), cvtsNonNaNSources, 'UniformOutput', false);

if (bIgnoreNaNs)
   tfSumSamples = nansum([ctfSamples{:}], 2);
else
   tfSumSamples = sum([ctfSamples{:}], 2);
end

vnNumNonNaNSeries = sum([ctfNonNaNSources{:}], 2);

% - Make return time series
tsSum = TimeSeries(vtSharedTimeBase, tfSumSamples, strTSClass);


% --- END of sumTS FUNCTION ---


function [tsSum, tsNonNaNSeries] = sTS_SumNonUnitary(tsSource, bIgnoreNaNs)

tsSum = tsSource;
tsNonNaNSeries = tsSum;

if (bIgnoreNaNs)
   tsSum.tfSamples = nansum(tsSum.tfSamples(:, :), 2);
   tsNonNaNSeries.tfSamples = sum(~isnan(tsSource.tfSamples(:, :)), 2);
else
   tsSum.tfSamples = sum(tsSum.tfSamples(:, :), 2);
   tsNonNaNSeries.tfSamples = repmat(size(tsSource.tfSamples(:, :), 2), size(tsNonNaNSeries.tfSamples, 1), 1);
end


% --- END of sum.m ---