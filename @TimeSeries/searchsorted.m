function [vtTimes, vnIndex, vfValues] = searchsorted(tsSeries, vtTarget, sPolicy)

% searchsorted - FUNCTION Find time points in time serie. Exact match is
% not needed
%
% Usage: [vtTimes <, vnIndex, vfValues>] = find(tsSeries, vtTarget)
%                              ... = find(tsSeries, vtTarget, 'first')
%
% 'tsSeries' is a time series. searchsorted will find event times 
% 'vtTarget' times to find times in the time series. Must be a column
%            vector
% 'sPolicy' can be 'before', 'after' or 'closest' it determines which time
%     is kept when the exact value of vtTimes is not in tsSeries (default
%     'before'
%
% If sPolicy is 'before', all the targets found before the first time of
% tsSeries will be NaN. If sPolicy is 'after', all the targets found after
% the last time of tsSeries will be NaN
%
% Outputs:
% vtTimes: times of tsSeries corresponding to vtTarget
% vnIndex: index corresponding to vtTimes
% vfValues: values of tsSeries at vtTimes
%
%
% Author: Antonin Blot <antonin.blot@gmail.com>
% adapt from Dylan Muir's find
% Created: 18th May, 2017

% -- Check arguments
DEF_sPolicy = 'before';

if (nargin < 2) || (nargin > 3)
   help searchsorted;
   error('TimeSeries:Usage', '*** searchsorted: Incorrect usage.');
end
if nargout >2 && strcmp(tsSeries.strSampleType, 'event')
    error('TimeSeries:Arguments', '*** searchsorted: Cannot return ''vfvalues'' for event TimeSeries');
end
if size(vtTarget,2) ~= 1
   error('TimeSeries:Arguments', '*** searchsorted: ''vtTarget'' must be a column vector.');
end
if ~exist('sPolicy', 'var') || isempty(sPolicy)
    sPolicy = DEF_sPolicy;
end

% Make sure tsSeries is properly formatted to avoid crashing matlab
vtSeries = tsSeries.vtTimeTrace;
if any(isnan(vtSeries))
    error('TimeSeries:Arguments', '*** searchsorted: ''tsSeries'' cannot have NaNs in time trace.');
end
% vtTarget needs to be sorted
[vtSortedTarget, vnArgSort] = sort(vtTarget);

if numel(vtSeries) == 0
    vtTimes = []; vnIndex = []; vfValues = [];
    return
end
% Find boundaries
vbBeforeFirst = vtSortedTarget < vtSeries(1); % before the first time
vbAfterLast =  vtSortedTarget > vtSeries(end); % before the first time
vbNaN = isnan(vtSortedTarget);
vbSortedValid = ~vbBeforeFirst & ~vbAfterLast & ~vbNaN;

% Perfom binary search sorted for valid targets
vnIndexBef = BinarySearchSortedList_mex(vtSeries, vtSortedTarget(vbSortedValid));
% BSSL returns the sample *preceding* the target. If there is an exact
% match, BSSL seems to return either the index of the match or the index
% before (numerical precision?)
vbExactMatch = abs(vtSeries(vnIndexBef) - vtSortedTarget(vbSortedValid)) < eps(max(vtSortedTarget(vbSortedValid)));
vnIndexAft = vnIndexBef;
vnIndexAft(~vbExactMatch) = vnIndexBef(~vbExactMatch) + 1;
vbExactMatch = abs(vtSeries(vnIndexAft) - vtSortedTarget(vbSortedValid)) < eps(max(vtSortedTarget(vbSortedValid)));
vnIndexBef(vbExactMatch) = vnIndexAft(vbExactMatch);

% initialise output
vnIndex = nan(size(vtSortedTarget));

% Find vnIndex depending on policy
switch lower(sPolicy)
    case {'prior', 'preceding', 'preceeding', 'before'}
        vnIndex(vbSortedValid) = vnIndexBef;
        % for target happening after the vector, before is defined
        vnIndex(vbAfterLast) = numel(vtSeries);
    case {'following', 'after'}
        % - Return results after target time
        vnIndex(vbSortedValid) = vnIndexAft;
        % for target happening before the vector, after is defined
        vnIndex(vbBeforeFirst) = 1;
    case {'closest', 'nearest', 'near'}
        vdDist = vtSeries(vnIndexAft) + vtSeries(vnIndexBef) - 2*vtSortedTarget(vbSortedValid);
        % if the distance if positive then aft - t is bigger than bef - t.
        % So I should keep bef
        vnClosest = nan(size(vdDist));
        vnClosest(vdDist>=0) = vnIndexBef(vdDist >= 0);
        vnClosest(vdDist<0) = vnIndexAft(vdDist < 0);
        vnIndex(vbSortedValid) = vnClosest;
        % for target happening before the vector, use after
        vnIndex(vbBeforeFirst) = 1;
        % for target happening after the vector, use before
        vnIndex(vbAfterLast) = numel(vtSeries);

    otherwise
        error('TimeSeries:Arguments', '*** searchsorted: ''sPolicy'' "%s" not understood.', sPolicy);
end

% Un-sort the index
[~, vnUnSort] = sort(vnArgSort);
vnIndex = vnIndex(vnUnSort);
vbValid = ~isnan(vnIndex);

% Create the other outputs
vtTimes = nan(size(vnIndex));
vtTimes(vbValid) = vtSeries(vnIndex(vbValid));
if nargout > 2
    vnSampleSize = tsSeries.samplesize;
    vnSampleSize(1) = numel(vtSortedTarget);
    vfValues = nan(vnSampleSize);
    vfValues(vbValid,:) = tsSeries.tfSamples(vnIndex(vbValid), :);
end
end


% --- END of find.m ---

