function bIsEmpty = isempty(tsTest)

% function isempty - Test if a time series is empty (i.e. no valid samples)
%
% Usage: bIsEmpty =  isempty(tsTest)
%        vbIsEmpty = isempty(vtsTest)
%
% isempty can be called on single time series, or on arrays of time series
% objects. In the latter case, 'vbIsEmpty' will be a boolean array of the
% same size as 'vtsTest'. 'bEmpty' is a boolean variable, and will be true
% if 'tsTest' contains no time samples.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 27th August, 2015

% -- Check arguments

if (nargin < 1)
   help isempty;
   error('TimeSeries:Usage', '*** isempty: Incorrect usage.');
end

if (~istimeseries(tsTest))
   error('TimeSeries:Arguments', '*** isempty: No time series was provided. How did you get here?');
end

if (numel(tsTest) == 0)
   bIsEmpty = true;
else
   bIsEmpty = arrayfun(@(ts)isempty(ts.vtTimeTrace), tsTest);
end

% --- END of isempty.m ---
