% image - METHOD Overloaded "image" method
%
% Usage: h = image(ts, <...>)
%
% See matlab image for usage.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 15th January, 2018

function h = image(ts, varargin)

% - Simply call "imagesc"
h = imagesc(ts, varargin{:});


% --- END of image.m ---
