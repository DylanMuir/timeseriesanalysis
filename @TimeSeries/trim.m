function tsTrimmed = trim(tsSource, vtTimeLimits)

% trim - FUNCTION Trim a time series without resampling
% 
% Usage: tsTrimmed = trim(tsSource, vtTimeLimits)
%        tsTrimmed = trim(tsSource, tsBoolean)
%
% 'tsSource' is a time series. 'vtTimeLimits' is a vector [tStartTime tEndTime]
% which defines a time range to trim 'tsSource' to (including end points).
%
% 'tsTrimmed' will be a new time series, containing data only from the time
% range defined by 'vtTimeLimits'.
%
% Alternatively, a unitary time series 'tsBoolean' can be provided as the
% second argument. In this case, only periods during which 'tsBoolean' is
% 'true' will be kept.
%
% See also the excise method, which removes the data between 'vtTimeLimits'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: September 2014

% -- Check arguments

if (nargin < 2)
   help trim;
   error('TimeSeries:Usage', ...
         '*** trim: Incorrect usage.');
end

% - Handle 'tsBoolean' calling convention
if (istimeseries(vtTimeLimits))
   tsTrimmed = excise(tsSource, not(vtTimeLimits), true);
   return;
end

% - Find first and last time point
vtTimeLimits = [nanmin(vtTimeLimits(:)); nanmax(vtTimeLimits(:))];

% - Find time points within the original time series that can be included
vbAcceptTimePoints = (tsSource.vtTimeTrace >= vtTimeLimits(1)) & (tsSource.vtTimeTrace <= vtTimeLimits(2));

% - Copy source time series
tsTrimmed = tsSource;
vnSampleSize = size(tsSource.tfSamples);

% - Trim for different sample types
switch lower(tsSource.strSampleType)
   case {'continuous', 'count', 'categorical', 'boolean'}
      % - Handle an empty time series
      if (isempty(tsSource.tfSamples))
         tsTrimmed.vtTimeBounds = vtTimeLimits;
         return;
      end
      
      % - Interpolate to find start and end points
      tsTrimmed.tfSamples = [tsTrimmed.fhInterpolation(tsSource, vtTimeLimits(1));
                             reshape(tsTrimmed.tfSamples(vbAcceptTimePoints, :), [], vnSampleSize(2:end));
                             tsTrimmed.fhInterpolation(tsTrimmed, vtTimeLimits(2))];
      
      % - Include start and end time points in time trace
      tsTrimmed.vtTimeTrace = [vtTimeLimits(1); tsTrimmed.vtTimeTrace(vbAcceptTimePoints); vtTimeLimits(end)];
      
   case 'event'
      % - Bound the time series
      tsTrimmed.vtTimeTrace = tsTrimmed.vtTimeTrace(vbAcceptTimePoints);
      tsTrimmed.vtTimeBounds = vtTimeLimits;
      
   otherwise
      % - Not supported
      error('TimeSeries:Unsupported', ...
            '*** trim: Trim is not supported for time series of class ''%s''.', tsSource.strSampleType);
end

% --- END of trim.m ---