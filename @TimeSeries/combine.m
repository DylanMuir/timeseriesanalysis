function [tsCombined] = combine(varargin)

% combine - METHOD Combine a set of time series objects, which share a common time base
%
% Usage: [tsCombined] = combine(ts1, ts2, ...)
%                       combine([ts1, ts2, ...], ...)
%                       combine({ts1, ts2, ...}, ...)
%
% 'ts1', 'ts2', ... are time series objects, each containing one or more
% time series, with identical sampling types. If any time bases are not
% identical to that of 'ts1', they will be resampled onto the 'ts1' time
% base.
%
% Arrays ['ts1', 'ts2', ...] or cell arrays {'ts1', 'ts2', ...} of time
% series can also be supplied.
%
% 'tsCombined' will be a time series object containing 'ts1', 'ts2', ... as
% separate time series, and with a common time base. Any particular sample
% shape to 'ts1', 'ts2', ... will be lost. The sample shape of
% 'tsCombined' will be [1 N] where 'N' is the total number of time series
% in the set 'ts1', 'ts2', ...
%
% See also: 'merge'

% Author: Dylan Muir <dylan.muir@unibas.ch
% Created: 23rd September, 2015

% - Check arguments
varargin = cellfun(@(c)num2cell(c), varargin, 'UniformOutput', false);
varargin = CellFlatten(varargin);
nNumArgIn = numel(varargin);
if (nNumArgIn < 2)
   help combine;
   error('TimeSeries:Usage', ...
         '*** combine: Incorrect usage.');
end

% - Check sample types
strSampleType = varargin{1}.strSampleType;

if (~all(cellfun(@(ts)ts.testsampletype(strSampleType), varargin)))
   error('TimeSeries:Arguments', ...
         '*** combine: All time series must share a common sample type.');
end

% - Use "merge" for event time series
if (testsampletype(varargin{1}, 'event'))
   error('TimeSeries:Arguments', ...
      '*** combine: Cannot be used for ''event'' time series. Try ''merge''.');
end

% - Time bases must be identical; otherwise resample
vtGlobalTimeTrace = varargin{1}.vtTimeTrace;

vbResample = cellfun(@(ts)(numel(ts.vtTimeTrace) ~= numel(vtGlobalTimeTrace)) || any(ts.vtTimeTrace ~= vtGlobalTimeTrace), varargin);
varargin(vbResample) = cellfun(@(ts)ts.resample(vtGlobalTimeTrace), varargin(vbResample), 'UniformOutput', false);

% - Combine all time series samples
tsCombined = varargin{1};
ctfSamples = cellfun(@(ts)ts.tfSamples(:, :), varargin, 'UniformOutput', false);
tsCombined.tfSamples = cat(2, ctfSamples{:});

% --- END of combine.m ---
