% imshow - METHOD Overloaded "imshow" method
%
% Usage: h = imshow(ts, <...>)
%
% See matlab imshow for usage.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 15th January, 2018

function h = imshow(ts, varargin)

% - Simply call "imagesc"
h = imagesc(ts, varargin{:});


% --- END of imshow.m ---
