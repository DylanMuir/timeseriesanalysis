function [cvoResult, cvoCategory] = accumarray(tsSubscript, tsValue, fhOperation, bTSArgument)

% accumarray - METHOD Index into one time series using a categorical time series
%
% Usage: [cvoResult, cvoCategory] = accumarray(tsSubscript, tsValue <, fhOperation, bTSArgument>)
%
% accumarray finds samples in 'tsValue' at time points that match
% unique categorical values in 'tsSubscript', then applies a function to
% the resulting time series.
%
% 'tsSubscript' must be of sample type 'categoricial, 'boolean' or 'count'.
% 'nan's in 'tsSubscript' will be ignored.
%
% 'fhOperation' is a function handle that operates on a time series and
% returns an arbitrary output. By default, 'nanmean_t' is used to compute
% the time average of the values in 'tsValue' over time. 'fhOperation' must
% have the signature oResult = @(ts).
%
% Alternatively, the optional argument 'bTSArgument' can be set to 'false',
% to signal that 'fhOperation' accepts a raw list of samples. In that case
% 'fhOperation' must have the signature oResult = @(vfSamples). 'vfSamples'
% may contain 'nan's. By default, 'bTSArgument' is 'true', indicating that
% 'fhOperation' expects a time series.
%
% 'cvoResult' will be a cell array, with an entry for each unique category
% in 'tsSubscript', with the category corresponding to each element
% contained in 'cvoCategory'. Each element of 'cvoResult' will contain the
% result of calling 'fhOperation' on the samples from 'tsValue' at time
% points when 'tsSubscript' matches the corresponding category.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 3rd August 2016

% -- Defaults

DEF_cstrPermittedSampleTypes = {'categorical', 'boolean', 'count'};
DEF_fhOperation = @nanmean_t;
DEF_bTSArgument = true;


% -- Check arguments

if (nargin < 2)
   help TimeSeries/index;
   error('TimeSeries:Usage', ...
      '*** TimeSeries/index: Incorrect usage.');
end

if (~exist('fhOperation', 'var') || isempty(fhOperation))
   fhOperation = DEF_fhOperation;
end

if (~exist('bTSArgument', 'var') || isempty(bTSArgument))
   bTSArgument = DEF_bTSArgument;
end

% - Test for permitted time series sample type
if (~any(tsSubscript.testsampletype(DEF_cstrPermittedSampleTypes)))
   error('TimeSeries:Arguments', ...
      ['*** TimeSeries/index: ''tsSubscript'' must be one of {' sprintf('''%s'', ', DEF_cstrPermittedSampleTypes{:}) '}']);
end

% - Test for unitary subscript time series
if (prod(tsSubscript.samplesize) > 1)
   error('TimeSeries:Arguments', ...
      '*** TimeSeries/index: ''tsSubscript'' must be a unitary time series.');
end

% -- Get a list of categories
cvoCategory = num2cell(unique(tsSubscript.tfSamples(~isnan(tsSubscript.tfSamples(:)))));

% - Resample subscript time series to source time series
vtTimeTrace = tsValue.vtTimeTrace;
tfOrigSamples = tsValue.tfSamples;
vnSampleSize = size(tfOrigSamples);
tsSubscript = tsSubscript.resample(vtTimeTrace);
tsTemporary = tsValue;

% - Loop over categories
for (nCatInd = numel(cvoCategory):-1:1)
   % - Get time samples that match category
   vbMatchingSamples = tsSubscript.tfSamples == cvoCategory{nCatInd};
   
   % - Perform operation, save results
   if (bTSArgument)
      % - Perform operation on TimeSeries
      tfSamples = tfOrigSamples;
      tfSamples(~vbMatchingSamples, :) = nan;
      tsTemporary.tfSamples = tfSamples;
      cvoResult{nCatInd} = fhOperation(tsTemporary);
      
   else
      % - Perform operation of raw samples
      tfSamples = reshape(tfOrigSamples(vbMatchingSamples, :), [nnz(vbMatchingSamples) vnSampleSize(2:end)]);
      cvoResult{nCatInd} = fhOperation(tfSamples);
   end
end

% --- END of index.m ---
