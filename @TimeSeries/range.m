function [tStartTime, tEndTime] = range(sTS)

% range - FUNCTION Compute (or extract) the time range of a time series
%
% Usage: [tStartTime, tEndTime] = range(sTS)
% Usage: [vtTimeRange] = range(sTS)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th November, 2014

if (nargin < 1)
   help range;
   error('TimeSeries:Usage', ...
         '*** range: Incorrect usage.');
end

warning('TimeSeries:Deprecated', ...
        '--- range: Warning: This method is deprecated. Use the ''vtTimeBounds'' property instead.');

tStartTime = sTS.vtTimeBounds;

if (nargout == 2)
   tEndTime = tStartTime(2);
   tStartTime = tStartTime(1);
end

% --- END of range.m ---
