function tsMean = meanTS(varargin)

% meanTS - FUNCTION Compute the mean (amplitude-wise) of several time series
%
% Usage: tsMean = meanTS(ts1 <, ts2, ...>)
%                 meanTS(..., 'IgnoreNaNs', bIgnoreNaNs)
%
% meanTS will compute the mean of several time series. Unitary and
% non-unitary time series can be supplied. The time base of the mean will be
% the interleaved time bases of the source time series. Time series will be
% interpolated, when necessary, according to the interpolation function set
% for each time series.
%
% If the parameter 'IgnoreNaNs' is set to 'true', then NaN values will be
% ignored (similar behaviour to nanmean). By default, NaN values will
% propagate into the sum (similar behaviour to mean).

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 20th April, 2015

TS_deprecated('mean');

tsMean = mean(varargin{:});

% --- END of meanTS.m ---
