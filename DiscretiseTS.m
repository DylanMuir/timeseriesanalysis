function tsDiscretised = DiscretiseTS(tsSource, vfThresholds, tTemporalSmoothing, tMinSegmentDuration, bTakeEarlierTransitions)

% DiscretiseTS - FUNCTION Discretise a time series in sample space, according to fixed value thresholds
%
% Usage: tsDiscretised = DiscretiseTS(tsSource, vfThresholds <, tTemporalSmoothing, tMinSegmentDuration, bTakeEarlierTransitions>)
%
% 'tsSource' must be a unitary time series

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 23rd October, 2014

% - Default parameters

DEF_tTemporalSmoothing = 0;
DEF_tMinSegmentDuration = 0;
DEF_bTakeEarlierTransitions = true;


% -- Check arguments

if (nargin < 2)
   help DiscretiseTS;
   error('TimeSeries:Usage', ...
         '*** DiscretiseTS: Incorrect usage');
end

if (~exist('tTemporalSmoothing', 'var') || isempty(tTemporalSmoothing))
   tTemporalSmoothing = DEF_tTemporalSmoothing;
end

if (~exist('tMinSegmentDuration', 'var') || isempty(tMinSegmentDuration))
   tMinSegmentDuration = DEF_tMinSegmentDuration;
end

if (~exist('bTakeEarlierTransitions', 'var') || isempty(bTakeEarlierTransitions))
   bTakeEarlierTransitions = DEF_bTakeEarlierTransitions;
end

% - Sort out threshold bins
vfThresholds = reshape(vfThresholds, 1, []);


% -- Smooth source TS

if (tTemporalSmoothing > 0)
   tsSourceMA = ConvolveTS(tsSource, @(vt)(double(abs(vt)<tTemporalSmoothing/2)./tTemporalSmoothing));
else
   tsSourceMA = tsSource;
end

% - Find all threshold crossings of smoothed TS
ctsThreshCrossingsMA = arrayfun(@(fT)EventThresholdTS(tsSourceMA, fT, 'both'), vfThresholds, 'UniformOutput', false);
tsThreshCrossingsMA = merge(ctsThreshCrossingsMA{:});

if (tTemporalSmoothing > 0)
   ctsThreshCrossingsSource = arrayfun(@(fT)EventThresholdTS(tsSource, fT, 'both'), vfThresholds, 'UniformOutput', false);
   tsThreshCrossingsSource = merge(ctsThreshCrossingsSource{:});
end

% - Find value of time series pre and post threshold crossing
tsPreCrossing = tsSourceMA.resample(tsThreshCrossingsMA.vtTimeTrace - 10*eps(tsThreshCrossingsMA.vtTimeTrace));
[~, vnPreCrossingBin] = histc(tsPreCrossing.tfSamples, [-inf vfThresholds inf]);
tsFinalPostCrossing = tsSourceMA.resample(tsThreshCrossingsMA.vtTimeTrace(end) + 10*eps(tsThreshCrossingsMA.vtTimeTrace(end)));
[~, nFinalBin] = histc(tsFinalPostCrossing.tfSamples, [-inf vfThresholds inf]);

% -- Analyse segments, discard short segments

vnSegmentValue = [vnPreCrossingBin; nFinalBin];
vtSegmentEndpoints = [tsSourceMA.vtTimeTrace(1); tsThreshCrossingsMA.vtTimeTrace; tsSourceMA.vtTimeTrace(end)];

% vnOrigValues = vnSegmentValue;
% vtOrigEndpoints = vtSegmentEndpoints;

% - Merge segments that are preceeded and followed by the same bin value
while (true)
   % - Discard short segments and merge with surrounding
   vtSegmentDurations = diff(vtSegmentEndpoints);
   vbDiscardSegment = vtSegmentDurations < tMinSegmentDuration;
   vbDiscardSegment([1 end]) = false;
   
   % - Are there any segments to discard?
   if (~any(vbDiscardSegment))
      break;
   end
   
   % - Locate segments to discard
   vnDiscardSegmentIndices = find(vbDiscardSegment);
   
   % - Discard segments entirely if preceding and following segments are of
   % same value
   vbMergeBoth = vnSegmentValue(vnDiscardSegmentIndices-1) == vnSegmentValue(vnDiscardSegmentIndices+1);
   vnMergeBothIndices = vnDiscardSegmentIndices(vbMergeBoth);
   vnMergeBothIndices([false; diff(vnMergeBothIndices) == 1]) = nan;
   vnMergeBothIndices = vnMergeBothIndices(~isnan(vnMergeBothIndices));

   % - Are there any segments to discard?
   if (isempty(vnMergeBothIndices))
      break;
   end
   
%    clf;
%    plot_ts(tsSourceMA, [], 'LineWidth', 2);
%    hold all;
%    for (nThreshold = 1:numel(vfThresholds))
%       plot(vtOrigEndpoints([1 end]), [1 1] * vfThresholds(nThreshold), 'k:');
%    end
%    
%    for (nSegment = 1:numel(vnSegmentValue))
%       plot(vtSegmentEndpoints(nSegment + [0 1]), vnSegmentValue(nSegment) * [1 1], 'r-');
%    end
%       
%    plot(vtSegmentEndpoints, 0, 'k.', 'MarkerSize', 22);
%    plot(vtSegmentEndpoints([vnMergeBothIndices; vnMergeBothIndices+1]), 0, 'r.', 'MarkerSize', 22);
%    drawnow;
   
   vnSegmentValue([vnMergeBothIndices; vnMergeBothIndices+1]) = nan;
   vtSegmentEndpoints([vnMergeBothIndices; vnMergeBothIndices+1]) = nan;
   
   % - Shrink segment list
   vnSegmentValue = vnSegmentValue(~isnan(vnSegmentValue));
   vtSegmentEndpoints = vtSegmentEndpoints(~isnan(vtSegmentEndpoints));
end

% - Discard short transition segments
vtSegmentDurations = diff(vtSegmentEndpoints);
vbDiscardSegment = vtSegmentDurations < tMinSegmentDuration;
vnDiscardSegmentIndices = find(vbDiscardSegment);

bDiscardFirst = vbDiscardSegment(1);
bDiscardLast = vbDiscardSegment(end);
vbDiscardSegment([1 end]) = false;

% clf;
% plot_ts(tsSourceMA, [], 'LineWidth', 2);
% hold all;
% for (nThreshold = 1:numel(vfThresholds))
%    plot(vtOrigEndpoints([1 end]), [1 1] * vfThresholds(nThreshold), 'k:');
% end
% 
% for (nSegment = 1:numel(vnSegmentValue))
%    plot(vtSegmentEndpoints(nSegment + [0 1]), vnSegmentValue(nSegment) * [1 1], 'r-');
% end
% 
% plot(vtSegmentEndpoints, 0, 'k.', 'MarkerSize', 22);
% plot(vtSegmentEndpoints([vnDiscardSegmentIndices; vnDiscardSegmentIndices+1]), 0, 'r.', 'MarkerSize', 22);
% drawnow;


% - Are there any segments to discard?
if (any(vbDiscardSegment))
   % - Locate segments to discard
   vnDiscardSegmentIndices = find(vbDiscardSegment);
   vnSegmentValue(vbDiscardSegment) = nan;
   
   if (bTakeEarlierTransitions)
      vtSegmentEndpoints(vnDiscardSegmentIndices+1) = nan;
   else
      vtSegmentEndpoints(vnDiscardSegmentIndices) = nan;
   end
   
   % - Discard segments
   vnSegmentValue = vnSegmentValue(~isnan(vnSegmentValue));
   vtSegmentEndpoints = vtSegmentEndpoints(~isnan(vtSegmentEndpoints));
end

% - Fix up initial and final transitions
if (bDiscardFirst)
   vtSegmentEndpoints = vtSegmentEndpoints(2:end);
   vnSegmentValue = vnSegmentValue(2:end);
end

if (bDiscardLast)
   vtSegmentEndpoints = vtSegmentEndpoints(1:end-1);
   vnSegmentValue = vnSegmentValue(1:end-1);
end

% - Find transition times in non-smoothed time series
if (tTemporalSmoothing > 0)
   vtAllSourceCrossings = tsThreshCrossingsSource.vtTimeTrace;

   % - It pains me to use a for loop...
   for (nTransition = 1:numel(vtSegmentEndpoints))
      % - Are the first or last endpoints simply the end of the time series?
      if ((nTransition == 1) && ~(vtSegmentEndpoints(1) > tsSourceMA.vtTimeTrace(1) + eps(tsSourceMA.vtTimeTrace(1)))) || ...
         ((nTransition == numel(vtSegmentEndpoints)) && ~(vtSegmentEndpoints(end) < tsSourceMA.vtTimeTrace(end) + eps(tsSourceMA.vtTimeTrace(end))))
         % - Yes, so just keep the current endpoint in that case
         continue;
      
      else
         % - For any other endpoint, find the closest matching transition
         % in the source time series
         [~, nClosestCrossing] = min(abs(vtAllSourceCrossings - vtSegmentEndpoints(nTransition)));

         % - Assign this crossing to this transition
         vtSegmentEndpoints(nTransition) = vtAllSourceCrossings(nClosestCrossing);
      end
   end
end

% - Construct discretised time series
tfSamples = reshape([vnSegmentValue vnSegmentValue]', [], 1);
vtTimeTrace = [vtSegmentEndpoints(2:end-1) - eps(vtSegmentEndpoints(2:end-1)) vtSegmentEndpoints(2:end-1) + eps(vtSegmentEndpoints(2:end-1))];
vtTimeTrace = [vtSegmentEndpoints(1); reshape(vtTimeTrace', [], 1); vtSegmentEndpoints(end)];

tsDiscretised = TimeSeries(vtTimeTrace, tfSamples, 'count');

% --- END of DiscretiseTS.m ---
