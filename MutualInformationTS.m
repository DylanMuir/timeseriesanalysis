function [vfMI, mfMICI, vfPEst, fPResolution, vfMISurrogate] = MutualInformationTS(tsA, tsB, nNumAmplitudeBins, vtLags, nMarkovOrder, nSurrogateLength, nNumSurrogates)

% MutualInformationTS - FUNCTION Compute mutual information between two time series, with an optional lag
% Usage: [vfMI, mfMICI, vfPEst, fPResolution, vfMISurrogate] = MutualInformationTS(tsA, tsB <, nNumAmplitudeBins, vtLags, nMarkovOrder, nSurrogateLength, nNumSurrogates>)
%
% The mutual information between time series 'tsA' and 'tsB' will be
% computed. These must both be unitary time series.
%
% The optional argument 'nNumAmplitudeBins' determines the number of
% histogram bins for each time series to use in computing mutual
% information. By default, 20 bins are used over each time series.
%
% The optional argument 'vtLags' defines a vector of time shifts, in
% seconds, to apply to to time series A before computing mutual
% information. Positive values shift time series A forwards in time, such
% that bin t_A=0 for 'tsA' is compared with bin t_B=tLag. Therefore, high
% MI at a value for tLag>0 suggests that information in 'tsB' follows
% information in 'tsA' by 'tLag'.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 14th November, 2014

% -- Defaults

DEF_nNumAmplitudeBins = 20;
DEF_vtLags = 0;
DEF_nMarkovOrder = 5;
DEF_nSurrogateLength = 10000;
DEF_nNumSurrogates = 200;
DEF_nMaxTransMatrixSize = 10000;

% -- Check arguments

if (nargin < 2)
   help MutualInformationTS;
   error('TimeSeries:Usage', ...
      '*** MutualInformation: Incorrect usage.');
end

if (~exist('nNumAmplitudeBins', 'var') || isempty(nNumAmplitudeBins))
   nNumAmplitudeBins = DEF_nNumAmplitudeBins;
end

if (~exist('vtLags', 'var') || isempty(vtLags))
   vtLags = DEF_vtLags;
end

if (size(tsA.tfSamples, 2) > 1) || (size(tsB.tfSamples, 2) > 1)
   error('TimeSeries:Arguments', ...
      ' *** MutualInformationTS: ''tsA'' and ''tsB'' must both be unitary time series.');
end

if (~exist('nMarkovOrder', 'var') || isempty(nMarkovOrder))
   nMarkovOrder = DEF_nMarkovOrder;
end

if (~exist('nSurrogateLength', 'var'))
   nSurrogateLength = DEF_nSurrogateLength;
end

% - Check surrogate length
nMinNumSamples = min(numel(tsA.vtTimeTrace), numel(tsB.vtTimeTrace));
if (nSurrogateLength > nMinNumSamples)
   nSurrogateLength = nMinNumSamples;
end

if (~exist('nNumSurrogates', 'var'))
   nNumSurrogates = DEF_nNumSurrogates;
end

bComputeCI = (nargout >= 2);
bComputeP = (nargout >= 3);


% -- Compute mutual information over lags

if (~isequal(vtLags, 0))
   disp('--- MutualInformationTS: Computing MI over lags...');
   parfor (nLagIndex = 1:numel(vtLags))
      % - Shift time series A
      tsS = tsA.timeshift(vtLags(nLagIndex)); %#ok<PFBNS>
      
      % -Compute mutual information
      if (bComputeCI)
         [vfMI(nLagIndex), mfMICI(nLagIndex, :)] = MutualInformationTS(tsS, tsB, nNumAmplitudeBins);
      else
         vfMI(nLagIndex) = MutualInformationTS(tsS, tsB, nNumAmplitudeBins);
      end
   end
   
else
   % -- Compute mutual information for two unlagged time series

   % - Discretise time series
   [vnBinsA, vnBinsB] = MITS_Discretise(tsA, tsB, nNumAmplitudeBins, true);
   
   if (bComputeCI)
      % - Resample traces to estimate MI confidence interval, over segments
      % equivalent to that which will be used to estimate significance
      nSurrogateLength = min(nSurrogateLength, numel(vnBinsA));
      
      if (nSurrogateLength < numel(vnBinsA))
         nNumBootstraps = nNumSurrogates;
      else
         nNumBootstraps = 1;
      end
      
      parfor (nSample = 1:nNumBootstraps)
         nStartTrace = max(round(rand * (numel(vnBinsA) - nSurrogateLength-2) + 1), 1);
         
         mfBiHist = accumarray([vnBinsA(nStartTrace:(nStartTrace+nSurrogateLength-1)) vnBinsB(nStartTrace:(nStartTrace+nSurrogateLength-1))], 1);
         fHistNorm = nansum(mfBiHist(:));
         mfBiHist = mfBiHist./ fHistNorm;
         
         % - Compute univariate histograms
         vfAHist = nansum(mfBiHist, 1);
         vfBHist = nansum(mfBiHist, 2);
         mf2DUniHist = bsxfun(@times, vfAHist, vfBHist);
         
         % - Compute mutual information
         mfLogHist = log2(mfBiHist ./ mf2DUniHist);
         mfLogHist(isinf(mfLogHist)) = 0;
         vfMIBoot(nSample) = nansum(nansum(mfBiHist .* mfLogHist));
      end
      
      vfMI = median(vfMIBoot);
      mfMICI = prctile(vfMIBoot, [2.5; 97.5]);
      
   else
      % - Compute MI over entire traces
      mfBiHist = accumarray([vnBinsA vnBinsB], 1);
      fHistNorm = nansum(mfBiHist(:));
      mfBiHist = mfBiHist./ fHistNorm;
      
      % - Compute univariate histograms
      vfAHist = nansum(mfBiHist, 1);
      vfBHist = nansum(mfBiHist, 2);
      mf2DUniHist = bsxfun(@times, vfAHist, vfBHist);
      
      % - Compute mutual information
      mfLogHist = log2(mfBiHist ./ mf2DUniHist);
      mfLogHist(isinf(mfLogHist)) = 0;
      vfMI = nansum(nansum(mfBiHist .* mfLogHist));
   end
end

% - Compute significance threshold for mutual information, if requested
if (bComputeP)
   % - Recompute and resample only if necessary
   if (~exist('vnBinsA', 'var'))
      [vnBinsA, vnBinsB] = MITS_Discretise(tsA, tsB, nNumAmplitudeBins, false);
   end
   
   % - Compute transition matricies
   [mnTransA, tnWordsA] = trans_count(vnBinsA, nMarkovOrder);
   [mnTransB, tnWordsB] = trans_count(vnBinsB, nMarkovOrder);
   
   % - Test to see if transition matrices are reasonable
   if (any([size(mnTransA, 1) size(mnTransB, 1)] > DEF_nMaxTransMatrixSize))
%       ((size(mnTransA, 1) > numel(vnBinsA)/5) || (size(mnTransB, 1) > numel(vnBinsB)/5)))
      % - Bail out
      warning('TimeSeries:ResourceUsage', ...
         '--- MutualInformationTS: Warning: Refusing to estimate P values, since one time series has poor structure.');
      vfPEst = nan(size(vtLags));
      fPResolution = nan;
      vfMISurrogate = nan;
      
   else
      % - Continue estimating P values
      mnTransA = full(mnTransA); % Wastes space, but makes surrogates much faster
      mnTransB = full(mnTransB);
      
      % - Generate Markov-chain surrogates and measure MI
      nSurrogateLength = min(nSurrogateLength, numel(vnBinsA));
      fprintf('--- MutualInformationTS: Calculating MI over surrogates [%d %d]...\n', size(mnTransA, 1), size(mnTransB, 1));
      
      % - Pre-compute surrogate parameters
      [~, sSurrParamsA{1:3}] = simple_surrogate(mnTransA, tnWordsA, 0, 0);
      [~, sSurrParamsB{1:3}] = simple_surrogate(mnTransB, tnWordsB, 0, 0);
      
      % - Measure mutual information between surrogates
      parfor (nSurrogate = 1:nNumSurrogates)
         % - Compute two surrogates
         vnSurrogateA = simple_surrogate([], tnWordsA, nSurrogateLength, 1, sSurrParamsA{:}); %#ok<PFBNS>
         vnSurrogateB = simple_surrogate([], tnWordsB, nSurrogateLength, 1, sSurrParamsB{:}); %#ok<PFBNS>
         
         % - Measure MI
         mfBiHist = accumarray([vnSurrogateA' vnSurrogateB'], 1/nSurrogateLength);
         vfAHist = nansum(mfBiHist, 1);
         vfBHist = nansum(mfBiHist, 2);
         mf2DUniHist = bsxfun(@times, vfAHist, vfBHist);
         mfLogHist = log2(mfBiHist ./ mf2DUniHist);
         mfLogHist(isinf(mfLogHist)) = 0;
         vfMISurrogate(nSurrogate) = nansum(nansum(mfBiHist .* mfLogHist));
      end
      
      % - Estimate P values
      vfMISurrogate = sort(vfMISurrogate);
      
      for (nLagIndex = numel(vtLags):-1:1)
         nFirstMatch = find(vfMISurrogate >= vfMI(nLagIndex), 1, 'first') - 1;
         if (isempty(nFirstMatch))
            nFirstMatch = nNumSurrogates;
         end
         vfPEst(nLagIndex) = 1 - nFirstMatch ./ nNumSurrogates;
      end
      
      fPResolution = 1 ./ nNumSurrogates;
      vfPEst(vfPEst < fPResolution) = fPResolution;
   end
end

% -- Make a plot, if necessary

if ((numel(vtLags)) > 1 && (nargout == 0))
   figure;
   [vtLags, vnLagSort] = sort(vtLags);
   plot(vtLags, vfMI(vnLagSort), '-', 'LineWidth', 2);
   hold on;
   set(gca, 'FontSize', 24, 'LineWidth', 2, 'TickDir', 'out');
   axis tight manual;
   vfYLim = ylim;
   ylim([0 vfYLim(2)]);
   plot([0 0], [0 vfYLim(2)], 'k:', 'LineWidth', 1.5);
   xlabel('tsB -lead / +lag (s)');
   ylabel('Mutual Inf. (bits)');
   set(gca, 'FontSize', 18);
end

end


function [vnBinsA, vnBinsB] = MITS_Discretise(tsA, tsB, nNumAmplitudeBins, bResample)

% - Resample larger sample time series onto smaller time series
if (bResample)
   if (numel(tsA.vtTimeTrace) <= numel(tsB.vtTimeTrace))
      tsB = tsB.resample(tsA.vtTimeTrace);
   else
      tsA = tsA.resample(tsB.vtTimeTrace);
   end
end

% - Trim time series to mutually-valid time ranges
vtTimeBoundA = tsA.vtTimeBounds;
vtTimeBoundB = tsB.vtTimeBounds;
vtGlobalTimeBound = [max(vtTimeBoundA(1), vtTimeBoundB(1)) min(vtTimeBoundA(2), vtTimeBoundB(2))];

tsA = tsA.trim(vtGlobalTimeBound);
tsB = tsB.trim(vtGlobalTimeBound);

% - Compute optimal histogram bins, that equally partition the data in
% each time series
vfBinsA = quantile(tsA.tfSamples, linspace(0, 1, nNumAmplitudeBins+1));
vfBinsB = quantile(tsB.tfSamples, linspace(0, 1, nNumAmplitudeBins+1));

% - Discretise time series according to same bins
[~, vnBinsA] = histc(tsA.tfSamples, vfBinsA);
vnBinsA(vnBinsA == nNumAmplitudeBins+1) = nNumAmplitudeBins;
vnBinsA(vnBinsA == 0) = nan;
[~, vnBinsB] = histc(tsB.tfSamples, vfBinsB);
vnBinsB(vnBinsB == nNumAmplitudeBins+1) = nNumAmplitudeBins;
vnBinsB(vnBinsB == 0) = nan;

% - Remove all NaNs
if (bResample)
   vbIsNan = isnan(vnBinsA) | isnan(vnBinsB);
   vnBinsA = vnBinsA(~vbIsNan);
   vnBinsB = vnBinsB(~vbIsNan);
else
   vnBinsA = vnBinsA(~isnan(vnBinsA));
   vnBinsB = vnBinsB(~isnan(vnBinsB));
end

end

% --- END of MutualInformationTS.m ---
