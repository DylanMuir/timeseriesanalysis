# TimeSeriesAnalysis #

This repository contains a Matlab toolbox for analysis of time-series data, such as imaging stacks or behavioural recordings.

## How do I get set up? ##

* Clone the repository to your machine where you want to work
* Decide whether you want to use the object-oriented toolbox (recommended; "development" branch) or the Matlab struct version (not recommended; "master" branch). Make sure you switch to the correct branch.
* Add the toolbox directory to the Matlab path

### Which version to use ###

* Use the object-oriented version of the toolbox unless you really can't ("development" branch). At some point the struct version ("master" branch) will no longer be updated. At present I am trying to keep everything in synch between the two versions.

## Getting started ##

### Object-oriented version ###


```
#!matlab

vtTimeTrace = 0:(1/100):10;
tfSignal = repmat(linspace(0, 1, numel(vtTimeTrace)), [1 10 10]);
ts = TimeSeries(vtTimeTrace, tfSignal); % Make a 2D time series
ts.plot; % or plot(ts); plot the time series
plot(ts + ts); % Add two time series
ts.var_t() % or var_t(ts); Compute the variance of 'ts' over time
plot(mean(ts)); % Plot the mean of all series within 'ts'
plot(ts.choose(5)); % Plot only series '5' within 'ts' (linear indexing)

tsEvent = TimeSeries(vtTimeTrace, [], 'event'); % Make an event time series
tfTriggeredResponse = ContingentBinTS(tsEvent, ts, [], -1:.5:1); % Compute the triggered response of 'ts', at trigger events from 'tsEvent'.
```

### Struct version ####


```
#!matlab

vtTimeTrace = 0:(1/100):10;
tfSignal = repmat(linspace(0, 1, numel(vtTimeTrace)), [1 10 10]);
ts = TimeSeriesStructure(vtTimeTrace, tfSignal);
plot_ts(ts); % Plot the time series
plot_ts(sumTS(ts, ts)); % Add two time series
var_tTS(ts) % Compute the variance of 'ts' over time
plot_ts(meanTS(ts)); % Plot the mean of all series within 'ts'
plot_ts(ts, 5); % Plot only series '5' within 'ts' (linear indexing)

tsEvent = TimeSeriesStructure(vtTimeTrace, [], 'event'); % Make an event time series
tfTriggeredResponse = ContingentBinTS(tsEvent, ts, [], -1:.5:1); % Compute the triggered response of 'ts', at trigger events from 'tsEvent'.
```

## Getting help ##

1. Look through the list of functions / methods to see if what you need already exists
1. If there's a bug, try to debug the problem yourself as much as possible (using breakpoints, ```dbstop error```, stepping through the code, etc)
1. Talk to Dylan or other people, maybe make an [issue report](https://bitbucket.org/lasermouse/timeseriesanalysis/issues?status=new&status=open)