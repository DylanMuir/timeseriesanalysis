function tsImpulse = EventToImpulseTS(tsEvent, fImpulseMagnitude)

% EventToImpulseTS - FUNCTION Convert an event time series to a continuous impulse time series
%
% Usage: tsImpulse = EventToImpulseTS(tsEvent <, fImpulseMagnitude>)

% Author: Dylan Muir <dylanm.muir@unibas.ch>
% Created: 14th November, 2014

DEF_fImpulseMagnitude = 1;

if (nargin < 1)
   help EventToImpulseTS;
   error('TimeSeries:Usage', ...
         '*** EventToImpulseTS: Incorrect usage.');
end

if (~TestSampleTypeTS(tsEvent, 'event'))
   error('TimeSeries:Arguments', ...
         '*** EventToImpulseTS: ''tsEvent'' must be an ''event'' time series.');
end

if (~exist('fImpulseMagnitude', 'var') || isempty(fImpulseMagnitude))
   fImpulseMagnitude = DEF_fImpulseMagnitude;
end

% - Pad out event times
vtOrigTimeTrace = tsEvent.vtTimeTrace;
vtTimeTrace = reshape(bsxfun(@plus, vtOrigTimeTrace, [-eps(vtOrigTimeTrace) zeros(size(vtOrigTimeTrace)) eps(vtOrigTimeTrace)])', [], 1);

% - Construct impulse samples
tfSamples = fImpulseMagnitude .* repmat([0 1 0]', numel(vtOrigTimeTrace), 1);

% - Construct time series
tsImpulse = TimeSeries(vtTimeTrace, tfSamples, 'continuous', [0 fImpulseMagnitude], [], GetRangeTS(tsEvent));

% --- END of EventToImpulseTS.m ---
