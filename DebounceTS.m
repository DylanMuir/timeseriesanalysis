function [tsDBEvents] = DebounceTS(tsEvents, tHysteresisTime, bTakeFirstEvent)

% DebounceTS - FUNCTION Remove rapid repeated events from an event time series
%
% Usage: [tsDBEvents] = DebounceTS(tsEvents, tHysteresisTime <, bTakeFirstEvent>)
%
% This function removes spurious extra events in an event time series
% 'tsEvents', that fall closer together than 'tHysteresisTime'.
%
% The optional argument 'bTakeFirstEvent' determines whether the first
% event ('true'; default) in a sequence is returned, or the last ('false').

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

% -- Defaults

DEF_bTakeFirstEvent = true;


% -- Check arguments

if (nargin < 2)
   help DebounceTS;
   error('TimeSeries:Usage', '*** DebounceTS: Incorrect usage.');
end

if (~testsampletype(tsEvents, 'event'))
   error('TimeSeries:Arguments', '*** DebounceTS: ''tsEvents'' must be an ''event'' time series.');
end

if (~isscalar(tHysteresisTime) || ~isnumeric(tHysteresisTime))
   error('TimeSeries:Arguments', '*** DebounceTS: ''tHysteresisTime'' must be a scalar duration.');
end

if (~exist('bTakeFirstEvent', 'var') || isempty(bTakeFirstEvent))
   bTakeFirstEvent = DEF_bTakeFirstEvent;
end

% - Get events
vtTimeTrace = tsEvents.vtTimeTrace;

% -- Catch an empty time series

if (isempty(vtTimeTrace) || all(isnan(vtTimeTrace)))
   return;
end


% -- Perform hysteresis filtering

% - Reverse time series, if "last" events are required
if (~bTakeFirstEvent)
   vtTimeDiffs = abs(diff(vtTimeTrace(end:-1:1)));
else
   vtTimeDiffs = diff(vtTimeTrace);
end

vbAcceptEvent = [true; false(size(vtTimeDiffs))];
vbAcceptEvent(2:end) = vtTimeDiffs > tHysteresisTime;

% - Reverse time series, if "last" events are required
if (~bTakeFirstEvent)
   vbAcceptEvent = vbAcceptEvent(end:-1:1);
end

% - Create proper time series object
tsDBEvents = TimeSeries(vtTimeTrace(vbAcceptEvent), [], 'event', [], [], tsEvents.vtTimeBounds);

% --- END of DebounceTS.m ---