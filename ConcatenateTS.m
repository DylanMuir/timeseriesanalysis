function tsConcatenated = ConcatenateTS(varargin)

% ConcatenateTS - FUNCTION Concatenate several time series back to back
%
% Usage: tsConcatenated = ConcatenateTS(tsSeries1, tsSeries2, ...)
%
% 'tsSeriesX' must all be time series with the same sampling type, the same
% number of time series and the same sample ranges.
%
% 'tsConcatenated' will be a time series containing all the time samples
% from 'tsSeriesX' placed back-to-back, such that the time bases of each
% series are shifted to follow one another.
%
% Concatenated time series will always be shifted to start at T=0.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

TS_deprecated('concatenate');

tsConcatenated = concatenate(varargin{:});

% --- END of ConcatenateTS.m ---
