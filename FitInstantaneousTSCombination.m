function [vfFactors, vtLags, vfOffsets, vfTFParams, tsPrediction, tsResiduals] = ...
   FitInstantaneousTSCombination(vtsSourceSeries, tsDependentSeries, ...
                                 fhTransfer, nNumTFParams, vfTFP0, vtLagLimits, ...
                                 bNormaliseIndependentVariance, bShowProgress)

% FitInstantaneousTSCombination - FUNCTION Fit a multiple linear regression model to a combination of time series
%
% Usage: [vfFactors, vtLags, tsPredicted, tsResiduals] = ...
%           FitInstantaneousTSCombination(vtsSourceSeries, tsDependentSeries ...
%                                         <, fhTransfer, nNumTFParams, vfTFP0, vtLagLimits, ...
%                                            bNormaliseIndependentVariance, bShowProgress>)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 2nd September, 2014

% -- Defaults

DEF_vtLagLimits = [0 inf];
DEF_bNormaliseIndependentVariance = false;
DEF_bShowProgress = false;


% - Check parameters
if (nargin < 2)
   help FitInstantaneousTSCombination;
   error('*** FitInstantaneousTSCombination: Incorrect usage');
end

if (~exist('fhTransfer', 'var'))
   fhTransfer = [];
   nNumTFParams = 0;
   vfTFP0 = [];
end

if (~exist('nNumTFParams', 'var') || isempty(nNumTFParams))
   nNumTFParams = 0;
   vfTFP0 = [];
end

if ((nNumTFParams > 0) && ~exist('vfTFP0', 'var'))
   vfTFP0 = ones(nNumTFParams, 1);
end

if (~exist('vtLagLimits', 'var') || isempty(vtLagLimits))
   vtLagLimits = DEF_vtLagLimits;
end

if (~exist('bNormaliseIndependentVariance', 'var') || isempty(bNormaliseIndependentVariance))
   bNormaliseIndependentVariance = DEF_bNormaliseIndependentVariance;
end

if (~exist('bShowProgress', 'var') || isempty(bShowProgress))
   bShowProgress = DEF_bShowProgress;
end

% - Set up initial parameters
vtsSourceSeries = reshape(vtsSourceSeries, [], 1); % Make a column vector
nNumSourceSeries = numel(vtsSourceSeries);
[nNumDependentTimePoints, nNumDependentSeries] = size(tsDependentSeries.tfSamples);


% -- Normalise independent time series, if requested

if (bNormaliseIndependentVariance)
   % - Estimate variance for each source series
   vfSourceVar = arrayfun(@var_t, vtsSourceSeries);
   
   % - Normalise samples in each source series by estimated variance
   ctfNormalisedSamples = arrayfun(@(ts, v)(ts.tfSamples ./ sqrt(v)), vtsSourceSeries, vfSourceVar, 'UniformOutput', false);
   [vtsSourceSeries.tfSamples] = deal(ctfNormalisedSamples{:});
end


% -- Step 1: Estimate inital lags using cross-correlation

% - Show some progress
fprintf(1, '--- FitInstantaneousTSCombination: Estimating lags...\n');

% - Resample time series for periodic sampling
fMeanSamplingTime = mean(diff(tsDependentSeries.vtTimeTrace));
vtPeriodicTimeTrace = (tsDependentSeries.vtTimeTrace(1):fMeanSamplingTime:tsDependentSeries.vtTimeTrace(end))';
ctfSourcePeriodic = arrayfun(@(ts, vt)(ts.fhInterpolation(ts, vtPeriodicTimeTrace)), vtsSourceSeries, 'UniformOutput', false);
tfDependentPeriodic = tsDependentSeries.fhInterpolation(tsDependentSeries, vtPeriodicTimeTrace);
tfSourceFFT = permute(reshape(cell2mat(cellfun(@(ts)(fft(ts, 2*2^nextpow2(nNumDependentTimePoints)-1, 1)), ctfSourcePeriodic, 'UniformOutput', false)), [], nNumSourceSeries), [1 3 2]);
tfDependentFFT = fft(tfDependentPeriodic, 2*2^nextpow2(nNumDependentTimePoints)-1, 1);

% - Compute cross-correlations to estimate lags
tfXCorrelation = abs(real(ifft(bsxfun(@times, conj(tfSourceFFT), tfDependentFFT))));
tfXCorrelation = [tfXCorrelation(end-nNumDependentTimePoints+1:end, :, :); tfXCorrelation(1:nNumDependentTimePoints+1, :, :)];

vnXCorrLags = (-nNumDependentTimePoints:nNumDependentTimePoints)';
vtXCorrLags = vnXCorrLags .* fMeanSamplingTime;

% - Identify cross-correlation peaks
vbValidLags = (vtXCorrLags >= vtLagLimits(1)) & (vtXCorrLags <= vtLagLimits(2));
tfXCorrelation(~vbValidLags, :) = nan;
[~, mnMaxXCorr] = max(tfXCorrelation, [], 1);
mnMaxXCorr = shiftdim(mnMaxXCorr, 1);


% -- Step 2: Perform an optimisation step to tune lags and work out factors

% - Pre-allocate prediction and residuals time series
tsPrediction = tsDependentSeries;
tfPredictionSamples = tsPrediction.tfSamples;
tsResiduals = tsDependentSeries;
tfResidualsSamples = tsResiduals.tfSamples;

% - Perform independent optimisation step for each dependent time series
parfor (nDependentSeries = 1:nNumDependentSeries)
   % - Display some progress
   fprintf(1, '--- FitInstantaneousTSCombination: Dependent series %d of %d...\n', nDependentSeries, nNumDependentSeries);
   
   % - Get this dependent series
   tsThisDependent = tsDependentSeries;
   tsThisDependent.tfSamples = tsThisDependent.tfSamples(:, nDependentSeries);
   
   % - Perform estimation
   [tsThisPrediction, tsThisRediduals, ...
      vfFactors(:, nDependentSeries), vtLags(:, nDependentSeries), vfOffsets(:, nDependentSeries), vfTFParams(:, nDependentSeries)] = ...
         FitSingleDependentSeries(vtsSourceSeries, tsThisDependent, vtXCorrLags(mnMaxXCorr(nDependentSeries, :)), ...
                                  vtLagLimits, fhTransfer, vfTFP0, bShowProgress);

   % - Concatenate predictions and residuals
   tfPredictionSamples(:, nDependentSeries) = tsThisPrediction.tfSamples;
   tfResidualsSamples(:, nDependentSeries) = tsThisRediduals.tfSamples;
end

% - Assign predictions and residuals back to output arguments
tsPrediction.tfSamples = tfPredictionSamples;
tsResiduals.tfSamples = tfResidualsSamples;

% % - Configure initial parameter estimate
% vfX0 = EncodeParams(ones(nNumSourceSeries*nNumDependentSeries, 1), ...
%                     reshape(vtXCorrLags(mnMaxXCorr(:)), [], 1), ...
%                     zeros(nNumSourceSeries*nNumDependentSeries, 1), ...
%                     reshape(repmat(vfTFP0, nNumSourceSeries*nNumDependentSeries, 1), [], 1));
% 
% % - Set up constraints
% A = [];
% b = [];
% Aeq = [];
% beq = [];
% lb = [-inf(nNumSourceSeries*nNumDependentSeries, 1); repmat(vtLagLimits(1), nNumSourceSeries*nNumDependentSeries, 1); -inf(nNumTFParams*nNumDependentSeries, 1)];
% ub = [inf(nNumSourceSeries*nNumDependentSeries, 1); repmat(vtLagLimits(2), nNumSourceSeries*nNumDependentSeries, 1); inf(nNumTFParams*nNumDependentSeries, 1)];
% nonlcon = [];
% 
% % - Replicate source time series to required predictors
% vtsSourceSeries = cell2mat(arrayfun(@(ts)SplitTS(repmat_ts(ts, nNumDependentSeries)), vtsSourceSeries, 'UniformOutput', false));
% 
% % - Set up current lags cache
% vtCurrentLags = nan(nNumSourceSeries*nNumDependentSeries, 1);
% vtsResampledSources = vtsSourceSeries;
% tfResampledSources = nan(nNumDependentTimePoints, nNumDependentSeries, nNumSourceSeries);
% 
% % - Fit with non-linear constrained optimisation
% sOpt = optimset('Display', 'iter', 'Outputfcn', @FSTSC_Display, 'MaxFunEvals', 1e6, 'Algorithm', 'active-set');
% vfXHat = fmincon(@FSTSC_CostFunction, vfX0, A, b, Aeq, beq, lb, ub, nonlcon, sOpt);
% 
% % - Return prediction, residuals and parameters
% [tsPrediction, tsResiduals] = MakePrediction(vfXHat);
% [vfFactors, vtLags, vfTFParams] = ExtractParams(vfXHat);

end

% --- END of FitInstantaneousTSCombination FUNCTION ---

% FitSingleDependentSeries - FUNCTION Perform the optimisation step on a single dependent series
function [tsThisPrediction, tsThisRediduals, ...
            vfFactors, vtLags, vfOffsets, vfTFParams] = ...
               FitSingleDependentSeries(vtsSourceSeries, tsThisDependent, vtLag0, vtLagLimits, fhTransfer, vfTFP0, bShowProgress)

persistent tsPrediction;

% - Perform a sanity check on the dependent series
if (all(isnan(tsThisDependent.tfSamples(:))))
   tsThisPrediction = tsThisDependent;
   tsThisRediduals = tsThisDependent;
   vfFactors = nan(numel(vtsSourceSeries), 1);
   vtLags = nan(numel(vtsSourceSeries), 1);
   vfOffsets = nan(numel(vtsSourceSeries), 1);
   vfTFParams = nan(numel(vfTFP0), 1);
   return;
end

% - Get information about time series
nNumSourceSeries = numel(vtsSourceSeries);
nNumDependentTimePoints = size(tsThisDependent.tfSamples, 1);
nNumTFParams = numel(vfTFP0);

% - Estimate variance for each source series
vfSourceVar = shiftdim(arrayfun(@var_t, vtsSourceSeries));

% - Estimate factors to match variance of dependent TS
fDependentVar = tsThisDependent.var_t();
vfFactors0 = fDependentVar ./ vfSourceVar;
vfFactors0(isnan(vfFactors0) | isinf(vfFactors0)) = 0;

% - Configure initial parameter estimate
vfX0 = EncodeParams(vfFactors0, ...
                    vtLag0, ...
                    zeros(nNumSourceSeries, 1), ...
                    vfTFP0);

% - Set up constraints
A = [];
b = [];
Aeq = [];
beq = [];
lb = [-inf(nNumSourceSeries, 1); repmat(vtLagLimits(1), nNumSourceSeries, 1); -inf(nNumSourceSeries, 1); -inf(nNumTFParams, 1)];
ub = [inf(nNumSourceSeries, 1); repmat(vtLagLimits(2), nNumSourceSeries, 1); inf(nNumSourceSeries, 1); inf(nNumTFParams, 1)];
nonlcon = [];

% - Set up current lags cache
vtCurrentLags = nan(nNumSourceSeries, 1);
vtsResampledSources = vtsSourceSeries;
tfResampledSources = nan(nNumDependentTimePoints, 1, nNumSourceSeries);

% - Fit with non-linear constrained optimisation
if (bShowProgress)
   sOpt = optimset('Display', 'iter', 'Outputfcn', @FSTSC_Display, 'MaxFunEvals', 1e5, 'Algorithm', 'active-set');
else
   sOpt = optimset('Display', 'none', 'MaxFunEvals', 1e5, 'Algorithm', 'active-set');
end

vfXHat = fmincon(@FSTSC_CostFunction, vfX0, A, b, Aeq, beq, lb, ub, nonlcon, sOpt);

% - Return prediction, residuals and parameters
[tsThisPrediction, tsThisRediduals] = MakePrediction(vfXHat);
[vfFactors, vtLags, vfOffsets, vfTFParams] = ExtractParams(vfXHat);

% --- END of FitSingleDependentSeries FUNCTION ---


   function fMSE = FSTSC_CostFunction(vfX)
      [tsPrediction, tsResiduals] = MakePrediction(vfX);
      fMSE = nanmean(tsResiduals.tfSamples(:).^2);
   end

   function bTerminate = FSTSC_Display(~, ~, strState)
      persistent hFigure;
      
      switch(strState)
         case 'init';
            hFigure = gcf;
            xlabel('Time');
            ylabel('Response');
            hold all;
            
         case 'iter';
            % - Use most recently calculated prediciton
            % tsPrediction = MakePrediction(vfX);
            
            figure(hFigure);
            cla;
            plot_ts(tsPrediction, 'r-');
            plot_ts(tsThisDependent, 'k:');
            drawnow;
            
         otherwise
      end
      
      bTerminate = false;
   end

   function [tsPredicted, tsResiduals] = MakePrediction(vfX)
      % - Get model params
      [vfFactors, vtLags, vfOffsets, vfTFParams] = ExtractParams(vfX);
      
      % - Interpolate source series to target time base
      % - Which sources need resampling?
      vbResampleSource = vtLags ~= vtCurrentLags;
      if (nnz(vbResampleSource) > 0)
         vtsResampledSources(vbResampleSource) = arrayfun(@(ts, l)(ResampleTS(ts, tsThisDependent.vtTimeTrace - l)), vtsSourceSeries(vbResampleSource), vtLags(vbResampleSource), 'UniformOutput', true);
         tfResampledSources(:, vbResampleSource) = cat(2, vtsResampledSources(vbResampleSource).tfSamples);
         vtCurrentLags = vtLags;
      end
      
      % - Combine source series
      tfLinearCombination = bsxfun(@minus, tfResampledSources, reshape(vfOffsets, 1, 1, nNumSourceSeries));
      tfLinearCombination = nansum(bsxfun(@times, tfLinearCombination, reshape(vfFactors, 1, 1, nNumSourceSeries)), 3);
      
      % - Pass through transfer function
      if (~isempty(fhTransfer))
         tfNLPrediction = fhTransfer(tsDependentSeries.vtTimeTrace, tfLinearCombination, vfTFParams);
      else
         tfNLPrediction = tfLinearCombination;
      end
      
      % - Construct prediction and residuals
      tfResidualSamples = tsThisDependent.tfSamples - tfNLPrediction;
      %       tfResidualSamples = bsxfun(@minus, tfResidualSamples, nanmean(tfResidualSamples, 1));
      tsPredicted = TimeSeriesStructure(tsThisDependent.vtTimeTrace, tfNLPrediction);
      tsResiduals = TimeSeriesStructure(tsThisDependent.vtTimeTrace, tfResidualSamples);
   end

   function [vfFactors, vtLags, vfOffsets, vfTFParams] = ExtractParams(vfX)
      vfFactors = vfX(1:nNumSourceSeries);
      vtLags = vfX(nNumSourceSeries+1:nNumSourceSeries*2);
      vfOffsets = vfX(nNumSourceSeries*2+1:nNumSourceSeries*3);
      vfTFParams = vfX(end-nNumTFParams+1:end);
   end

   function vfX = EncodeParams(vfFactors, vtLags, vfOffsets, vfTFParams)
      vfX = [vfFactors; vtLags; vfOffsets; vfTFParams];
   end
end


% --- END of FitInstantaneousTSCombination.m ---
