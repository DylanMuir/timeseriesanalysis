function tfInterpolated = InterpolateLinearFillNans(sTS, vtInterpolationTimes, tMaxStackGap)

% InterpolateLinearFillNans - FUNCTION Interpolate time series, filling gaps with NaNs
%
% Usage: [tfInterpolated] = InterpolateLinearFillNans(sTS, vtInterpolationTimes, tMaxStackGap)
%
% 'sTS' is a TimeSeries object. 'vtInterpolationTimes' is a vector of
% times at which interpolation should be performed. 'tMaxStackGap' is a
% duration which indicates a dropped frame. This value should be something
% greater than the inter-frame interval, for regularly sampled time series.
%
% 'InterpolateLinearFillNans' can be used as a user-supplied interpolation
% function when creating a TimeSeries object (see documentation for
% 'TimeSeries' for more information).

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 28th August, 2015

% -- No argument checking, for speed

% - Short-cut for empty time series
if isempty(sTS.vtTimeTrace)
   tfInterpolated = nan(numel(vtInterpolationTimes), 1);
   return;
end

% - Find time samples preceeding interpolation times
[vtInterpolationTimes, vnInterpolantOrder] = sort(reshape(vtInterpolationTimes, [], 1));
vnPreviousSample = reshape(sTS.fhFindPreviousSample(sTS.vtTimeTrace, vtInterpolationTimes), [], 1);
vbAcceptInterpolation = ~isnan(vnPreviousSample);
vbExactSample = false(size(vbAcceptInterpolation));
vbExactSample(vbAcceptInterpolation) = sTS.vtTimeTrace(vnPreviousSample(vbAcceptInterpolation)) == vtInterpolationTimes(vbAcceptInterpolation);

% - Identify gaps that are too long, which indicates dropped samples
vbFillNaNs = false(size(vbAcceptInterpolation));
vbFillNaNs(vbAcceptInterpolation) = (vtInterpolationTimes(vbAcceptInterpolation) - sTS.vtTimeTrace(vnPreviousSample(vbAcceptInterpolation))) > tMaxStackGap;
vbAcceptInterpolation = vbAcceptInterpolation & ~vbFillNaNs;

% - Pre-allocate samples
vnSampleSize = size(sTS.tfSamples);
vnSampleSize(1) = numel(vtInterpolationTimes);
tfInterpolated = nan(vnSampleSize);

% - Catch interpolation times which are exactly time points of the source time series
tfInterpolated(vbExactSample, :) = sTS.tfSamples(vnPreviousSample(vbExactSample), :);
vbPerformInterpolation = vbAcceptInterpolation & ~vbExactSample;

% - Interpolate samples when necessary
if any(vbPerformInterpolation)
   vtPreviousTime = sTS.vtTimeTrace(vnPreviousSample(vbPerformInterpolation));
   vtNextTime = sTS.vtTimeTrace(vnPreviousSample(vbPerformInterpolation)+1);
   vfInterpolationFactors = (vtInterpolationTimes(vbPerformInterpolation) - vtPreviousTime) ./ ...
      (vtNextTime - vtPreviousTime);
   
   % - Interpolate samples
   tfNextSample = double(sTS.tfSamples(vnPreviousSample(vbPerformInterpolation)+1, :));
   tfPreviousSample = double(sTS.tfSamples(vnPreviousSample(vbPerformInterpolation), :));
   tfInterpolated(vbPerformInterpolation, :) = bsxfun(@times, vfInterpolationFactors, ...
      (tfNextSample - tfPreviousSample)) + tfPreviousSample;
end

% - Remove non-accepted interpolants
tfInterpolated(~vbAcceptInterpolation, :) = nan;

% - Reorder samples
tfInterpolated(vnInterpolantOrder, :) = tfInterpolated(:, :);

% --- END of InterpolateLinearFillNans.m ---

