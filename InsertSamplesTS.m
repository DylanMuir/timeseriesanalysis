function ts = InsertSamplesTS(varargin)

% InsertSamplesTS - FUNCTION Insert samples into a time series
%
% Usage: ts = InsertSamplesTS(ts, vtNewTimeTrace, tfNewSamples)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 21st April, 2015

TS_deprecated('insert');

ts = insert(varargin{:});

% --- END of InsertSamplesTS.m ---