function [tsAndTS] = BoolAndTS(tsA, tsB)

% BoolAndTS - FUNCTION Perform a boolean AND of two boolean time series
%
% Usage: [tsAndTS] = BoolAndTS(tsA, tsB)

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th August, 2015

TS_deprecated('& or ''and''');

tsAndTS = tsA & tsB;

% --- END of BoolAndTS.m ---
