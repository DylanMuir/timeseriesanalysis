function tfMean = mean_tTS(tsSource, varargin)

% mean_tTS - FUNCTION Compute mean of time series, over time
%
% Usage: tfMean = mean_tTS(tsSource <, bIgnoreNaNs>)
%
% 'tsSource' is a continuous or count time series. The sample mean for each
% series in 'tsSource' will be computed and returned in 'tfMean'. 'tfMean'
% will be of size [1 M N ...], corresponding to the size of the time series
% in 'tsSource'.
%
% Samples will be weighted according the time duration they represent, for
% irregularly-sampled time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 14th September, 2015

TS_deprecated('mean_t');

tfMean = tsSource.mean_t(varargin{:});

% --- END of mean_tTS.m ---
