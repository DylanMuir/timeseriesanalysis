function tfVar = var_tTS(varargin)

% var_tTS - FUNCTION Compute variance of a time series, over time
%
% Usage: tfVar = var_tTS(tsSource)
%
% 'tsSource' is a continuous or count time series. The sample infinite-time
% variance for each series in 'tsSource' will be computed and returned in
% 'tfVar'. 'tfVar' will be of size [1 M N ...], corresponding to the size
% of the time series in 'tsSource'.
%
% Samples will be weighted according the time duration they represent, for
% irregularly-sampled time series.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 10th September, 2014

TS_deprecated('var_t');

tfVar = var_t(varargin{:});

% --- END of var_tTS.m ---
