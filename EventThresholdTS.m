function tsEvents = EventThresholdTS(tsSource, tsThreshold, strCrossingType, bInterpolate)

% EventThresholdTS - FUNCTION Find time points when a (possibly dynamic) threshold is crossed (with interpolation)
%
% Usage: tsEvents = EventThresholdTS(tsSource, tsThreshold <, strCrossingType, bInterpolate>)
%        tsEvents = EventThresholdTS(tsSource, fThreshold <, strCrossingType, bInterpolate>)
%
% Time series 'tsSource' will be examined to determine when the values in
% the time series pass an arbitrary threshold, defined either by another
% time series 'tsThreshold' (in which case the threshold is dynamic) or by
% a constant scalar value 'fThreshold' (in which case the threshold is
% constant). 'tsSource' (and 'tsThreshold') must be unitary time series.
%
% The optional argument 'strCrossingType' determines whether rising
% crossings, falling crossings or both are returned. 'strCrossingType' must
% be one of {'both', 'rising', 'falling'}.
%
% The optional argument 'bInterpolate' controls whether precise threshold
% crossing times will be found through optimisation. When 'true'
% (default) threshold crossing times will be optimised by interpolating
% between samples. When 'false', the nearest sample time to the threshold
% crossing will be returned.
%
% 'tsEvents' will be a new "event" time series, containing the times of the
% crossings.
%
% 'tsEvents' is determined in a two-step estimation and optimisation
% process. Interpolation of the source time series is used to find the
% optimal crossing times.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 4th September, 2014

% -- Defaults

DEF_cstrAllowedSampleTypes = {'continuous', 'count', 'boolean'};
DEF_strCrossingType = 'both';
DEF_bInterpolate = true;


% -- Check arguments

if (nargin < 2)
   help EventThresholdTS;
   error('TimeSeries:Usage', ...
      '*** EventThresholdTS: Incorrect usage.');
end

if (~any(tsSource.testsampletype(DEF_cstrAllowedSampleTypes)))
   error('TimeSeries:Arguments', ...
      '*** EventThresholdTS: This function requires continuous, count or boolean time series data.');
end

if (~exist('strCrossingType', 'var') || isempty(strCrossingType))
   strCrossingType = DEF_strCrossingType;
end

if (~exist('bInterpolate', 'var') || isempty(bInterpolate))
   bInterpolate = DEF_bInterpolate;
end

% - Work out dimensions of time series
[nNumTimePoints, nNumSourceSeries] = size(tsSource.tfSamples);

% - Catch empty source time series
if (nNumTimePoints == 0)
   % - Return an empty event time series
   tsEvents = TimeSeriesStructure([], [], 'event');
   return;
end

% - Handle boolean time series
if (tsSource.testsampletype('boolean'))
   bInterpolate = false;
   tsThreshold = 0.5;
end

if (nNumSourceSeries > 1)
   error('TimeSeries:Arguments', ...
      '*** EventThresholdTS: This function can only operate on unitary time series.');
end

if (istimeseries(tsThreshold))
   [~, nNumThresholdSeries] = size(tsThreshold.tfSamples);
   
   if (nNumThresholdSeries > 1)
      error('TimeSeries:Arguments', ...
         '*** EventThresholdTS: This function can only operate on unitary time series.');
   end
else
   % - Convert static threshold into a time series structure
   tsThreshold = TimeSeries(tsSource.vtTimeTrace([1 end]), tsThreshold * [1 1], 'continuous');
end


% -- Step 1: Perform initial estimate of threshold crossing

% - Shift samples to (dynamic) threshold
tsResampThreshold = tsThreshold.resample(tsSource.vtTimeTrace);
vfSamples = tsSource.tfSamples - tsResampThreshold.tfSamples;

% - Initial crossing estimate
vnDiffs = diff(sign(vfSamples));
vnInitialCrossings = unique(find(vnDiffs));

% - Filter crossings by type
vbFalling = vnDiffs(vnInitialCrossings) < 0;
vbRising = vnDiffs(vnInitialCrossings) > 0;

if (ischar(strCrossingType))
   strCrossingType = lower(strCrossingType);
end

switch strCrossingType
   case {'falling', -1}
      vnInitialCrossings = vnInitialCrossings(vbFalling);
      
   case {'rising', 1}
      vnInitialCrossings = vnInitialCrossings(vbRising);
      
   otherwise
      % - Accept both
      vnInitialCrossings = vnInitialCrossings(vbRising | vbFalling);
end

% - Nudge potential crossings at the start or end of the time series
vnInitialCrossings(vnInitialCrossings == nNumTimePoints) = nNumTimePoints-1;
vnInitialCrossings(vnInitialCrossings == 1) = 2;

% - Were any crossings identified?
if (isempty(vnInitialCrossings))
   % - No, so return an empty time series
   tsEvents = TimeSeries([], [], 'event');
   return;
end

% - Filter "jitter" threshold crossings (crossings on adjacent samples)
vbDiscardCrossings = [false; diff(vnInitialCrossings) <= 1];
vnInitialCrossings = vnInitialCrossings(~vbDiscardCrossings);

% - Filter "touches" on adjacent time samples
vbDiscardCrossings = sign(vfSamples(vnInitialCrossings-1)) == sign(vfSamples(vnInitialCrossings+1));
vnInitialCrossings = vnInitialCrossings(~vbDiscardCrossings);

% - Were any crossings identified?
if (isempty(vnInitialCrossings))
   % - No, so return an empty time series
   tsEvents = TimeSeries([], [], 'event');
   return;
end


% -- Step 2: Refine estimates using an optimisation step

if (bInterpolate)
   parfor (nCrossingNum = 1:numel(vnInitialCrossings))
      % - Is there a crossing here?
      vtSegmentEndpoints = [tsSource.vtTimeTrace(vnInitialCrossings(nCrossingNum)) tsSource.vtTimeTrace(vnInitialCrossings(nCrossingNum)+1)]; %#ok<PFBNS>
      vfSegmentEndpoints = tsSource.fhInterpolation(tsSource, vtSegmentEndpoints) - ...
         tsThreshold.fhInterpolation(tsThreshold, vtSegmentEndpoints); %#ok<PFBNS>
      
      % - Check for valid endpoints
      if (any(~isfinite(vfSegmentEndpoints)) || isequal(sign(vfSegmentEndpoints(1)), sign(vfSegmentEndpoints(2))))
         % - There is no zero-crossing here, so discard
         vtCrossings(nCrossingNum) = nan;
         continue;
      end
      
      % - Optimise the location of this threshold-crossing
      vtCrossings(nCrossingNum) = fzero(@(t)(tsSource.fhInterpolation(tsSource, t) - tsThreshold.fhInterpolation(tsThreshold, t)), ...
         [tsSource.vtTimeTrace(vnInitialCrossings(nCrossingNum)) tsSource.vtTimeTrace(vnInitialCrossings(nCrossingNum)+1)]);
   end
   
else
   vtCrossings = tsSource.vtTimeTrace(vnInitialCrossings);
end

% -- Convert identified crossing times into an event time series

tsEvents = TimeSeries(unique(vtCrossings), [], 'event');

end

% --- END of EventThresholdTS.m ---
