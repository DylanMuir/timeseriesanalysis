function [tsResampled] = ResampleTS(varargin)

% ResampleTS - FUNCTION Resample a time series to a target time base
%
% Usage: [tsResampled] = ResampleTS(tsSource, vtTargetTimeBase)
%
% ResampleTS uses the defined interpolation function to resample a time
% series 'tsSource', to a target time base.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: September 2014

TS_deprecated('resample');

tsResampled = resample(varargin{:});

% --- END of ResampleTS.m ---
