function [vfBaseRange, vfTestCTEst, vfTestCTSEM, fCoeffDetermDirect, vbSignificant, mfPCI, vbUndecided, nSamplesDrawn] = ...
            MeasureRelationshipTS(tsBase, tsTest, fhEstimator, fBandwidth, fAlpha, nMaxBootstrapSamples)

% MeasureRelationshipTS - FUNCTION Quantify a first-order relationship between two countable time series
%
% Usage: [vfBaseRange, vfTestCTEst, vfTestCTSEM, fCoeffDetermDirect] = MeasureRelationshipTS(tsBase, tsTest, <fhEstimator, fBandwidth>)
%        [..., vbSignificant, mfPCI, vbUndecided, nSamplesDrawn] = MeasureRelationshipTS(..., <fAlpha, nMaxBootstrapSamples>)
%
% This function measures the average first-order relationship between
% values of two time series. I.e., what is the expected value of 'tsTest',
% given a value of 'tsBase'? This can be used to estimate tuning curves of
% firing rate given running speed, for example.
%
% 'tsBase' defines a time series that will serve as the independent base
% variable. 'tsTest' defines the dependent variable. This function will
% estimate E[tsTest(t) | tsBase(t)]; the expected value of 'tsTest' at time
% 't', given the value of 'tsBase' at time 't'. This function will also
% estimate \sigma_{tsTest(t) | tsBase(t)}; the standard deviation of this
% measure. Note that both these operators can be swapped for any other
% measures of central tendency and spread, using 'fhEstimator' (see below).
%
% 'vfBaseRange' will be a set of values from 'tsBase', in sorted order.
% This vector defines the range values for each element of the other
% return arguments. 'vfTestCTEst' is a vector of values estimating the
% central tendency (CT) of 'tsTest'. Each element corresponds to an element
% in 'vfBaseRange'. A tuning curve is therefore plotted using the command
%    plot(vfBaseRange, vfTestCTEst);
%
% 'vfTestCTSEM' is a vector of values estimating the spread of
% 'tsTest', with each element corresponding to an element in 'vfBaseRange'.
% The default 'fhEstimator' returns the standard error of the mean.
%
% An arbitrary function can be optionally provided to compute the central
% tendency and spread of the relationship, by providing a function handing
% in 'fhEstimator'. 'fhEstimator' must have the signature
%    [fCentralTendencyEst, fSpreadEst] = fhEstimator(vfTestSamples, vfSampleWeights);
%
% This function uses a kernel density estimator to weight samples, using a
% Gaussian function with an automatically estimated bandwidth (standard
% deviation). The bandwidth can be optionally provided in 'fBandwidth', to
% override the estimated bandwidth.
%
% This function optionally computes confidence intervals for the central
% tendency, returned in the argument 'mfCTCI'. 'mfCTCI' is a [2 x N]
% matrix, with mfCTCI(1, :) corresponding to the lower confidence bound and
% mtCTCI(2, :) corresponding to the upper confidence bound. Each element in
% each vector corresponds to a base sample in 'vfBaseRange'. The desired
% confidence interval can be optionally specified by 'fCI', as a
% proportion. By default, 90% confidence intervals are returned. Sufficient
% bootstrap samples will be made to estimate the desired CI.
%
% This function optionally estimates whether the relationship between the
% two time series is significant, by quantifying whether the "test" time
% series is significantly elevated depending on some values of the "base"
% time series. This analysis is based on a Monte Carlo boostrap. The alpha
% significance level can be specified with the optional parameter 'fAlpha'.
% By default, 'fAlpha' = 0.05.
%
% 'vbSignificant' will a boolean vector, indicating whether the
% corresponding "base" time series value in 'vfBaseRange' leads to a
% significant increase in the "test" time series value. 'mfPCI' will be a
% [2 x N] matrix, containing conservative (i.e. broad) confidence intervals
% for the corresponding P values for each element in 'vfBaseRange'.
% 'vbUndecided' will be a boolean vector, indicating whether the
% corresponding "base" time series value was decided upon or not. These
% will be the values in the "base" time series that are closest to the
% 'fAlpha' significance level (either above or below). No more than 5% of
% the values in the "base" time series will be left undecided.
%
% 'nSamplesDrawn' will contain the total number of bootstrap samples taken
% to derermine significance.
%
% The optional parameter 'nMaxBootstrapSamples' allows one to determine the
% maximum effort made to estimate significance. One bootstrap sample
% corresponds to one hypothesis test per base TS sample. By default, the
% limit is set to 10 times the square of the number of base TS samples.

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: 17th February, 2015

% -- Defaults

DEF_fAlpha = [];
DEF_fhEstimator = @WeightedNanMeanStd;

%% -- Check arguments

if (nargin < 2)
   help MeasureRelationshipTS;
   error('TimeSeries:Usage', ...
      '*** MeasureRelationshipTS: Incorrect usage.')
end

if (~exist('fAlpha', 'var') || isempty(fAlpha))
   fAlpha = DEF_fAlpha;
end

if (~exist('fhEstimator', 'var') || isempty(fhEstimator))
   fhEstimator = DEF_fhEstimator;
end

bEstimateSig = nargout > 4;


%% - Both time series must be countable
cstrAllowedSampleTypes = {'continuous', 'count'};

if (~tsBase.testsampletype(cstrAllowedSampleTypes)) || (~tsTest.testsampletype(cstrAllowedSampleTypes))
   error('TimeSeries:Arguments', ...
      '*** MeasureRelationshipTS: Input time series must be of class [%s].', ...
      sprintf('%s ', cstrAllowedSampleTypes{:}));
end

% - Both time series must be unitary
if (size(tsBase.tfSamples, 2) > 1) || (size(tsTest.tfSamples, 2) > 1)
   error('TimeSeries:Arguments', ...
      '*** MeasureRelationshipTS: Input time series must be unitary.');
end


%% -- Compute kernel density estimates over full data

% - Resample TS with more samples onto other TS
if (numel(tsTest.vtTimeTrace) > numel(tsBase.vtTimeTrace))
   tsTest = tsTest.resample(tsBase.vtTimeTrace);
else
   tsBase = tsBase.resample(tsTest.vtTimeTrace);
end

% - Extract samples
vfSamplesBase = tsBase.tfSamples;
vfSamplesTest = tsTest.tfSamples;

vbUseSample = ~isnan(vfSamplesBase) & ~isnan(vfSamplesTest);
vfSamplesBase = vfSamplesBase(vbUseSample);
vfSamplesTest = vfSamplesTest(vbUseSample);


%% - Estimate bandwidth and calculate sample weighting

if (~exist('fBandwidth', 'var') || isempty(fBandwidth))
   fMedBase = median(vfSamplesBase);
   fSigma = median(abs(vfSamplesBase - fMedBase));
   fBandwidth = 4 * fSigma * (4/(3*numel(vfSamplesBase)))^(1/5);
end

% - Compute base samples weighting
mfWeights = squareform(pdist(vfSamplesBase, @(xi, xj)(normpdf(xj, xi, fBandwidth))));
fTestMean = nanmean(vfSamplesTest);


%% - Compute weighted central tendency and spread for each base sample

% - Estimate central tendency
for (nSampleIndex = 1:numel(vfSamplesBase))
   vfTheseWeights = mfWeights(:, nSampleIndex);
   [vfTestCTEst(nSampleIndex), vfTestCTSEM(nSampleIndex), vfTestSSE(nSampleIndex), vfTestSST(nSampleIndex)] = ...
      fhEstimator(vfSamplesTest, vfTheseWeights, fTestMean); %#ok<PFBNS>
end

% - Measure explained variance
fCoeffDetermDirect = 1 - nansum(vfTestSSE) ./ nansum(vfTestSST);

% - Estimate significance, if requested
if (bEstimateSig)
   % - Set default maximum bootstrap samples
   if (~exist('nMaxBootstrapSamples', 'var') || isempty(nMaxBootstrapSamples))
      nMaxBootstrapSamples = 10*numel(vfSamplesTest)^2;
   end
   
   fhMCSample = @(vnSampleIndex, nNumSamples)BootSample(vfSamplesTest, mfWeights, fhEstimator, vnSampleIndex, nNumSamples, false) >= repmat(vfTestCTEst(vnSampleIndex)', 1, nNumSamples);
   [vbSignificant, ~, vbUndecided, mfPCI, vnMCSamples] = MMCTest(fhMCSample, numel(vfSamplesBase), fAlpha, numel(vfSamplesBase)/20, nMaxBootstrapSamples);
   nSamplesDrawn = sum(vnMCSamples);
end


%% - Sort and return estimates

[vfBaseRange, vnSortIndices] = sort(vfSamplesBase);
vfTestCTEst = vfTestCTEst(vnSortIndices);
vfTestCTSEM = vfTestCTSEM(vnSortIndices);

% - Sort significance estimates
if (bEstimateSig)
   vbSignificant = vbSignificant(vnSortIndices);
   vbUndecided = vbUndecided(vnSortIndices);
   mfPCI = mfPCI(:, vnSortIndices);
end

% --- END of MeasureRelationshipTS.m ---

function [fMed, fStd] = WeightedNanMedianStd(vfSamples, vfWeights)

% - Sort by value
[vfSamples, vnOrder] = sort(vfSamples);
vfWeights = vfWeights(vnOrder);

% - Find median point
vfCumWeights = cumsum(vfWeights);
nMedPoint = find((vfCumWeights./vfCumWeights(end)) > 0.5, 1, 'first');

% - Return median
fMed = vfSamples(nMedPoint);
fStd = nan;


function [fMean, fStd, fSSEw, fSSTw] = WeightedNanMeanStd(vfSamples, vfWeights, fPopMean)

nDim = 1;

% - Compute mean and std. dev.
fMean = sum(bsxfun(@times, vfSamples, vfWeights), nDim) ./ sum(vfWeights(:));

if (nargout > 1)
   fSSEw = sum(bsxfun(@times, (vfSamples - fMean).^2, vfWeights), nDim) ./ sum(vfWeights(:));
   fSSTw = sum(bsxfun(@times, (vfSamples - fPopMean).^2, vfWeights), nDim) ./ sum(vfWeights(:));
   fStd = sqrt(fSSEw) ./ sqrt(sum(vfWeights(:)));
end


function vfBootH0CTEst = BootSample(vfSamplesTest, mfWeights, fhEstimator, vnSampleIndex, nNumBootstraps, bSampleH1)

DEF_nChunkSize = 20e6;

% - Split bootstrap samples into chunks
nNumChunks = ceil(numel(vfSamplesTest)*nNumBootstraps / DEF_nChunkSize);
vnChunkBounds = round(linspace(1, nNumBootstraps, nNumChunks+1));
vnChunkBounds(1) = 0;
vnChunkBounds(end) = nNumBootstraps;
mnChunkLims = [vnChunkBounds(1:end-1)'+1 vnChunkBounds(2:end)'];

% - Collect weights for parallel distribution
mfDistribWeights = mfWeights(:, vnSampleIndex);

% fprintf(1, '#samplestot: %d #samplessel: %d #bootstraps: %d #chunks: %d\n', ...
%    numel(vfSamplesTest), numel(vnSampleIndex), nNumBootstraps, nNumChunks);

% - Distribute over test samples
parfor (nSample = 1:numel(vnSampleIndex))
   vfTheseWeights = mfDistribWeights(:, nSample);

   % - Loop over bootstrap sample chunks
   vfChunkH0CTEst = [];
   for (nChunkIndex = nNumChunks:-1:1)
      vnChunkWindow = mnChunkLims(nChunkIndex, 1):mnChunkLims(nChunkIndex, 2); %#ok<PFBNS>
      
      % - Generate a bootstrap resampling with replacement
      mnBootSample = randi(numel(vfSamplesTest), numel(vfSamplesTest), numel(vnChunkWindow));
      
      % - Estimate central tendency over resamples
      if (bSampleH1)
         vfChunkH0CTEst(vnChunkWindow) = fhEstimator(vfSamplesTest(mnBootSample), vfTheseWeights(mnBootSample)); %#ok<PFBNS>
      else
         vfChunkH0CTEst(vnChunkWindow) = fhEstimator(vfSamplesTest(mnBootSample), vfTheseWeights);
      end
   end
   
   % - Record bootstrapped estimes
   vfBootH0CTEst(nSample, :) = vfChunkH0CTEst;
end

% --- END of MeasureRelationshipTS.m ---
