function varargout = plot_ts(tsSeries, vnTraces, varargin)

% plot_ts - FUNCTION Plot a time series
%
% Usage: vhOutput = plot_ts(tsSeries)
%                   plot_ts(tsSeries <, vnTraces>)
%                   plot_ts(..., <plot options>)
%
% Plots a time series of any sample type, contained in 'tsSeries'. Series
% structures containing multiple time series add multiple traces to the
% plot (one for each series).
%
% The optional argument 'vnTraces' plots only the selected time series from
% the 'tsSeries' structure.
%
% Standard Matlab plot arguments are supported (see examples below).
%
% The hold state of the axis is respected.
%
% Examples:
% * Plot the 5th time series contained within 'tsSeries'.
%     plot_ts(tsSeries, 5);
%
% * Plot the time series as a green dashed line of width 2 points.
%     plot_ts(tsSeries, [], 'g:', 'LineWidth', 2);

% Author: Dylan Muir <dylan.muir@unibas.ch>
% Created: September 2014

TS_deprecated('plot');

% - Check number of aguments
if (nargin < 1)
   help plot_ts;
   error('TimeSeries:Usage', '*** plot_ts: Incorrect usage.');
end

% - Call plot method
if (exist('vnTraces', 'var') && ~isempty(vnTraces))
   [varargout{1:nargout}] = plot(tsSeries.choose(vnTraces), varargin{:});
else
   [varargout{1:nargout}] = plot(tsSeries, varargin{:});
end

% --- END of plot_ts.m ---
